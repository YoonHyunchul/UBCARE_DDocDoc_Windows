﻿using PCReservationClientVersionManager.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PCReservationClientVersionManager.Procedure
{

    /// <summary>
    /// 실제로 수정되어야할 파일리스트, 삭제되어야할 파일리스트, 생성되어야할 파일 리스트를 넘겨 받아
    /// 실제로 파일서버로 부터 최신 파일을 내려받고 현재 구버전 파일리스트를 삭제하는 클래스
    /// </summary>
    class FileUpdater
    {
        private List<JobFile> shouldBeUpdatedFiles;
        private List<JobFile> shouldBeRemovedFiles;
        private List<JobFile> shouldBeCreatedFiles;


        public FileUpdater(List<JobFile> shouldBeUpdatedFiles, List<JobFile> shouldBeRemovedFiles,List<JobFile> shouldBeCreatedFiles)
        {
            this.shouldBeUpdatedFiles = shouldBeUpdatedFiles;
            this.shouldBeRemovedFiles = shouldBeRemovedFiles;
            this.shouldBeCreatedFiles = shouldBeCreatedFiles;
        }


        public void Update()
        {

        }

        private void fileUpdating()
        {

        }
        private void fileRemoving()
        {

        }
        private void fileCreating()
        {

        }

    }


    


}
