﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using PCReservationClientVersionManager.Domain;

namespace PCReservationClientVersionManager.Procedure
{
    public class LastestFileVersionChecker : FileVersionBase
    {
        public override List<JobFile> FileCheck()
        {
            Console.WriteLine("[LastestFileVersionChecker] Downlad lastest version file list from web server");

            List<JobFile> jobFileList = new List<JobFile>();
            XmlDocument xmlDoc = new XmlDocument();

            try
            {
                xmlDoc.Load(Properties.Settings.Default.PCReservationLastestVersion_xml);
                XmlElement root = xmlDoc.DocumentElement;
                XmlNodeList nodes = root.ChildNodes;
                foreach (XmlNode node in nodes)
                {
                    switch (node.Name)
                    {
                        case "MainVersion":
                            break;
                        case "Files":
                            XmlNodeList files = node.ChildNodes;
                            foreach (XmlNode file in files)
                            {
                                String fileName = file["fileName"].InnerText;
                                String fileExtension = file["fileExtension"].InnerText;
                                String creationTime = file["creationTime"].InnerText;
                                String fullPath = "";
                                String productVersion = file["productVersion"].InnerText;
                                String fileVersion = file["fileVersion"].InnerText;
                                JobFile tmp = new JobFile(fileName, fileExtension, creationTime, fullPath, fileVersion, productVersion);
                                jobFileList.Add(tmp);
                                //Console.WriteLine(tmp.ToString());
                            }
                            break;
                        default:
                            break;
                    }
                }
                return jobFileList;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return null;
            }
        }
    }
}
