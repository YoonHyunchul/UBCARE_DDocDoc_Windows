﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PCReservationClientVersionManager.Domain;

namespace PCReservationClientVersionManager.Procedure
{
    class FileVersionComparer 
    {
        private List<JobFile> shouldBeRemovedFiles;
        private List<JobFile> shouldBeUpdatedFiles;
        private List<JobFile> shouldBeCreatedFiles;

        public FileVersionComparer()
        {
            shouldBeRemovedFiles = new List<JobFile>();
            shouldBeUpdatedFiles = new List<JobFile>();
            shouldBeCreatedFiles = new List<JobFile>();
        }

        public void Compare(List<JobFile> installedFiles, List<JobFile> lastestFiles)
        {
            //////이 함수 구현하기 힘들다 ㅜㅜ 이거만 구현하면 끝나는데 화이팅
        }

        public List<JobFile> GetShouldBeRemovedFiles()
        {
            return this.shouldBeRemovedFiles;
        }

        public List<JobFile> GetShouldBeUpdatedFiles()
        {
            return this.shouldBeUpdatedFiles;
        }
        
        public List<JobFile> GetShouldBeCreatedFiles()
        {
            return this.shouldBeCreatedFiles;
        }

        public int CountOfAllLists()
        {
            return this.shouldBeCreatedFiles.Count + this.shouldBeRemovedFiles.Count + this.shouldBeUpdatedFiles.Count;
        }
    }
}
