﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PCReservationClientVersionManager.Domain;

namespace PCReservationClientVersionManager.Procedure
{
    /// <summary>
    /// 설치된 파일 버전 리스트, 웹서버에서 내려받은 최신 파일버전 리스트를 비교하여
    /// 수정되어야할 파일리스트, 지워져야 하는 파일리슽, 생성되어야할 파일리스트 
    /// 3개 파일리스트를 뽑아내는 클래스 
    /// </summary>
    class UpdatingFileFilter 
    {
        private List<JobFile> shouldBeUpdatedFiles;
        private List<JobFile> shouldBeRemovedFiles;
        private List<JobFile> shouldBeCreatedFiles;


        public UpdatingFileFilter(List<JobFile> installedFiles, List<JobFile> LastestFiles)
        {
            setUpdatedFiles(installedFiles, LastestFiles);
            setRemovedFiles(installedFiles, LastestFiles);
            setCreatedFiles(installedFiles, LastestFiles);
        }

        private void setUpdatedFiles(List<JobFile> installedFiles, List<JobFile> LastestFiles)
        {

        }

        private void setRemovedFiles(List<JobFile> installedFiles, List<JobFile> LastestFiles)
        {

        }

        private void setCreatedFiles(List<JobFile> installedFiles, List<JobFile> LastestFiles)
        {

        }


        public List<JobFile> GetUpdateFiles()
        {
            return this.shouldBeUpdatedFiles;
        }

        public List<JobFile> GetRemoveFiles()
        {
            return this.shouldBeRemovedFiles;
        }

        public List<JobFile> GetCreateFiles()
        {
            return this.shouldBeCreatedFiles;
        }

    }
}
