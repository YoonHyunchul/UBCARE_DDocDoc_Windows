﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PCReservationClientVersionManager.Domain;

namespace PCReservationClientVersionManager.Procedure
{
    public abstract class FileVersionBase
    {
        public abstract List<JobFile> FileCheck();
    }
}
