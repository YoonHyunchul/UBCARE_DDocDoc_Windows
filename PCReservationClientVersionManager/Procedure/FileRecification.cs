﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PCReservationClientVersionManager.Domain;
using System.Net;
using System.IO;

namespace PCReservationClientVersionManager.Procedure
{
    /// <summary>
    /// 실제로 ShouldBeRemovedFiles, ShouldBeUpdatedFiles, ShouldBeCreatedFiles 리스트로 업데이트를 진행하는 클래스
    /// 삭제할건 삭제하고 내려받을것은 내려 받는 핵심 클래스
    /// 실제로 UI에 Progress를 담당하는 클래스
    /// </summary>
    class FileRecification
    {
        private List<JobFile> shouldBeRemovedFiles;
        private List<JobFile> shouldBeUpdatedFiles;
        private List<JobFile> shouldBeCreatedFiles;

        private String filesPath = @"E:\Test";
        private String downloadUrl = "https://s3.ap-northeast-2.amazonaws.com/crm-update-site/";
        
        
        public FileRecification()
        {

        }

        public void SetFiles(List<JobFile> shouldBeRemovedFiles,List<JobFile> shouldBeUpdatedFiles, List<JobFile> shouldBeCreatedFiles)
        {
            this.shouldBeRemovedFiles = shouldBeRemovedFiles;
            this.shouldBeUpdatedFiles = shouldBeUpdatedFiles;
            this.shouldBeCreatedFiles = shouldBeCreatedFiles;
        }

        public void TestMocker()
        {
            shouldBeCreatedFiles = new List<JobFile>();
            shouldBeCreatedFiles.Add(new JobFile("BITClientOfNewReservation.dll", ".dll", "20161128120158", "", "7.0.0.0", "7.0.0.0"));
            shouldBeCreatedFiles.Add(new JobFile("BITCommonLibrary.dll",".dll","20161007142036","","5.5.6.3","5.5.6.3"));
            shouldBeCreatedFiles.Add(new JobFile("BITDataBaseCommon.dll", ".dll", "20161024162128","","5.5.3","5.5.3"));
            shouldBeCreatedFiles.Add(new JobFile("BITEnvironment.dll",".dll","20161007142036","","5.3.101.1","5.3.101.1"));
            shouldBeCreatedFiles.Add(new JobFile("CefSharp.dll",".dll","20130312070854","","",""));
            shouldBeCreatedFiles.Add(new JobFile("PCReservationClient.exe",".exe","20161128120200","","1.0.0.0","1.0.0.0"));
            shouldBeCreatedFiles.Add(new JobFile("PCReservationClient.exe.config",".config","20161128120138","","",""));
            shouldBeCreatedFiles.Add(new JobFile("Proxy.dll",".dll","20161128120156","","1.0.0.0","1.0.0.0"));

            shouldBeRemovedFiles = new List<JobFile>();
            shouldBeRemovedFiles.Add(new JobFile("BITClientOfNewReservation.dll", ".dll", "20161128120158", "", "7.0.0.0", "7.0.0.0"));
            shouldBeRemovedFiles.Add(new JobFile("BITCommonLibrary.dll", ".dll", "20161007142036", "", "5.5.6.3", "5.5.6.3"));
            shouldBeRemovedFiles.Add(new JobFile("BITDataBaseCommon.dll", ".dll", "20161024162128", "", "5.5.3", "5.5.3"));
            shouldBeRemovedFiles.Add(new JobFile("BITEnvironment.dll", ".dll", "20161007142036", "", "5.3.101.1", "5.3.101.1"));
            shouldBeRemovedFiles.Add(new JobFile("CefSharp.dll", ".dll", "20130312070854", "", "", ""));
            shouldBeRemovedFiles.Add(new JobFile("PCReservationClient.exe", ".exe", "20161128120200", "", "1.0.0.0", "1.0.0.0"));
            shouldBeRemovedFiles.Add(new JobFile("PCReservationClient.exe.config", ".config", "20161128120138", "", "", ""));
            shouldBeRemovedFiles.Add(new JobFile("Proxy.dll", ".dll", "20161128120156", "", "1.0.0.0", "1.0.0.0"));

            shouldBeUpdatedFiles = new List<JobFile>();
            shouldBeUpdatedFiles.Add(new JobFile("BITClientOfNewReservation.dll", ".dll", "20161128120158", "", "7.0.0.0", "7.0.0.0"));
            shouldBeUpdatedFiles.Add(new JobFile("BITCommonLibrary.dll", ".dll", "20161007142036", "", "5.5.6.3", "5.5.6.3"));
            shouldBeUpdatedFiles.Add(new JobFile("BITDataBaseCommon.dll", ".dll", "20161024162128", "", "5.5.3", "5.5.3"));
            shouldBeUpdatedFiles.Add(new JobFile("BITEnvironment.dll", ".dll", "20161007142036", "", "5.3.101.1", "5.3.101.1"));
            shouldBeUpdatedFiles.Add(new JobFile("CefSharp.dll", ".dll", "20130312070854", "", "", ""));
            shouldBeUpdatedFiles.Add(new JobFile("PCReservationClient.exe", ".exe", "20161128120200", "", "1.0.0.0", "1.0.0.0"));
            shouldBeUpdatedFiles.Add(new JobFile("PCReservationClient.exe.config", ".config", "20161128120138", "", "", ""));
            shouldBeUpdatedFiles.Add(new JobFile("Proxy.dll", ".dll", "20161128120156", "", "1.0.0.0", "1.0.0.0"));

        }




        public void fileCreatring()
        {
            foreach ( JobFile file in shouldBeCreatedFiles)
            {
                WebClient wc = new WebClient();
                Uri uri = new Uri(downloadUrl + file.FileName);
                String targetFile = Path.Combine(filesPath,file.FileName);
                wc.DownloadFileAsync(uri,targetFile);
            }
        }

        public void fileRemoving()
        {
            foreach ( JobFile file in shouldBeRemovedFiles)
            {
                String targetFile = Path.Combine(filesPath, file.FileName);
                if (File.Exists(targetFile))
                {
                    try
                    {
                        File.Delete(targetFile);
                    }
                    catch (IOException e)
                    {
                        Console.WriteLine(e);
                    }
                }
            }
        }

        public void fileUpdating()
        {
            foreach ( JobFile file in shouldBeUpdatedFiles)
            {
                String targetFile = Path.Combine(filesPath, file.FileName);
                Uri uri = new Uri(downloadUrl + file.FileName);
                WebClient wc = new WebClient();
                if ( File.Exists(targetFile))
                {
                    try
                    {
                        File.Delete(targetFile);
                        
                    }
                    catch(IOException e)
                    {
                        Console.WriteLine(e);
                    }
                }
                wc.DownloadFileAsync(uri, targetFile);
            }
        }
    }
}
