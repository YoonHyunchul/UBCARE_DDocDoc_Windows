﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Diagnostics;
using PCReservationClientVersionManager.Domain;

namespace PCReservationClientVersionManager.Procedure
{
    /// <summary>
    /// This FileVersionChecker Class has available work for getting version of all files which were installed including PCReservationClient
    /// </summary>
    public class InstalledFileVersionSweeper : FileVersionBase
    {
        public override List<JobFile> FileCheck()
        {
            Console.WriteLine("[InstalledFileVersionSweeper] sweeping installed file list from installed path");

            String installedPath = Properties.Settings.Default.PCReservationInstalledVersion_dir;
            List<JobFile> jobFileList = new List<JobFile>();

            try
            {
                if (Directory.Exists(installedPath))
                {
                    DirectoryInfo di = new DirectoryInfo(installedPath);
                    foreach (FileInfo file in di.GetFiles())
                    {
                        JobFile tmp = new JobFile(file);
                        jobFileList.Add(tmp);
                        //Console.WriteLine(tmp);
                    }
                }
                return jobFileList;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return null;
            }
        }
    }
}
