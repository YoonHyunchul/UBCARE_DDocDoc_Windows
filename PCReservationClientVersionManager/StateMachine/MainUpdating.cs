﻿using System;
using PCReservationClientVersionManager.Domain;
using PCReservationClientVersionManager.Procedure;
using System.Collections.Generic;


namespace PCReservationClientVersionManager.StateMachine
{
    /// <summary>
    /// MainUpdating 클래스는 모든 작업을 동기로 실행한다. 
    /// Updater.exe 라는 새로운 프로세스가 예관시를 파일 참조 하는 것이고 
    /// Updater.exe의 UI는 ProgressBar가 전부이다
    /// UI가 끊겨도 된다. Update나 잘하자 반드시 잘하자 'ㅋ'
    /// </summary>
    public partial class MainUpdating 
    {

        private List<JobFile> installedFiles;
        private List<JobFile> lastestFiles;             

        private List<JobFile> shouldBeRemovedFiles;
        private List<JobFile> shouldBeUpdatedFiles;
        private List<JobFile> shouldBeCreatedFiles;

        FileVersionComparer fileVersionComparer;
        InstalledFileVersionSweeper installedFileVersionSweeper;
        LastestFileVersionChecker lastestFileVersionChecker;
        FileRecification fileRecification;

        private UpdateState upState;

        private enum UpdateState 
        {
            init,
            SweepingInstalledFiles,
            DownloadingLastestFileVersion,
            ComparingLists,
            Suicide,
            KillingPCReservation,
            RecificationInit,
            ProcessingShouldBeRemoved,
            ProcessingShouldBeUpdated,
            ProcessingShouldBeCreated,
            ExecutingPCReservation
        }


        public MainUpdating()
        {
            this.upState = UpdateState.init;
        }

        public void Update()
        {
            stateMachine();
        }

        private void stateMachine()
        {
            while(true)
            {
                switch(this.upState)
                {
                    case UpdateState.init:      // my job
                        Console.WriteLine("[UpdaterMachine] init");
                        this.initializing();
                        this.upState = UpdateState.SweepingInstalledFiles;
                        break;
                    case UpdateState.SweepingInstalledFiles:  //class job
                        this.installedFiles = this.installedFileVersionSweeper.FileCheck();
                        Console.WriteLine("[UpdaterMachine] SweepingInstalledFiles");
                        this.upState = UpdateState.DownloadingLastestFileVersion;
                        break;
                    case UpdateState.DownloadingLastestFileVersion: //class job
                        this.lastestFiles = this.lastestFileVersionChecker.FileCheck();
                        Console.WriteLine("[UpdaterMachine] DownloadingLastestFileVersion");
                        this.upState = UpdateState.ComparingLists;
                        break;
                    case UpdateState.ComparingLists:    // class job
                        Console.WriteLine("[UpdaterMachine] ComparingLists");
                        fileVersionComparer.Compare(installedFiles, lastestFiles);

                        if (0 == fileVersionComparer.CountOfAllLists())
                        {
                            this.upState = UpdateState.Suicide;
                        }
                        else
                        {
                            this.shouldBeRemovedFiles = fileVersionComparer.GetShouldBeRemovedFiles();
                            this.shouldBeUpdatedFiles = fileVersionComparer.GetShouldBeUpdatedFiles();
                            this.shouldBeCreatedFiles = fileVersionComparer.GetShouldBeCreatedFiles();
                            this.upState = UpdateState.KillingPCReservation;
                        }
                        break;
                    case UpdateState.Suicide:   // my job
                        Console.WriteLine("[UpdaterMachine] Suicide");
                        this.suiciding();
                        return;
                    case UpdateState.KillingPCReservation:  // my job
                        Console.WriteLine("[UpdaterMachine] KillPCReservation");
                        this.KillingPCReservation();
                        this.upState = UpdateState.RecificationInit;
                        break;
                    case UpdateState.RecificationInit: // class job
                        Console.WriteLine("[UpdaterMachine] RecificationInit");
                        this.fileRecification.SetFiles(this.shouldBeRemovedFiles, this.shouldBeUpdatedFiles, this.shouldBeCreatedFiles);
                        this.upState = UpdateState.ProcessingShouldBeRemoved;
                        break;
                    case UpdateState.ProcessingShouldBeRemoved: // class job
                        Console.WriteLine("[UpdaterMachine] ProcessingShouldBeRemoved");
                        this.fileRecification.fileRemoving();
                        this.upState = UpdateState.ProcessingShouldBeUpdated;
                        break;
                    case UpdateState.ProcessingShouldBeUpdated: // class job
                        Console.WriteLine("[UpdaterMachine] ProcessingShouldBeUpdated");
                        this.fileRecification.fileUpdating();
                        this.upState = UpdateState.ProcessingShouldBeCreated;
                        break;
                    case UpdateState.ProcessingShouldBeCreated: // class job
                        Console.WriteLine("[UpdaterMachine] ProcessingShouldBeCreated");
                        this.fileRecification.fileCreatring();
                        this.upState = UpdateState.ExecutingPCReservation;
                        break;
                    case UpdateState.ExecutingPCReservation:    // my job
                        Console.WriteLine("[UpdaterMachine] ExecutingPCReservation");
                        this.ExecutingPCReservation();
                        this.upState = UpdateState.Suicide;
                        break;
                }
            }
        }

    }
}