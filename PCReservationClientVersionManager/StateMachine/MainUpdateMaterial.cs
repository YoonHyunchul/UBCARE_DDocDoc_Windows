﻿using System;
using PCReservationClientVersionManager.Domain;
using PCReservationClientVersionManager.Procedure;
using System.Collections.Generic;
namespace PCReservationClientVersionManager.StateMachine
{

    public partial class MainUpdating
    {
        private void initializing()
        {
            this.shouldBeRemovedFiles = new List<JobFile>();
            this.shouldBeUpdatedFiles = new List<JobFile>();
            this.shouldBeCreatedFiles = new List<JobFile>();

            this.fileVersionComparer = new FileVersionComparer();
            this.installedFileVersionSweeper = new InstalledFileVersionSweeper();
            this.lastestFileVersionChecker = new LastestFileVersionChecker();
            this.fileRecification = new FileRecification();
        }

        private void suiciding()
        {
            Console.WriteLine("Kill myself Updater.exe");
        }
        private void KillingPCReservation()
        {
            Console.WriteLine("Kill PCReservation.exe");
        }
        private void ExecutingPCReservation()
        {
            Console.WriteLine("Executing PCReservaion.exe");
        }
    }
}