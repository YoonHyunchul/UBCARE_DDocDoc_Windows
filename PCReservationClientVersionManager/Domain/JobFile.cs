﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Diagnostics;

namespace PCReservationClientVersionManager.Domain
{
    public class JobFile 
    {
        public String FileName { get; private set; }
        public String FileExtension { get; private set; }
        public String CreationTime { get; private set; }
        public String FullPath { get; private set; }
        public String FileVersion { get; private set; }
        public String ProductVersion { get; private set; }


        public JobFile(JobFile fileinfo)
        {
            this.FileName = fileinfo.FileName;
            this.FileExtension = fileinfo.FileExtension;
            this.CreationTime = fileinfo.CreationTime;
            this.FullPath = fileinfo.FullPath;
            this.FileVersion = fileinfo.FileVersion;
            this.ProductVersion = fileinfo.ProductVersion;
        }

        public JobFile(FileInfo fileInfo)
        {
            FileVersionInfo fileVersionInfo = FileVersionInfo.GetVersionInfo(Path.Combine(fileInfo.DirectoryName, fileInfo.Name));
            
            this.FileName = fileInfo.Name;
            this.FileExtension = fileInfo.Extension;
            this.CreationTime = fileInfo.CreationTime.ToString("yyyyMMddHHmmss");
            this.FullPath = fileVersionInfo.FileName;
            this.FileVersion = fileVersionInfo.FileVersion;
            this.ProductVersion = fileVersionInfo.ProductVersion;
        }

        public JobFile(String fileName, String fileExtension, String creationTime, String fullPath, String fileVersion, String productVersion)
        {
            this.FileName = fileName;
            this.FileExtension = fileExtension;
            this.CreationTime = creationTime;
            this.FullPath = fullPath;
            this.FileVersion = fileVersion;
            this.ProductVersion = productVersion;
        }


        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();

            sb.Append("\n[FIle Info] FileName : ");
            sb.Append(this.FileName);

            sb.Append("\n[FIle Info] FileExtension : ");
            sb.Append(this.FileExtension);

            sb.Append("\n[FIle Info] CreationTime : ");
            sb.Append(this.CreationTime);

            sb.Append("\n[FIle Info] FullPath : ");
            sb.Append(this.FullPath);

            sb.Append("\n[FIle Info] FileVersion : ");
            sb.Append(this.FileVersion);

            sb.Append("\n[FIle Info] ProductVersion : ");
            sb.Append(this.ProductVersion);


            return sb.ToString();
        }

    }
}
