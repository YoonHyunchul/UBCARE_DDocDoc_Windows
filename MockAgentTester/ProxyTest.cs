﻿using EMRClientLib.Interface;
using EMRClientLib.Domain;
using Newtonsoft.Json;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EMRProxy;
using Moq;
using System.Net.WebSockets;
using System.Threading;
using BIT.DrBIT.Library;

namespace Mock
{
    [TestFixture]
    class ProxyTest
    {
        //private static IDdocdocAgent agent = new MockAgent();
        //private static AbstractAgentHospital agent = new BITClientOfNewReservation();
        //private static Proxy proxy = new Proxy(agent);

        /*
        [Test]
        public void deleteReservation()
        {
            String strReservation =  "{\"emrReservId\":\"25466\",\"reservationId\":\"57cd682578dfc971d51bfd9c\",\"userName\":\"김종민\",\"userPhone\":\"01066884731\",\"birthDate\":\"19710226\",\"gender\":\"M\",\"userId\":\"579a04cca2511dcf3eefd412\",\"symptomText\":null,\"confirmReservationDatetime\":\"2016-09-06 12:00\",\"category\":null,\"office\":\"PI\",\"staff\":\"PI2\",\"chartNo\":\"30\"}";
            Reservation reservation = JsonConvert.DeserializeObject<Reservation>(strReservation);
            proxy.DeleteReservation(reservation);
        }

        [SetUp]
        public void SetUp()
        {
            agent = new MockAgent();
            proxy = new Proxy(agent);
        }

        [TearDown]
        public void TearDown()
        {
            proxy = null;
            agent = null;
        }
        */

        /*
        [Test]
        public void Request_Login_Test()
        {
            // Given

            // Whend

            // Then
            String hospitalToken = proxy.hospitalTitle;

            Assert.IsNotNull(hospitalToken);
        }

        [Test]
        public void Event_실시간_대기자수_현황_갱신()
        {
            // Given

            // When
            agent.FireChangeAwaiterCounterUsingEMR();

            // Then
        }

        [Test]
        public void Event_현장_예약_전송()
        {
            // Given
            Reservation reservation = new Reservation("emrReservId", "null", "홍길동", "010-1234-5678", "1993-02-08", "M", "gildongHong", "아파요. 마이 아파요.. 정말 아파요", "2016-08-02 14:20", "치과보철과", "김보철", "CharNo123");

            // When
            agent.FireCreateReservationUsingEMR(reservation);

            // Then
        }

        [Test]
        public void Event_확정_예약_시간_변경_전송()
        {
            // Given
            Reservation reservation = new Reservation("emrReservId", "reservationId", "홍길동", "010-1234-5678", "1993-02-08", "M", "gildongHong", "아파요. 마이 아파요.. 정말 아파요", "2016-08-02 14:20", "치과보철과", "김보철", "CharNo123");

            // When
            agent.FireUpdateReservationUsingEMR(reservation);

            // Then
        }

        [Test]
        public void Event_예약_현황_변경분_테스트_변경없는_경우()
        {
            // Given
            List<OfficeReservationStateItem> current = null;
            proxy.lastOfficeReservationStateItemList = null;
            
            // When
            current = proxy.MakeDiffReservationStateItemList(agent.GetReservations(null, null));
            current = proxy.MakeDiffReservationStateItemList(agent.GetReservations(null, null));
            
            // Then
            Assert.IsTrue(current.Count == 0);
        }

        [Test]
        public void Event_예약_현황_변경분_테스트_변경있는_경우()
        {
            // Given
            List<OfficeReservationStateItem> last = null;
            List<OfficeReservationStateItem> current = null;
            proxy.lastOfficeReservationStateItemList = null;
            
            // When
            current = proxy.MakeDiffReservationStateItemList(agent.GetReservations(null, null));
            last = proxy.lastOfficeReservationStateItemList;
            last.RemoveAt(5);
            last.RemoveAt(7);
            current = proxy.MakeDiffReservationStateItemList(agent.GetReservations(null, null));
            
            // Then            
            Assert.IsTrue(current.Count == 2);
        }

        [Test]
        public void WebKit_GetReservationList()
        {
            // Given
            String date = "2016-08-03";
            String[] officeArray = {"치과보철과", "치과교정과"};

            // When
            String jsonMessage = proxy.GetEmrReservations(date, officeArray);

            String expected = "[{\"office\":\"치과보철과\",\"staff\":\"한보철\",\"datetime\":\"2016-08-03 11:00\",\"chartNo\":\"chartNo008\",\"emrReservId\":\"emrReservId008\",\"userName\":\"이몽룡\"},{\"office\":\"치과보철과\",\"staff\":\"한보철\",\"datetime\":\"2016-08-03 12:00\",\"chartNo\":\"chartNo009\",\"emrReservId\":\"emrReservId009\",\"userName\":\"변학도\"},{\"office\":\"치과보철과\",\"staff\":\"한보철\",\"datetime\":\"2016-08-03 17:00\",\"chartNo\":\"chartNo010\",\"emrReservId\":\"emrReservId010\",\"userName\":\"성춘향\"},{\"office\":\"치과교정과\",\"staff\":\"이구강\",\"datetime\":\"2016-08-03 10:10\",\"chartNo\":\"chartNo013\",\"emrReservId\":\"emrReservId013\",\"userName\":\"키티\"},{\"office\":\"치과교정과\",\"staff\":\"이구강\",\"datetime\":\"2016-08-03 11:00\",\"chartNo\":\"chartNo014\",\"emrReservId\":\"emrReservId014\",\"userName\":\"니모\"},{\"office\":\"치과교정과\",\"staff\":\"이구강\",\"datetime\":\"2016-08-03 14:20\",\"chartNo\":\"chartNo015\",\"emrReservId\":\"emrReservId015\",\"userName\":\"도리\"}]";

            // Then            
            Assert.AreEqual(expected, jsonMessage);
        }
         */
    }
}
