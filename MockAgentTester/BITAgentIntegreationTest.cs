﻿using EMRClientLib.Interface;
using EMRClientLib.Domain;
using Newtonsoft.Json;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BIT.DrBIT.Library;


namespace Mock
{
    [TestFixture]
    class BITAgentIntegreationTest
    {
        private AbstractAgentDerive agent = new AbstractAgentDerive();
        
        private ServerMessageFormat message = null;

        [SetUp]
        public void SetUp()
        {
            
            BITClientOfNewDerive bd = new BITClientOfNewDerive();
            agent.agentHospital = bd.bitClientOfHospital;
            agent.agentReceive = bd.bitClientOfReceive;
            agent.agentReservation = bd.bitClientOfReservation;
            message = new ServerMessageFormat();
        }
        
        [Test]
        public void GetHospitalId()
        {
            var result = agent.agentHospital.GetHospitalId().Trim();

            Assert.AreEqual("12345678", result);
        }

        [Test]
        public void GetHospitalPassword()
        {
            var result = agent.agentHospital.GetHospitalPassword().Trim();

            Assert.AreEqual("12345678", result);
        }

        [Test]
        public void IsChartNoAutoGenerate()
        {
            var result = agent.agentHospital.IsChartNoAutoGenerate();

            Assert.IsTrue(result);
        }

        [Test, RequiresSTA]
        public void GetCategoryOfficeStaffs()
        {
            // Given
            List<CategoryOfficeStaff> expected = new List<CategoryOfficeStaff>();

            expected.Add(new CategoryOfficeStaff("치과보철과", "MA", "치과보철과", "MA1", "김보철", "98765"));
            expected.Add(new CategoryOfficeStaff("치과보철과", "MA", "치과보철과", "MA2", "한보철", ""));
            expected.Add(new CategoryOfficeStaff("치과교정과", "OS", "치과교정과", "OS", "비트", ""));
            expected.Add(new CategoryOfficeStaff("구강내과", "PI", "구강내과", "PI1", "김치아", ""));
            expected.Add(new CategoryOfficeStaff("구강내과", "PI", "구강내과", "PI2", "이구강", ""));
            expected.Add(new CategoryOfficeStaff("구강내과", "PI", "구강내과", "PI3", "이치료", ""));

            // When
            var result = agent.agentHospital.GetCategoryOfficeStaffs();

            // Then
            CollectionAssert.AreEquivalent(expected, result);
        }

        [Test]
        public void GetTimetable()
        {
            // Given
            
            // When
            var result = agent.agentHospital.GetTimetable();

            // Then
            Assert.IsNull(result);
        }

        [Test]
        public void GetDayOffSchedule()
        {
            // Given

            // When
            var result = agent.agentHospital.GetDayOffSchedule();

            // Then
            Assert.IsNull(result);
        }

        [Test]
        public void GetCharts()
        {
            // Given

            // When            
            agent.agentHospital.GetCharts("", "1989-07-07", "M");

            // Then
            Assert.AreEqual(5, agent.agentHospital.GetCharts("", "", "").Count());
            Assert.AreEqual(3, agent.agentHospital.GetCharts("김종민", "", "").Count());
            Assert.AreEqual(3, agent.agentHospital.GetCharts("", "1971-02-26", "").Count());
            Assert.AreEqual(5, agent.agentHospital.GetCharts("", "", "M").Count());
            Assert.AreEqual(1, agent.agentHospital.GetCharts("안신환", "1989-07-07", "").Count());
            Assert.AreEqual(1, agent.agentHospital.GetCharts("안신환", "", "M").Count());
            Assert.AreEqual(1, agent.agentHospital.GetCharts("", "1989-07-07", "M").Count());
            Assert.AreEqual(1, agent.agentHospital.GetCharts("안신환", "1989-07-07", "M").Count());
            Assert.IsNull(agent.agentHospital.GetCharts("안신환", "1989-07-07", "F"));            
        }

        [Test]
        public void GetRealTimeAwaiterCount()
        {
            // Given
            List<AwaiterCounter> expected = new List<AwaiterCounter>();

            expected.Add(new AwaiterCounter("PI", "구강내과", "PI1", "김치아", 0));
            expected.Add(new AwaiterCounter("PI", "구강내과", "PI2", "이구강", 0));
            expected.Add(new AwaiterCounter("PI", "구강내과", "PI3", "이치료", 0));
            expected.Add(new AwaiterCounter("OS", "치과교정과", "OS", "비트", 0));
            expected.Add(new AwaiterCounter("MA", "치과보철과", "MA1", "김보철", 0));
            expected.Add(new AwaiterCounter("MA", "치과보철과", "MA2", "한보철", 0));

            // When
            var result = agent.agentHospital.GetRealTimeAwaiterCount();

            // Then
            CollectionAssert.AreEquivalent(expected, result);
        }

        [Test]
        public void GetReservation()
        {
            // Given
            Reservation expected = null;
            // When
            //agent.GetReservation(null);
            //agent.GetReservation("");
            agent.agentReservation.GetReservation("33335414");
            var result = agent.agentReservation.GetReservation("25414");

            // Then
            /* 
             * Procedure name :GetReservation
             * Error Message : 위치 0에 행이 없습니다.
             */
            //Assert.IsNull(agent.GetReservation(""));

            expected = new Reservation("25414", null, "안신환", "010-1234-1111", "1989-07-07", "M", null, "", "2016-08-11 14:20", null, "MA", "치과보철과", "MA1", "김보철", "6", "예약메모", "연속메모");
            Assert.AreEqual(expected, agent.agentReservation.GetReservation("25414"));
        }

        [Test]
        public void GetReservations_By_Date_And_Offices()
        {
            // Given
            List<CategoryOfficeStaff> offices = new List<CategoryOfficeStaff>();

            // When
            
            // Then
            //Assert.AreEqual(1, agent.GetReservations(null, null).Count());
            Assert.AreEqual(1, agent.agentReservation.GetReservations(null, offices).Count());
            Assert.AreEqual(1, agent.agentReservation.GetReservations("", offices).Count());
            Assert.AreEqual(1, agent.agentReservation.GetReservations("2016-10-31", offices).Count());
            Assert.AreEqual(0, agent.agentReservation.GetReservations("2016-09-30", offices).Count());
            Assert.AreEqual(1, agent.agentReservation.GetReservations("2016-10-31", offices).Count());

            offices = new List<CategoryOfficeStaff>();
            offices.Add(new CategoryOfficeStaff("", "PI", "구강내과", "PI1", "김치아", ""));

            Assert.AreEqual(0, agent.agentReservation.GetReservations("", offices).Count());
            Assert.AreEqual(0, agent.agentReservation.GetReservations("2016-10-31", offices).Count());
            Assert.AreEqual(0, agent.agentReservation.GetReservations("2016-09-30", offices).Count());

            offices = new List<CategoryOfficeStaff>();
            offices.Add(new CategoryOfficeStaff("", "MA", "치과보철과", "MA1", "김보철", "98765"));

            Assert.AreEqual(1, agent.agentReservation.GetReservations("", offices).Count());
            Assert.AreEqual(1, agent.agentReservation.GetReservations("2016-10-31", offices).Count());
            Assert.AreEqual(0, agent.agentReservation.GetReservations("2016-09-30", offices).Count());

            offices = new List<CategoryOfficeStaff>();
            offices.Add(new CategoryOfficeStaff("", "PI", "구강내과", "PI1", "김치아", ""));
            offices.Add(new CategoryOfficeStaff("", "MA", "치과보철과", "MA1", "김보철", "98765"));

            Assert.AreEqual(1, agent.agentReservation.GetReservations("", offices).Count());
            Assert.AreEqual(1, agent.agentReservation.GetReservations("2016-10-31", offices).Count());
            Assert.AreEqual(0, agent.agentReservation.GetReservations("2016-09-30", offices).Count());

            offices = new List<CategoryOfficeStaff>();
            offices.Add(new CategoryOfficeStaff("", "MA", "치과보철과", "MA1", "김보철", "98765"));
            offices.Add(new CategoryOfficeStaff("", "PI", "구강내과", "PI1", "김치아", ""));

            Assert.AreEqual(1, agent.agentReservation.GetReservations("", offices).Count());
            Assert.AreEqual(1, agent.agentReservation.GetReservations("2016-10-31", offices).Count());
            Assert.AreEqual(0, agent.agentReservation.GetReservations("2016-09-30", offices).Count());
        }

        [Test]
        public void GetReservations_By_EmrUserId()
        {
            // Given

            // When

            // Then
            Assert.AreEqual(0, agent.agentReservation.GetReservations(null).Count());
            Assert.AreEqual(0, agent.agentReservation.GetReservations("").Count());
            Assert.AreEqual(1, agent.agentReservation.GetReservations("6").Count());
            Assert.AreEqual(0, agent.agentReservation.GetReservations("20").Count());
        }

        [Test]
        public void CreateReservation_And_DeleteReservation()
        {
            Reservation reservation = new Reservation("안신환", "010-1234-1111", "1989-07-07", "M", null, 
                "", "2016-09-12 11:30", null, "Gu", "구강내과", 
                "Lee", "이치료", "6", "예약메모", "연속메모");

            String emrReservId = agent.agentReservation.CreateReservation(reservation);
            Assert.IsNotNull(emrReservId);
			
			reservation = new Reservation("안신환", "010-1234-1111", "1989-07-07", "M", null,
                "", "2016-09-12 11:30", null, "Gu", "구강내과",
                "Lee", "이치료", "6", "예약메모", "연속메모");
            reservation.emrReservId = emrReservId.Trim();

            bool isDeleted = agent.agentReservation.DeleteReservation(reservation);
            Assert.IsTrue(isDeleted);
		}


        [Test]
        public void UpdateReservation()
        {
            Reservation reservation = null;
            Reservation updatedReservation = null; 
            bool isUpdated = false;
            String oldConfirmReservationDatetime = null;

            reservation = agent.agentReservation.GetReservation("25444");
            oldConfirmReservationDatetime = reservation.confirmReservationDatetime;
            reservation.confirmReservationDatetime = "2016-10-10 15:30";

            isUpdated = agent.agentReservation.UpdateReservation(reservation);
            Assert.IsTrue(isUpdated);

            updatedReservation = agent.agentReservation.GetReservation("25444");
            Assert.AreEqual(reservation.confirmReservationDatetime, updatedReservation.confirmReservationDatetime);

            // Rollback
            reservation.confirmReservationDatetime = oldConfirmReservationDatetime;

            isUpdated = agent.agentReservation.UpdateReservation(reservation);
            Assert.IsTrue(isUpdated);

            updatedReservation = agent.agentReservation.GetReservation("25444");
            Assert.AreEqual(reservation.confirmReservationDatetime, updatedReservation.confirmReservationDatetime);
        }

        /*
        [Test]
        public void BITAgent_GetOfficeReservationStateList_예약_현황_조회_날짜없이_진료실없이()
        {
            // Given
            
            // When
            List<OfficeReservationStateItem> result = agent.GetReservations(null, null);

            // Then
            Assert.IsNotNull(result);

            var jsonMessage = JsonConvert.SerializeObject(result, Formatting.None);
            var expected = "[{\"office\":\"구강내과\",\"staff\":\"이치료\",\"datetime\":\"2016-08-05 10:10\",\"chartNo\":\"000001\",\"emrReservId\":\"25404\",\"userName\":\"김비트\"},{\"office\":\"구강내과\",\"staff\":\"이치료\",\"datetime\":\"2016-08-05 13:30\",\"chartNo\":\"8\",\"emrReservId\":\"25423\",\"userName\":\"나재진\"},{\"office\":\"치과교정과\",\"staff\":\"비트\",\"datetime\":\"2016-08-05 16:40\",\"chartNo\":\"19\",\"emrReservId\":\"25433\",\"userName\":\"김종민\"},{\"office\":\"구강내과\",\"staff\":\"이치료\",\"datetime\":\"2016-08-08 14:00\",\"chartNo\":\"8\",\"emrReservId\":\"25405\",\"userName\":\"나재진\"},{\"office\":\"치과보철과\",\"staff\":\"김보철\",\"datetime\":\"2016-08-11 14:20\",\"chartNo\":\"6\",\"emrReservId\":\"25414\",\"userName\":\"안신환\"}]";
            Assert.AreEqual(expected, jsonMessage);
        }

        [Test]
        public void BITAgent_GetOfficeReservationStateList_예약_현황_조회_날짜_진료실없이()
        {
            // Given

            // When
            List<OfficeReservationStateItem> result = agent.GetReservations("2016-08-05", null);

            // Then
            Assert.IsNotNull(result);

            var jsonMessage = JsonConvert.SerializeObject(result, Formatting.None);
            var expected = "[{\"office\":\"구강내과\",\"staff\":\"이치료\",\"datetime\":\"2016-08-05 10:10\",\"chartNo\":\"000001\",\"emrReservId\":\"25404\",\"userName\":\"김비트\"},{\"office\":\"구강내과\",\"staff\":\"이치료\",\"datetime\":\"2016-08-05 13:30\",\"chartNo\":\"8\",\"emrReservId\":\"25423\",\"userName\":\"나재진\"},{\"office\":\"치과교정과\",\"staff\":\"비트\",\"datetime\":\"2016-08-05 16:40\",\"chartNo\":\"19\",\"emrReservId\":\"25433\",\"userName\":\"김종민\"}]";
            Assert.AreEqual(expected, jsonMessage);
        }

        [Test]
        public void BITAgent_GetOfficeReservationStateList_예약_현황_조회_날짜_진료실목록()
        {
            // Given
            List<Dictionary<String, String>> offices = new List<Dictionary<String, String>>();
            Dictionary<string, string> office = null;

            office = new Dictionary<string, string>();

            office.Add("office", "치과보철과");
            office.Add("staff", "김보철");

            offices.Add(office);

            office = new Dictionary<string, string>();

            office.Add("office", "구강내과");
            office.Add("staff", "이치료");

            offices.Add(office);

            // When
            List<OfficeReservationStateItem> result = agent.GetReservations("2016-08-05", offices);

            // Then
            Assert.IsNotNull(result);

            var jsonMessage = JsonConvert.SerializeObject(result, Formatting.None);
            var expected = "[{\"office\":\"구강내과\",\"staff\":\"이치료\",\"datetime\":\"2016-08-05 10:10\",\"chartNo\":\"000001\",\"emrReservId\":\"25404\",\"userName\":\"김비트\"},{\"office\":\"구강내과\",\"staff\":\"이치료\",\"datetime\":\"2016-08-05 13:30\",\"chartNo\":\"8\",\"emrReservId\":\"25423\",\"userName\":\"나재진\"}]";
            Assert.AreEqual(expected, jsonMessage);
        }

        [Test]
        public void BITAgent_GetChartInfoList_01()
        {
            // Given
            var birthDate = "1988-08-29";
            var gender = "M";

            // When
            List<Chart> result = agent.GetCharts(birthDate, gender);

            // Then
            var jsonMessage = JsonConvert.SerializeObject(result, Formatting.None);

            var expected = "[{\"chartNo\":\"000001\",\"name\":\"김비트\",\"phone\":\"\"}]";

            Assert.AreEqual(expected, jsonMessage);
        }

        [Test]
        public void BITAgent_GetChartInfoList_02()
        {
            // Given
            var birthDate = "1989-07-07";
            var gender = "M";

            // When
            List<Chart> result = agent.GetCharts(birthDate, gender);

            // Then
            var jsonMessage = JsonConvert.SerializeObject(result, Formatting.None);

            var expected = "[{\"chartNo\":\"6\",\"name\":\"안신환\",\"phone\":\"010-1234-1111\"},{\"chartNo\":\"8\",\"name\":\"나재진\",\"phone\":\"010-1111-1111\"}]";

            Assert.AreEqual(expected, jsonMessage);
        }

        [Test]
        public void BITAgent_GetChartInfoList_03()
        {
            // Given
            var birthDate = "1989-07-07";
            var gender = "F";

            // When
            List<Chart> result = agent.GetCharts(birthDate, gender);

            // Then
            var jsonMessage = JsonConvert.SerializeObject(result, Formatting.None);

            var expected = "null";

            Assert.AreEqual(expected, jsonMessage);
        }

        [Test]
        public void BITAgent_GetWaitingList()
        {
            // Given

            // When
            List<AwaiterCounter> result = agent.GetRealTimeAwaiterCount();

            // Then
            var jsonMessage = JsonConvert.SerializeObject(result, Formatting.None);

            var expected = "[{\"office\":\"치과교정과\",\"staff\":\"Tom\",\"count\":0},{\"office\":\"치과교정과\",\"staff\":\"Jerry\",\"count\":0},{\"office\":\"구강내과\",\"staff\":\"비트\",\"count\":0},{\"office\":\"치과보철과\",\"staff\":\"김피부\",\"count\":0},{\"office\":\"치과보철과\",\"staff\":\"이피부\",\"count\":0},{\"office\":\"치과보철과\",\"staff\":\"강피부\",\"count\":0}]";

            Assert.IsNotEmpty(result);
        }

        [Test]
        public void BITAgent_GetReservationDetail()
        {
            // Given
            var emrReservId = "25414";

            // When
            Reservation result = agent.GetReservation(emrReservId);

            // Then
            var jsonMessage = JsonConvert.SerializeObject(result, Formatting.None);

            var expected = "{\"emrReservId\":\"25414\",\"reservationId\":\"\",\"userName\":\"안신환\",\"userPhone\":\"010-1234-1111\",\"birthDate\":\"1989-07-07\",\"gender\":\"M\",\"userId\":null,\"symptomText\":\"\",\"confirmReservationDatetime\":\"2016-08-11 14:20\",\"office\":\"치과보철과\",\"staff\":\"김보철\",\"chartNo\":\"6\"}";

            Assert.AreEqual(expected, jsonMessage);
        }

        [Test]
        public void BITAgent_GetCategoryOfficeStaffList()
        {
            // Given

            // When
            List<CategoryOfficeStaff> result = agent.GetCategoryOfficeStaffs();

            // Then
            var jsonMessage = JsonConvert.SerializeObject(result, Formatting.None);

            var expected = "[{\"category\":\"치과보철과\",\"office\":\"치과보철과\",\"staff\":\"김보철\"},{\"category\":\"치과보철과\",\"office\":\"치과보철과\",\"staff\":\"한보철\"},{\"category\":\"치과교정과\",\"office\":\"치과교정과\",\"staff\":\"비트\"},{\"category\":\"구강내과\",\"office\":\"구강내과\",\"staff\":\"김치아\"},{\"category\":\"구강내과\",\"office\":\"구강내과\",\"staff\":\"이구강\"},{\"category\":\"구강내과\",\"office\":\"구강내과\",\"staff\":\"이치료\"}]";

            Assert.AreEqual(expected, jsonMessage);
        }

        [Test]
        public void BITAgent_GetCategoryList()
        {
            // Given

            // When
            List<String> result = agent.GetCategories();

            // Then
            var jsonMessage = JsonConvert.SerializeObject(result, Formatting.None);

            var expected = "[\"구강내과\",\"치과교정과\",\"치과보철과\"]";

            Assert.AreEqual(expected, jsonMessage);
        }

        [Test]
        public void BITAgent_GetTimetable()
        {
            // Given

            // When
            List<Timetable> result = agent.GetTimetable();

            // Then
            Assert.IsNull(result);
        }
        
        [Test]
        public void BITAgent_ConfirmReservation_신환()
        {
            // Given
            // 신환인 경우 차트번호가 null
            //var reservation = new Reservation("홍길동", "010-1234-5678", "1993-02-08", "M", "gildongHong", "아파요. 마이 아파요.. 정말 아파요", "2016-08-08 14:20", "치과보철과", "한보철", null);
            var reservation = new Reservation("주윤식", "010-3220-5510", "19880101", "M", "56b1c169fe8c3303007c0719", "아파요. 마이 아파요.. 정말 아파요", "2016-08-05 09:30", "치과보철과", "한보철", null);

            // When
            String emrReservId = agent.CreateReservation(reservation);

            // Then
            Assert.NotNull(emrReservId);
        }

        [Test]
        public void BITAgent_ConfirmReservation_재진()
        {
            // Given
            //var reservation = new Reservation("나재진", "010-1111-1111", "1989-07-07", "M", "iamoldone", "아파요. 마이 아파요.. 정말 아파요", "2016-08-11 16:20", "치과교정과", "비트", "8");

            var reservation = new Reservation("주윤식", "010-3220-5510", "1988-01-01", "M", "56b1c169fe8c3303007c0719", "아파요. 마이 아파요.. 정말 아파요", "2016-08-05 09:30", "구강내과", "이치료", "20");
            
            // When
            String emrReservId = agent.CreateReservation(reservation);

            // Then
            Assert.NotNull(emrReservId);
        }

        [Test]
        public void BITAgent_CancelReservation_실제_예약()
        {
            // Given
            // SELECT OcmNum, OcmAcpDtm FROM ocminf 
            // Update OcmInf Set OcmComStt='CN' Where OcmNUm= '     25406'
            var emrReservId = "25414";

            // When
            bool result = agent.DeleteReservation(emrReservId);

            // Then
            Assert.IsTrue(result);
        }

        [Test]
        public void BITAgent_CancelReservation_없는_예약()
        {
            // Given
            // Update OcmInf Set OcmComStt='CN' Where OcmNUm= '     99999'
            var emrReservId = "99999";

            // When
            // CF: 없는 예약을 취소하면 False 를 리턴하고 있음
            // "개체 참조가... " MessageBox 가 뜨는 것이 정상
            bool result = agent.DeleteReservation(emrReservId);

            // Then
            Assert.IsFalse(result);
        }

        [Test]
        public void BITAgent_ChangeReservationDatetime()
        {
            // Given
            message.type = "CHANGED_RESERVATION_DATETIME_REQ";
            message.status = "SUCCESS";

            // When
            bool result = agent.UpdateReservation("25414", "2016-08-11 14:20");
            
            // Then
            Assert.IsTrue(result);
        }
        */
    }
}
