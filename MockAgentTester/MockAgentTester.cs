﻿using EMRClientLib.Interface;
using EMRClientLib.Domain;
using Newtonsoft.Json;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mock
{
    [TestFixture]
    class MockAgentTester
    {
        private AbstractAgentDerive agent = new AbstractAgentDerive();
        //private AbstractAgentHospital agent = new MockAgent();
        private ServerMessageFormat message = null;

        [SetUp]
        public void SetUp()
        {
            agent.agentHospital = new MockAgentHospital();
            agent.agentReservation = new MockAgentReservation();
            message = new ServerMessageFormat();
        }

        [Test]
        public void Request_예약_현황_조회_날짜없이_진료과없이()
        {
            // Given
            message.type = "RESERVATION_LIST_RES";
            message.status = "SUCCESS";

            List<Reservation> currentReservationList = agent.agentReservation.GetReservations(null, null);

            List<OfficeReservationState> currentOfficeReservationStateList = new List<OfficeReservationState>();

            var tmp = currentReservationList.GroupBy(x => new { category = x.category, office = x.office, staffId = x.staffId, staff = x.staff }, x => new { confirmReservationDatetime = x.confirmReservationDatetime, patients = new Patient(x.chartNo, x.emrReservId) });

            foreach (var item in tmp)
            {
                OfficeReservationState ors = new OfficeReservationState(item.Key.staffId, item.Key.staff, new List<ReservationState>());

                //ors.reservationStateList.Add(new ReservationState(item.date, item.GroupBy(x => x.datetime, x => x.patients))

                var tmp2 = item.GroupBy(x => x.confirmReservationDatetime, x => x.patients).ToList();

                for (int idx = 0; idx < tmp2.Count(); idx++)
                {
                    ors.reservationStateList.Add(new ReservationState(tmp2[idx].Key, tmp2[idx].Count()));
                }

                currentOfficeReservationStateList.Add(ors);
            }

            message.payload = currentOfficeReservationStateList;

            String jsonMessage = JsonConvert.SerializeObject(message, Formatting.None);

            // When

            // Then
            var expected = "{\"type\":\"RESERVATION_LIST_RES\",\"status\":\"SUCCESS\",\"payload\":[{\"categories\":null,\"staffId\":\"Kim\",\"staff\":\"김보철\",\"reservationState\":[{\"datetime\":\"2016-09-21 13:00\",\"count\":1},{\"datetime\":\"2016-09-21 13:10\",\"count\":1},{\"datetime\":\"2016-09-21 14:10\",\"count\":1},{\"datetime\":\"2016-09-21 15:20\",\"count\":1}]},{\"categories\":null,\"staffId\":\"Han\",\"staff\":\"한보철\",\"reservationState\":[{\"datetime\":\"2016-09-21 12:00\",\"count\":1},{\"datetime\":\"2016-09-21 12:20\",\"count\":1},{\"datetime\":\"2016-09-22 13:10\",\"count\":1},{\"datetime\":\"2016-09-23 11:00\",\"count\":1},{\"datetime\":\"2016-09-23 12:00\",\"count\":1},{\"datetime\":\"2016-09-23 17:00\",\"count\":1}]},{\"categories\":null,\"staffId\":\"Lee\",\"staff\":\"이구강\",\"reservationState\":[{\"datetime\":\"2016-09-22 13:10\",\"count\":1},{\"datetime\":\"2016-09-22 13:20\",\"count\":1},{\"datetime\":\"2016-09-22 13:30\",\"count\":1},{\"datetime\":\"2016-09-23 10:10\",\"count\":1},{\"datetime\":\"2016-09-23 11:00\",\"count\":1},{\"datetime\":\"2016-09-23 14:20\",\"count\":1}]},{\"categories\":null,\"staffId\":\"Han\",\"staff\":\"한보철\",\"reservationState\":[{\"datetime\":\"2016-10-03 14:20\",\"count\":1}]}]}";

            Assert.AreEqual(expected, jsonMessage);
        }

        [Test]
        public void Request_예약_현황_조회_날짜_진료실없이()
        {
            // Given
            message.type = "RESERVATION_LIST_RES";
            message.status = "SUCCESS";

            List<Reservation> currentReservationList = agent.agentReservation.GetReservations("2016-09-21", null);

            List<OfficeReservationState> currentOfficeReservationStateList = new List<OfficeReservationState>();

            var tmp = currentReservationList.GroupBy(x => new { category = x.category, office = x.office, staffId = x.staffId, staff = x.staff }, x => new { confirmReservationDatetime = x.confirmReservationDatetime, patients = new Patient(x.chartNo, x.emrReservId) });

            foreach (var item in tmp)
            {
                OfficeReservationState ors = new OfficeReservationState(item.Key.staffId, item.Key.staff, new List<ReservationState>());

                //ors.reservationStateList.Add(new ReservationState(item.date, item.GroupBy(x => x.datetime, x => x.patients))

                var tmp2 = item.GroupBy(x => x.confirmReservationDatetime, x => x.patients).ToList();

                for (int idx = 0; idx < tmp2.Count(); idx++)
                {
                    ors.reservationStateList.Add(new ReservationState(tmp2[idx].Key, tmp2[idx].Count()));
                }

                currentOfficeReservationStateList.Add(ors);
            }

            message.payload = currentOfficeReservationStateList;

            String jsonMessage = JsonConvert.SerializeObject(message, Formatting.None);

            // When

            // Then
            var expected = "{\"type\":\"RESERVATION_LIST_RES\",\"status\":\"SUCCESS\",\"payload\":[{\"categories\":null,\"staffId\":\"Kim\",\"staff\":\"김보철\",\"reservationState\":[{\"datetime\":\"2016-09-21 13:00\",\"count\":1},{\"datetime\":\"2016-09-21 13:10\",\"count\":1},{\"datetime\":\"2016-09-21 14:10\",\"count\":1},{\"datetime\":\"2016-09-21 15:20\",\"count\":1}]},{\"categories\":null,\"staffId\":\"Han\",\"staff\":\"한보철\",\"reservationState\":[{\"datetime\":\"2016-09-21 12:00\",\"count\":1},{\"datetime\":\"2016-09-21 12:20\",\"count\":1}]}]}";

            Assert.AreEqual(expected, jsonMessage);
        }
        
        [Test]
        public void Request_예약_현황_조회_날짜없이_진료실목록()
        {
            // Given
            message.type = "RESERVATION_LIST_RES";
            message.status = "SUCCESS";

            List<CategoryOfficeStaff> offices = new List<CategoryOfficeStaff>();

            offices.Add(new CategoryOfficeStaff("", "chibo", "치과보철과", "Kim", "김보철", "98765"));
            offices.Add(new CategoryOfficeStaff("", "chikyo", "치과교정과", "Lee", "이구강", ""));

            List<Reservation> currentReservationList = agent.agentReservation.GetReservations(null, offices);

            List<OfficeReservationState> currentOfficeReservationStateList = new List<OfficeReservationState>();

            var tmp = currentReservationList.GroupBy(x => new { category = x.category, office = x.office, staffId = x.staffId, staff = x.staff }, x => new { confirmReservationDatetime = x.confirmReservationDatetime, patients = new Patient(x.chartNo, x.emrReservId) });

            foreach (var item in tmp)
            {
                OfficeReservationState ors = new OfficeReservationState(item.Key.staffId, item.Key.staff, new List<ReservationState>());

                //ors.reservationStateList.Add(new ReservationState(item.date, item.GroupBy(x => x.datetime, x => x.patients))

                var tmp2 = item.GroupBy(x => x.confirmReservationDatetime, x => x.patients).ToList();                               

                for (int idx = 0; idx < tmp2.Count(); idx++)
                {
                    ors.reservationStateList.Add(new ReservationState(tmp2[idx].Key, tmp2[idx].Count()));
                }
                
                currentOfficeReservationStateList.Add(ors);
            }

            message.payload = currentOfficeReservationStateList;
            String jsonMessage = JsonConvert.SerializeObject(message, Formatting.None);

            // When

            // Then
            var expected = "{\"type\":\"RESERVATION_LIST_RES\",\"status\":\"SUCCESS\",\"payload\":[{\"categories\":null,\"staffId\":\"Kim\",\"staff\":\"김보철\",\"reservationState\":[{\"datetime\":\"2016-09-21 13:00\",\"count\":1},{\"datetime\":\"2016-09-21 13:10\",\"count\":1},{\"datetime\":\"2016-09-21 14:10\",\"count\":1},{\"datetime\":\"2016-09-21 15:20\",\"count\":1}]},{\"categories\":null,\"staffId\":\"Lee\",\"staff\":\"이구강\",\"reservationState\":[{\"datetime\":\"2016-09-22 13:10\",\"count\":1},{\"datetime\":\"2016-09-22 13:20\",\"count\":1},{\"datetime\":\"2016-09-22 13:30\",\"count\":1},{\"datetime\":\"2016-09-23 10:10\",\"count\":1},{\"datetime\":\"2016-09-23 11:00\",\"count\":1},{\"datetime\":\"2016-09-23 14:20\",\"count\":1}]}]}";
            
            Assert.AreEqual(expected, jsonMessage);
        }

        [Test]
        public void Request_예약_현황_조회_날짜_진료실목록()
        {
            // Given
            message.type = "RESERVATION_LIST_RES";
            message.status = "SUCCESS";

            List<CategoryOfficeStaff> offices = new List<CategoryOfficeStaff>();

            offices.Add(new CategoryOfficeStaff("", "chibo", "치과보철과", "Kim", "김보철", "98765"));
            offices.Add(new CategoryOfficeStaff("", "chikyo", "치과교정과", "Lee", "이구강", ""));

            message.payload = agent.agentReservation.GetReservations("2016-08-05", offices);
            String jsonMessage = JsonConvert.SerializeObject(message, Formatting.None);

            // When

            // Then
            var expected = "{\"type\":\"RESERVATION_LIST_RES\",\"status\":\"SUCCESS\",\"payload\":[]}";

            Assert.AreEqual(expected, jsonMessage);
        }
        
        [Test]
        public void Request_차트_번호_조회()
        {
            // Given
            message.type = "CHART_INFO_LIST_RES";
            message.status = "SUCCESS";

            message.payload = agent.agentHospital.GetCharts(null, "1993-05-13", "M");
            String jsonMessage = JsonConvert.SerializeObject(message, Formatting.None);

            // When

            // Then
            var expected = "{\"type\":\"CHART_INFO_LIST_RES\",\"status\":\"SUCCESS\",\"payload\":[{\"chartNo\":\"12341\",\"emrUserId\":\"emrUsrId01\",\"name\":\"홍길동\",\"phone\":\"010-1234-1231\",\"birthDate\":\"1999-09-01\",\"gender\":\"M\",\"address\":\"서울\"},{\"chartNo\":\"12342\",\"emrUserId\":\"emrUsrId02\",\"name\":\"일지매\",\"phone\":\"010-1234-1232\",\"birthDate\":\"1999-09-02\",\"gender\":\"M\",\"address\":\"부산\"},{\"chartNo\":\"12343\",\"emrUserId\":\"emrUsrId03\",\"name\":\"설현\",\"phone\":\"010-1234-1233\",\"birthDate\":\"1999-09-03\",\"gender\":\"F\",\"address\":\"대구\"},{\"chartNo\":\"12344\",\"emrUserId\":\"emrUsrId04\",\"name\":\"강감찬\",\"phone\":\"010-1234-1234\",\"birthDate\":\"1999-09-04\",\"gender\":\"M\",\"address\":\"대전\"},{\"chartNo\":\"12345\",\"emrUserId\":\"emrUsrId05\",\"name\":\"배수지\",\"phone\":\"010-1234-1235\",\"birthDate\":\"2001-09-05\",\"gender\":\"F\",\"address\":\"제주\"}]}";

            Assert.AreEqual(expected, jsonMessage);
        }

        [Test]
        public void Request_대기자_목록_조회()
        {
            // Given
            message.type = "RESERVATION_LIST_RES";
            message.status = "SUCCESS";

            List<AwaiterCounter> awaiterCounterList = agent.agentHospital.GetRealTimeAwaiterCount();

            //var officeAwaiterCounter = awaiterCounterList.GroupBy(x => x.office, x => x.count).Select(group => new { office = group.Key, count = group.Sum() });
            var officeAwaiterCounter = awaiterCounterList
                .GroupBy(x => x.office, x => x.count)
                .Select(group => new { office = group.Key, count = group.Sum(), detail = new List<AwaiterCounter>() })
                .ToList();

            officeAwaiterCounter
                .ForEach(x => awaiterCounterList
                    .ForEach(y => { if (x.office.Equals(y.office)) x.detail.Add(y); }));

            var totalAwaiterCount = officeAwaiterCounter.Sum(group => group.count);

            message.payload = new { count = totalAwaiterCount, detail = officeAwaiterCounter };

            String jsonMessage = JsonConvert.SerializeObject(message, Formatting.None);

            // When

            // Then
            var expected = "{\"type\":\"RESERVATION_LIST_RES\",\"status\":\"SUCCESS\",\"payload\":{\"count\":8,\"detail\":[{\"office\":\"치과보철과\",\"count\":3,\"detail\":[{\"officeId\":\"chibo\",\"office\":\"치과보철과\",\"staffId\":\"Kim\",\"staff\":\"김보철\",\"count\":2},{\"officeId\":\"chibo\",\"office\":\"치과보철과\",\"staffId\":\"Han\",\"staff\":\"한보철\",\"count\":1}]},{\"office\":\"치과교정과\",\"count\":5,\"detail\":[{\"officeId\":\"chikyo\",\"office\":\"치과교정과\",\"staffId\":\"Lee\",\"staff\":\"이구강\",\"count\":5}]}]}}";

            Assert.AreEqual(expected, jsonMessage);
        }

        [Test]
        public void Request_예약_상세_조회()
        {
            // Given
            message.type = "RESERVATION_DETAIL_RES";
            message.status = "SUCCESS";

            String emrReservId = "emrReserveId006";

            // When
            // 예약 상세 조회
            Reservation reservation = agent.agentReservation.GetReservation(emrReservId);

            message.payload = reservation;

            String jsonMessage = JsonConvert.SerializeObject(message, Formatting.None);

            // Then
            var expected = "{\"type\":\"RESERVATION_DETAIL_RES\",\"status\":\"SUCCESS\",\"payload\":{\"emrReservId\":\"emrReserveId006\",\"reservationId\":null,\"userName\":\"홍길동\",\"userPhone\":\"010-1234-1206\",\"birthDate\":\"1999-09-06\",\"gender\":\"M\",\"userId\":\"hg001\",\"symptomText\":\"\",\"confirmReservationDatetime\":\"2016-09-21 12:20\",\"category\":\"치과보철과\",\"officeId\":\"chibo\",\"office\":\"치과보철과\",\"staffId\":\"Han\",\"staff\":\"한보철\",\"chartNo\":\"chartNo006\",\"memo\":\"예약메모\",\"comment\":\"연속메모\"}}";

            Assert.AreEqual(expected, jsonMessage);
        }

        [Test]
        public void Request_예약_확정()
        {
            // Given
            message.type = "CONFIRM_RESERVATION_RES";
            message.status = "SUCCESS";

            Reservation reservation = new Reservation("홍길동", "010-1234-5678", "1993-02-08", "M", "gildongHong", "아파요. 마이 아파요.. 정말 아파요", "2016-08-02 14:20", null, "chi", "치과보철과", "kim", "김보철", "CharNo123", "예약메모", "연속메모");

            // When
            // 예약 상세 조회
            String emrReservId = agent.agentReservation.CreateReservation(reservation);

            message.payload = new { emrReservId = emrReservId };

            String jsonMessage = JsonConvert.SerializeObject(message, Formatting.None);

            // Then
            Assert.IsNotNull(emrReservId);
        }

        [Test]
        public void Request_진료_과목_요청()
        {
            // Given
            message.type = "CATEGORY_LIST_RES";
            message.status = "SUCCESS";

            // When
            // 진료 과목 요청
            message.payload = agent.agentHospital.GetCategoryOfficeStaffs().Select(x => x.category).Distinct();

            String jsonMessage = JsonConvert.SerializeObject(message, Formatting.None);

            // Then
            var expected = "{\"type\":\"CATEGORY_LIST_RES\",\"status\":\"SUCCESS\",\"payload\":[\"치과보철과\",\"치과교정과\"]}";

            Assert.AreEqual(expected, jsonMessage);
        }

        [Test]
        public void Request_진료_시간_요청()
        {
            // Given
            message.type = "TIMETABLE_RES";
            message.status = "SUCCESS";

            // When
            // 진료 시간 요청
            message.payload = agent.agentHospital.GetTimetable();

            String jsonMessage = JsonConvert.SerializeObject(message, Formatting.None);

            // Then
            var expected = "{\"type\":\"TIMETABLE_RES\",\"status\":\"SUCCESS\",\"payload\":[{\"title\":\"월요일\",\"startTime\":\"09:00\",\"endTime\":\"18:00\",\"breakTimes\":[{\"title\":\"점심\",\"startTime\":\"12:00\",\"endTime\":\"13:00\"},{\"title\":\"저녁\",\"startTime\":\"18:00\",\"endTime\":\"19:00\"}]},{\"title\":\"화요일\",\"startTime\":\"09:00\",\"endTime\":\"20:00\",\"breakTimes\":[{\"title\":\"점심\",\"startTime\":\"12:00\",\"endTime\":\"13:00\"},{\"title\":\"저녁\",\"startTime\":\"18:00\",\"endTime\":\"19:00\"}]},{\"title\":\"목요일\",\"startTime\":\"09:00\",\"endTime\":\"20:00\",\"breakTimes\":[{\"title\":\"점심\",\"startTime\":\"12:00\",\"endTime\":\"13:00\"},{\"title\":\"저녁\",\"startTime\":\"18:00\",\"endTime\":\"19:00\"}]},{\"title\":\"금요일\",\"startTime\":\"09:00\",\"endTime\":\"20:00\",\"breakTimes\":[{\"title\":\"점심\",\"startTime\":\"12:00\",\"endTime\":\"13:00\"},{\"title\":\"저녁\",\"startTime\":\"18:00\",\"endTime\":\"19:00\"}]}]}";

            Assert.AreEqual(expected, jsonMessage);
        }
        
        [Test]
        public void Request_확정_예약_취소_Success()
        {
            // Given
            message.type = "CANCEL_RESERVATION_RES";
            message.status = "SUCCESS";

            Reservation reservation = new Reservation();
            reservation.emrReservId = "5791f4e04687f0240bf75a8a";

            // When
            bool result = agent.agentReservation.DeleteReservation(reservation);

            if (result)
            {
                message.status = "SUCCESS";
            }
            else
            {
                message.status = "ERROR";
                message.payload = new { message = "존재하지 않는 똑닥 예약 아이디(reservationId) 입니다." };
            }

            String jsonMessage = JsonConvert.SerializeObject(message, Formatting.None);

            // Then
            var expected = "{\"type\":\"CANCEL_RESERVATION_RES\",\"status\":\"SUCCESS\",\"payload\":null}";

            Assert.AreEqual(expected, jsonMessage);
        }

        [Test]
        public void Request_확정_예약_취소_Fail()
        {
            // Given
            message.type = "CANCEL_RESERVATION_RES";
            message.status = "SUCCESS";

            Reservation reservation = new Reservation();
            reservation.emrReservId = "fail";

            // When
            bool result = agent.agentReservation.DeleteReservation(reservation);

            if (result)
            {
                message.status = "SUCCESS";
            }
            else
            {
                message.status = "ERROR";
                message.payload = new { message = "존재하지 않는 똑닥 예약 아이디(reservationId) 입니다." };
            }

            String jsonMessage = JsonConvert.SerializeObject(message, Formatting.None);

            // Then
            var expected = "{\"type\":\"CANCEL_RESERVATION_RES\",\"status\":\"ERROR\",\"payload\":{\"message\":\"존재하지 않는 똑닥 예약 아이디(reservationId) 입니다.\"}}";

            Assert.AreEqual(expected, jsonMessage);
        }

        [Test]
        public void Request_진료실_의사_리스트_전송()
        {
            // Given
            message.type = "OFFICE_DOCTOR_LIST_REQ";

            // When
            List<CategoryOfficeStaff> list = agent.agentHospital.GetCategoryOfficeStaffs();

            /*
            var categoryOfficeStaffs = result
                // 진료실목별
                .GroupBy(x => x.category)
                .Select(group => new
                {
                    category = group.Key,
                    offices = group
                        // 진료실별
                        .GroupBy(y => (y.office))
                        .Select(g2 => new
                        {
                            office = g2.Key,
                            // 직원(주치의) 리스트
                            staffs = g2
                                .Select(y => new 
                                { 
                                    staff = y.staff 
                                })
                        })
                });

            message.payload = categoryOfficeStaffs;

            String jsonMessage = JsonConvert.SerializeObject(message, Formatting.None);

            // Then
            var expected = "{\"type\":\"CATEGORY_OFFICE_DOCTOR_LIST_REQ\",\"status\":null,\"payload\":[{\"category\":\"치과보철과\",\"offices\":[{\"office\":\"치과보철과\",\"staffs\":[{\"staff\":\"김보철\"},{\"staff\":\"한보철\"}]}]},{\"category\":\"치과교정과\",\"offices\":[{\"office\":\"치과교정과\",\"staffs\":[{\"staff\":\"이구강\"}]}]}]}";
             */

            var officeStaffList = list
                .GroupBy(x => x.office)
                .Select(group => new
                    {
                        office = group.Key,
                        staffs = group.Select(y => y.staff)
                    });

            message.payload = officeStaffList;

            String jsonMessage = JsonConvert.SerializeObject(message, Formatting.None);

            // Then
            var expected = "{\"type\":\"OFFICE_DOCTOR_LIST_REQ\",\"status\":null,\"payload\":[{\"office\":\"치과보철과\",\"staffs\":[\"김보철\",\"한보철\"]},{\"office\":\"치과교정과\",\"staffs\":[\"한보철\",\"이구강\"]}]}";

           Assert.AreEqual(expected, jsonMessage);
        }

        [Test]
        public void Request_확정_예약_변경()
        {
            // Given
            message.type = "CHANGED_RESERVATION_DATETIME_REQ";
            message.status = "SUCCESS";

            Reservation reservation = new Reservation();
            reservation.emrReservId = "5791f4e04687f0240bf75a8a";
            reservation.confirmReservationDatetime = "2016-08-11 14:20";

            // When
            bool result = agent.agentReservation.UpdateReservation(reservation);

            if (result)
            {
                message.status = "SUCCESS";
            }
            else
            {
                message.status = "ERROR";
                message.payload = new { message = "존재하지 않는 똑닥 예약 아이디(reservationId) 입니다." };
            }

            String jsonMessage = JsonConvert.SerializeObject(message, Formatting.None);

            // Then
            var expected = "{\"type\":\"CHANGED_RESERVATION_DATETIME_REQ\",\"status\":\"SUCCESS\",\"payload\":null}";

            Assert.AreEqual(expected, jsonMessage);
        }
    }
}