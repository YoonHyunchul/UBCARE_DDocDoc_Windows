﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using EMRProxy;
using EMRClientLib;
using EMRClientLib.Domain;
using EMRClientLib.Interface;
using Newtonsoft.Json;
using CefSharp.Example;
using CefSharp.WinForms;
using CefSharp;
using System.Resources;
using System.Globalization;

/* https://bitbucket.org/chromiumembedded/cef
 * https://github.com/cefsharp/CefSharp/tree/b8eaa573b37962973c1ccc5a37d86f1183b2e2f9 for .NET 3.5
 * https://github.com/cefsharp/CefSharp/wiki/Building-CefSharp
 * https://github.com/cefsharp/CefSharp/wiki/Frequently-asked-questions#3-how-do-you-expose-a-net-class-to-javascript
 * 
 * http://webkitdotnet.sourceforge.net/index.php
 * http://webkitdotnet.sourceforge.net/basics.php
 * Mozilla/5.0 (Windows; U; Windows NT 6.2; ko-KR) AppleWebKit/533+ (KHTML, like Gecko)
 * http://www.useragentstring.com/index.php
 * Firefox 5.0 Aurora	April 13, 2011
 * https://en.wikipedia.org/wiki/Firefox_release_history
 * 
 * http://stackoverflow.com/questions/4146687/using-webkit-net-to-call-a-c-sharp-function-from-javascript
 */
namespace EMRUserControl
{
    /**
     * @brief EMR 과 통신하기 위한 Proxy 와 WebView 를 내장한 윈도우 컨트롤
     */
    public partial class EMRWebView : UserControl, IExampleView
    {
        /// EMR 과 통신을 Proxy
        private IProxy proxy = null;
        private AbstractAgentDerive agent = null;

        /// Window Tray Balloon Text 표기를 위해 public 으로 선언
        /// 크롬 브라우저와 통신을 위한 프록시
        public WebViewProxy webViewProxy = null;
        private WebView webView;
        private String webClientUrl;


        /**
         * @brief 생성자
         */
        public EMRWebView()
        {
            InitializeComponent();
        }

        public void ReLoad()
        {
            this.webView.Load(webClientUrl);
            
        }

        public void Load(String url)
        {
            this.webView.Load(url);
            
        }

        /**
         * @brief 생성 후 초기화 진행
         * @param agent: IDdocdocAgent 인터페이스 구현체(각 EMR 업체별 구현 필요)
         * @param startUrl: 예약 서버 URL
         */
        public void init(AbstractAgentDerive agent, String startUrl)
        {
            
            this.agent = agent;
            if (this.agent == null)
            {
                MessageBox.Show(Properties.Resources.EMRInterfaceAgentModuleNotExistMessage, Properties.Resources.EMRInterfaceAgentModuleNotExistMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Error);
                Environment.Exit(0);
            }


            this.proxy = new Proxy(this.agent);
            this.webClientUrl = startUrl;
            
            this.webView = new WebView(webClientUrl, new BrowserSettings());

            this.webViewProxy = new WebViewProxy(this.proxy, this.webView, this.notifyIcon);

            //자바스크립트와 usercontrol 사이의 통신 객체
            this.webView.RegisterJsObject(Properties.Settings1.Default.JSCaller, this.webViewProxy);
            //this.webView.Invoke()

            this.webView.Dock = DockStyle.Fill;
            this.Controls.Add(webView);
        }

        public void ShowDebbger()
        {
            this.webView.ShowDevTools();
        }

        public event EventHandler ShowDevToolsActivated
        {
            add { }
            remove { }
        }

        public event EventHandler CloseDevToolsActivated
        {
            add { }
            remove { }
        }

        public event EventHandler ExitActivated
        {
            add { }
            remove { }
        }

        public event EventHandler UndoActivated
        {
            add { }
            remove { }
        }

        public event EventHandler RedoActivated
        {
            add { }
            remove { }
        }

        public event EventHandler CutActivated
        {
            add { }
            remove { }
        }

        public event EventHandler CopyActivated
        {
            add { }
            remove { }
        }

        public event EventHandler PasteActivated
        {
            add { }
            remove { }
        }

        public event EventHandler DeleteActivated
        {
            add { }
            remove { }
        }

        public event EventHandler SelectAllActivated
        {
            add { }
            remove { }
        }

        public event EventHandler TestResourceLoadActivated
        {
            add { }
            remove { }
        }

        public event EventHandler TestSchemeLoadActivated
        {
            add { }
            remove { }
        }

        public event EventHandler TestExecuteScriptActivated
        {
            add { }
            remove { }
        }

        public event EventHandler TestEvaluateScriptActivated
        {
            add { }
            remove { }
        }

        public event EventHandler TestBindActivated
        {
            add { }
            remove { }
        }

        public event EventHandler TestConsoleMessageActivated
        {
            add { }
            remove { }
        }

        public event EventHandler TestTooltipActivated
        {
            add { }
            remove { }
        }

        public event EventHandler TestPopupActivated
        {
            add { }
            remove { }
        }

        public event EventHandler TestLoadStringActivated
        {
            add { }
            remove { }
        }

        public event EventHandler TestCookieVisitorActivated
        {
            add { }
            remove { }
        }

        public event Action<object, string> UrlActivated;

        public event EventHandler BackActivated
        {
            add { }
            remove { }
        }

        public event EventHandler ForwardActivated
        {
            add { }
            remove { }
        }

        public void SetTitle(string title)
        {
            Text = title;
        }

        public void SetAddress(string address)
        {

        }

        public void SetCanGoBack(bool can_go_back)
        {

        }

        public void SetCanGoForward(bool can_go_forward)
        {

        }

        public void SetIsLoading(bool is_loading)
        {
            HandleToolStripLayout();
        }

        public void ExecuteScript(string script)
        {
            webView.ExecuteScript(script);
        }

        public object EvaluateScript(string script)
        {
            return webView.EvaluateScript(script);
        }

        public void DisplayOutput(string output)
        {

        }

        private void HandleToolStripLayout(object sender, LayoutEventArgs e)
        {
            HandleToolStripLayout();
        }

        private void HandleToolStripLayout()
        {

        }

        private void HandleGoButtonClick(object sender, EventArgs e)
        {

        }

        private void UrlTextBoxKeyUp(object sender, KeyEventArgs e)
        {

        }

        private void AboutToolStripMenuItemClick(object sender, EventArgs e)
        {

        }

        private void notifyIcon_BalloonTipClicked(object sender, EventArgs e)
        {
            // Windows Forms 컨트롤에 크로스 스레드 호출을 만든 방법
            // https://msdn.microsoft.com/ko-kr/library/ms171728(v=vs.110).aspx
            // http://happyguy81.tistory.com/59
            Form parentForm = this.FindForm();

            if (parentForm.WindowState == FormWindowState.Minimized)
                this.Focus();
        }
    }

    /*
     * 인자에 기본값을 설정해도 CefSharp 의 JavaScript 에서는 인자의 갯수가 맞아야 메소드 호출 가능
     */
    public class WebViewProxy
    {
        private IProxy proxy = null;
        private WebView webView = null;
        private NotifyIcon notifyIcon = null;
        private String dateFormat = "yyyy-MM-dd";
        private ConsoleColor eventConsoleColor = ConsoleColor.Yellow;
        private ConsoleColor proxyConsoleColor = ConsoleColor.Magenta;
        public WebViewProxy(IProxy proxy, WebView webView, NotifyIcon notifyIcon)
        {
            
            this.proxy = proxy;
            this.webView = webView;
            this.notifyIcon = notifyIcon;

            // EMR 에서 진료 대기자 수 변동 시 이벤트 핸들러
            this.proxy.AddListenerOnChangeRealtimeAwaiterCounterUsingEMR(new DelegateChangeRealtimeAwaiterCounterFromEMR(EventHandlerForChangeRealtimeAwaiterCounterUsingEMR));
            // EMR 에서 예약을 생성한 경우 이벤트 핸들러
            this.proxy.AddListenerOnCreateReservationUsingEMR(new DelegateCreateReservationFromEMR(EventHandlerForCreateReservationUsingEMR));
            // EMR 에서 예약을 변경한 경우 이벤트 핸들러
            this.proxy.AddListenerOnUpdateReservationUsingEMR(new DelegateUpdateReservationFromEMR(EventHandlerForUpdateReservationUsingEMR));
            // EMR 에서 예약을 삭제한 경우 이벤트 핸들러
            this.proxy.AddListenerOnDeleteReservationUsingEMRR(new DelegateDeleteReservationFromEMR(EventHandlerForDeleteReservationUsingEMR));
            // EMR 에서 예약을 접수한 경우 이벤트 핸들러
            this.proxy.AddListenerOnReceiveReservationUsingEMR(new DelegateReceiveReservationFromEMR(EventHandlerForReceiveReservationUsingEMR));
            // EMR 에서 예약을 수납한 경우 이벤트 핸들러
            this.proxy.AddListenerOnReceiptReservationUsingEMR(new DelegateReceiptReservationFromEMR(EventHandlerForReceiptReservationUsingEMR));
        }

        // 실시간 대기자수 변경
        public void EventHandlerForChangeRealtimeAwaiterCounterUsingEMR()
        {
            Console.ForegroundColor = eventConsoleColor;
            Console.WriteLine("[Event] AwiterCounter");
            Console.ResetColor();
            var script = "eventListenerFromAgentProxy('AwaiterCounter', '')";

            this.webView.ExecuteScript(script);
        }

        // EMR 예약 생성
        public void EventHandlerForCreateReservationUsingEMR(Reservation reservation)
        {
            Console.ForegroundColor = eventConsoleColor;
            Console.WriteLine("[Event] CreateReservation");
            Console.WriteLine(reservation);
            Console.ResetColor();
            var script = "eventListenerFromAgentProxy('CreateReservation', '" + JsonConvert.SerializeObject(reservation) + "')";

            this.webView.ExecuteScript(script);
        }

        // EMR 예약 수정
        public void EventHandlerForUpdateReservationUsingEMR(Reservation reservation)
        {
            Console.ForegroundColor = eventConsoleColor;
            Console.WriteLine("[Event] UpdateReservation");
            Console.WriteLine(reservation);
            Console.ResetColor();
            var script = "eventListenerFromAgentProxy('UpdateReservation', '" + JsonConvert.SerializeObject(reservation) + "')";

            this.webView.ExecuteScript(script);
        }

        // EMR 예약 삭제
        public void EventHandlerForDeleteReservationUsingEMR(Reservation reservation)
        {
            Console.ForegroundColor = eventConsoleColor;
            Console.WriteLine("[Event] DeleteReservation");
            Console.WriteLine(reservation);
            Console.ResetColor();
            var script = "eventListenerFromAgentProxy('DeleteReservation', '" + JsonConvert.SerializeObject(reservation) + "')";

            this.webView.ExecuteScript(script);
        }

        

        // EMR 예약 to 접수
        public void EventHandlerForReceiveReservationUsingEMR(Reservation reservation)
        {
            Console.ForegroundColor = eventConsoleColor;
            Console.WriteLine("[Event] Reservation to Receipt");
            Console.WriteLine(reservation);
            Console.ResetColor();
            var script = "eventListenerFromAgentProxy('ReceiveReservation', '" + JsonConvert.SerializeObject(reservation) + "')";

            this.webView.ExecuteScript(script);
        }
        
        // EMR 예약 to (접수 to) 수납 대기
        public void EventHandlerForReceiptReservationUsingEMR(Reservation reservation)
        {
            Console.ForegroundColor = eventConsoleColor;
            Console.WriteLine("[Event] Reservation to Complete");
            Console.WriteLine(reservation);
            Console.ResetColor();
            var script = "eventListenerFromAgentProxy('ReceiptReservation', '" + JsonConvert.SerializeObject(reservation) + "')";

            this.webView.ExecuteScript(script);
        }

        #region 4 PC클라이언트

        public void OpenBrowser(String url)
        {
            System.Diagnostics.Process.Start("iexplore.exe", url); 
        }
        public String getHospitalInfo()
        {
            Console.ForegroundColor = proxyConsoleColor;
            Console.WriteLine("[PROXY] getHospital 함수처리 실행");
            Console.ResetColor();
            try
            {
                var hospitalInfo = new
                {
                    hospitalId = proxy.HospitalId,
                    hospitalPassword = proxy.HospitalPassword,
                    isChartNoAutoGenerate = proxy.IsChartNoAutoGenerate(),
                    useComment = proxy.UseComment()
                };

                String jsonString = JsonConvert.SerializeObject(hospitalInfo);

                return jsonString;
            }
            catch (Exception ex)
            {
                MessageBox.Show(Properties.Resources.GetHopitalInfoFailMessage,Properties.Resources.EMRAgentInterfaceModuleErrorCaption,MessageBoxButtons.OK,MessageBoxIcon.Error);
                Environment.Exit(0);
                return "";
            }
        }

        public String getStaffs()
        {
            Console.ForegroundColor = proxyConsoleColor;
            Console.WriteLine("[PROXY] getStaffs 함수처리 실행");
            Console.ResetColor();
            try
            {
                var list = proxy.GetStaffs();

                String jsonString = JsonConvert.SerializeObject(list);

                return jsonString;
            }
            catch (Exception ex)
            {
                MessageBox.Show(Properties.Resources.GetStaffsFailMessage, Properties.Resources.EMRAgentInterfaceModuleErrorCaption, MessageBoxButtons.OK, MessageBoxIcon.Error);
                Environment.Exit(0);
                return "";
            }
        }

        public String getCharts(String name, String birthdate, String gender)
        {
            Console.ForegroundColor = proxyConsoleColor;
            Console.WriteLine("[PROXY] getCharts 함수처리 실행 " + name + " " +birthdate +" "+gender);
            Console.ResetColor();
            try
            {
                name = String.IsNullOrEmpty(name) ? null : name;
                birthdate = String.IsNullOrEmpty(birthdate) ? null : birthdate;
                gender = String.IsNullOrEmpty(gender) ? null : gender;

                List<Chart> list = new List<Chart>();

                if (String.IsNullOrEmpty(name) && String.IsNullOrEmpty(birthdate))
                {

                }
                else
                {
                    list = proxy.GetCharts(name, birthdate, gender);
                }

                String jsonString = JsonConvert.SerializeObject(list);

                return jsonString;
            }
            catch (Exception ex)
            {
                MessageBox.Show(Properties.Resources.GetChartsFailMessage, Properties.Resources.EMRAgentInterfaceModuleErrorCaption, MessageBoxButtons.OK, MessageBoxIcon.Error);
                Environment.Exit(0);
                return "[]";
            }
        }


        public String getLastestChartNO()
        {
            Console.ForegroundColor = proxyConsoleColor;
            Console.WriteLine("[PROXY] getLastestChartNO 함수처리 실행 ");
            Console.ResetColor();
            try
            {
                String lastChartNO = String.Empty;

                lastChartNO = proxy.GetLastestChartNO();
                String result = "{\"result\":\"true\",\"code\":\"" + null + "\",\"data\":" + lastChartNO + "}";
                return result;

            }
            catch(Exception e)
            {
                String result = "{\"result\":\"false\",\"code\":\"" + null + "\",\"data\":" + null + "}";
                return result;
            }
        }

        public String getReservation(String emrReservId)
        {
            Console.ForegroundColor = proxyConsoleColor;
            Console.WriteLine("[PROXY] getReservation 함수처리 실행, emrReservId : " + emrReservId);
            Console.ResetColor();
            try
            {
                if (String.IsNullOrEmpty(emrReservId))
                {
                    return "";
                }

                Reservation reservation = proxy.GetReservation(emrReservId);

                String jsonString = (reservation == null)? "": JsonConvert.SerializeObject(reservation);

                return jsonString;
            }
            catch (Exception ex)
            {
                MessageBox.Show(Properties.Resources.GetReservationFailMessage, Properties.Resources.EMRAgentInterfaceModuleErrorCaption,MessageBoxButtons.OK,MessageBoxIcon.Error);
                Environment.Exit(0);
                return "";
            }
        }

        public String getReservations(String startDate = "", String endDate = "")
        {
            Console.ForegroundColor = proxyConsoleColor;
            Console.WriteLine("[PROXY] getReservations 함수처리 실행, Date : " + startDate + "~" + endDate);
            Console.ResetColor();
            try
            {
                startDate = String.IsNullOrEmpty(startDate) ? DateTime.Now.ToString(this.dateFormat) : startDate;
                endDate = String.IsNullOrEmpty(endDate) ? DateTime.Now.AddDays(1).ToString(this.dateFormat) : endDate;

                List<Reservation> list = proxy.GetReservations(startDate,endDate, null);

                String jsonString = JsonConvert.SerializeObject(list);

                return jsonString;
            }
            catch (Exception ex)
            {
                MessageBox.Show(Properties.Resources.GetReservationFailMessage, Properties.Resources.EMRAgentInterfaceModuleErrorCaption,MessageBoxButtons.OK,MessageBoxIcon.Error);
                Environment.Exit(0);
                return "[]";
            }
        }

        public String createReservation(String strReservation)
        {
            Console.ForegroundColor = proxyConsoleColor;
            Console.WriteLine("[PROXY] createReservation 함수처리 실행, strReservation : " + strReservation);
            Console.ResetColor();
            try
            {
                if (String.IsNullOrEmpty(strReservation))
                {
                    return "";
                }

                Reservation reservation = JsonConvert.DeserializeObject<Reservation>(strReservation);
                
                String emrReservId = proxy.CreateReservation(reservation);

                String Result = "{\"result\":\"true\",\"code\":\"" + null + "\",\"data\":\"" + emrReservId + "\"}";

                return Result;
            }
            catch (Exception ex)
            {
                switch (ex.Message)
                {
                    case "1":
                        MessageBox.Show(Properties.Resources.CreateReservationFailCaseNotExistChartNO, Properties.Resources.EMRAgentInterfaceModuleErrorCaption,MessageBoxButtons.OK,MessageBoxIcon.Error);
                        break;
                    case "2":
                        MessageBox.Show(Properties.Resources.CreateReservationFailCaseMemoLengthOverflow, Properties.Resources.EMRAgentInterfaceModuleErrorCaption, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        break;
                    case "3":
                        MessageBox.Show(Properties.Resources.CreateReservationFailCaseAlreadyBooked, Properties.Resources.EMRAgentInterfaceModuleErrorCaption, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        break;
                    default:
                        break;
                } 
                
                String Result = "{\"result\":\"false\",\"code\":\"" + ex.Message + "\",\"data\":" + strReservation + "}";
                return Result;
            }
        }

        public bool updateReservation(String strReservation)
        {
            Console.ForegroundColor = proxyConsoleColor;
            Console.WriteLine("[PROXY] updateReservation 함수처리 실행, strReservation : " + strReservation);
            Console.ResetColor();
            try
            {
                if (String.IsNullOrEmpty(strReservation))
                {
                    return false;
                }

                Reservation reservation = JsonConvert.DeserializeObject<Reservation>(strReservation);
                
                return proxy.UpdateReservation(reservation);
            }
            catch (Exception ex)
            {
                MessageBox.Show(Properties.Resources.UpdateReservationFailMessage, Properties.Resources.EMRAgentInterfaceModuleErrorCaption,MessageBoxButtons.OK,MessageBoxIcon.Error);
                Environment.Exit(0);
                return false;
            }
        }

        public String deleteReservation(String strReservation)
        {
            Console.ForegroundColor = proxyConsoleColor;
            Console.WriteLine("[PROXY] deleteReservation 함수처리 실행, strReservation : " + strReservation);
            Console.ResetColor();
            try
            {
                if (String.IsNullOrEmpty(strReservation))
                {
                    MessageBox.Show(Properties.Resources.DeleteReservationFailCaseNoData, Properties.Resources.EMRAgentInterfaceModuleErrorCaption, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    String Result = "{\"result\":\"false\",\"code\":\"" + 3 + "\",\"data\":" + strReservation + "}"; // DeleteErrorCode 3 : 삭제할려고 하는 예약에 아무런 데이터가 넘어오지 않은 경우
                    return "";
                }

                String emrReservId = null;
                Reservation reservation = null;

                try
                {
                    reservation = JsonConvert.DeserializeObject<Reservation>(strReservation);
                    emrReservId = reservation.emrReservId;
                }
                catch
                {
                    Newtonsoft.Json.Linq.JObject jObject = Newtonsoft.Json.Linq.JObject.Parse(strReservation);
                    emrReservId = (string)jObject["emrReservId"];
                }

                reservation = proxy.GetReservation(emrReservId);

                if (reservation == null)
                {
                    MessageBox.Show(Properties.Resources.DeleteReservationFailCaseNotExitReservation, Properties.Resources.EMRAgentInterfaceModuleErrorCaption, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    String Result = "{\"result\":\"false\",\"code\":\"" + 2 + "\",\"data\":" + strReservation + "}";  //DeleteErrorCode 2 : 이미 삭제된 예약 현황을 삭제 할려고 하는 경우
                    return Result;
                }

                bool isDeleted = proxy.DeleteReservation(reservation);

                if (isDeleted)
                {
                    String Result = "{\"result\":\"true\",\"code\":\"" + null + "\",\"data\":\"" + reservation.reservationId + "\"}";
                    return Result;
                }

                return "";
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                switch (ex.Message)
                {
                    case "1":  // DeleteErrorCode 1 : 이미 내원 하였거나 수납을 진행한 사항에 대해서는 예약 삭제를 할수 없음
                        MessageBox.Show(Properties.Resources.DeleteReservationFailCaseAlreadyReceipt, Properties.Resources.EMRAgentInterfaceModuleErrorCaption, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        break;
                    default:
                        break;
                }

                String Result = "{\"result\":\"false\",\"code\":\"" + ex.Message + "\",\"data\":" + strReservation + "}";
                return Result;
            }
        }

        public String getReceive(String receiveId)
        {
            Console.ForegroundColor = proxyConsoleColor;
            Console.WriteLine("[PROXY] getReceive 함수처리 실행, receiveId : " + receiveId);
            Console.ResetColor();
            try 
            {
                if (String.IsNullOrEmpty(receiveId))
                {
                    return "";
                }

                Receive receive = proxy.GetReceive(receiveId);

                String jsonString = receive == null? "": JsonConvert.SerializeObject(receive);

                return jsonString;

            }
            catch (Exception ex) 
            {
                MessageBox.Show(Properties.Resources.GetReceiveFailMessage, Properties.Resources.EMRAgentInterfaceModuleErrorCaption,MessageBoxButtons.OK,MessageBoxIcon.Error);
                Environment.Exit(0);
                return "";
            }
        }

        public String getReceives(String startDate = "", String endDate = "", String strStaffIds = "")
        {
            Console.ForegroundColor = proxyConsoleColor;
            Console.WriteLine("[PROXY] getReceives 함수처리 실행, Date : " + startDate + "~" + endDate);
            Console.ResetColor();
            try
            {
                
                startDate = String.IsNullOrEmpty(startDate) ? DateTime.Now.ToString(this.dateFormat) : startDate;
                endDate = String.IsNullOrEmpty(endDate) ? DateTime.Now.AddDays(1).ToString(this.dateFormat) : endDate;

                String[] staffIds = null;

                if (!String.IsNullOrEmpty(strStaffIds)) {
                    char[] separatingChars = {','};
                    staffIds = strStaffIds.Split(separatingChars);
                }

                List<Receive> list = proxy.GetReceives(startDate, endDate, staffIds);

                ///TODO: 상태: EnumReceiveState.RECEIVE(접수) / EnumReceiveState.CHECK_UP(진찰) / EnumReceiveState.RECEIPT_WAIT(수납대기) / EnumReceiveState.RECEIPT_DONE(수납완료)
                
                String jsonString = JsonConvert.SerializeObject(list);

                return jsonString;
            }
            catch (Exception ex)
            {
                MessageBox.Show(Properties.Resources.GetReceiveFailMessage, Properties.Resources.EMRAgentInterfaceModuleErrorCaption,MessageBoxButtons.OK,MessageBoxIcon.Error);
                Environment.Exit(0);
                return "";
            }
        }

        public String getReceivesFromReservation (String startDate = "", String endDate = "", String strStaffIds="")
        {
            Console.ForegroundColor = proxyConsoleColor;
            Console.WriteLine("[PROXY] getReceivesFromReservation 함수처리 실행, Date : " + startDate + "~" + endDate);
            Console.ResetColor();
            try
            {
                startDate = String.IsNullOrEmpty(startDate) ? DateTime.Now.ToString(this.dateFormat) : startDate;
                endDate = String.IsNullOrEmpty(endDate) ? DateTime.Now.AddDays(1).ToString(this.dateFormat) : endDate;

                String[] staffIds = null;
                if (!String.IsNullOrEmpty(strStaffIds))
                {
                    char[] separatingChars = { ',' };
                    staffIds = strStaffIds.Split(separatingChars);
                }

                List<Receive> list = proxy.GetReceivesFromReservation(startDate, endDate, staffIds);

                String jsonString = JsonConvert.SerializeObject(list);

                return jsonString;
            }
            catch (Exception ex)
            {
                MessageBox.Show(Properties.Resources.GetReceivesFromReservationFailMessage, Properties.Resources.EMRAgentInterfaceModuleErrorCaption,MessageBoxButtons.OK,MessageBoxIcon.Error);
                Environment.Exit(0);
                return "";
            }

        }

        public String getReceiptsFromReservation(String startDate = "", String endDate = "", String strStaffIds = "")
        {
            Console.ForegroundColor = proxyConsoleColor;
            Console.WriteLine("[PROXY] getReceiptsFromReservation 함수처리 실행, Date : " + startDate + "~" + endDate);
            Console.ResetColor();
            try
            {
                startDate = String.IsNullOrEmpty(startDate) ? DateTime.Now.ToString(this.dateFormat) : startDate;
                endDate = String.IsNullOrEmpty(endDate) ? DateTime.Now.AddDays(1).ToString(this.dateFormat) : endDate;

                String[] staffIds = null;
                if (!String.IsNullOrEmpty(strStaffIds))
                {
                    char[] separatingChars = { ',' };
                    staffIds = strStaffIds.Split(separatingChars);
                }

                List<Receive> list = proxy.GetReceiptsFromReservation(startDate, endDate, staffIds);

                String jsonString = JsonConvert.SerializeObject(list);

                return jsonString;


            }
            catch (Exception ex)
            {
                MessageBox.Show(Properties.Resources.GetReceiptsFromReservationMessage, Properties.Resources.EMRAgentInterfaceModuleErrorCaption,MessageBoxButtons.OK,MessageBoxIcon.Error);
                Environment.Exit(0);
                return "";
            }
        }
        #endregion

        #region 4 예약서버
        // 진료과목
        public String getCategories()
        {
            Console.ForegroundColor = proxyConsoleColor;
            Console.WriteLine("[PROXY] getCategories 함수처리 실행");
            Console.ResetColor();
            try
            {
                List<String> list = proxy.GetCategories();

                String jsonString = JsonConvert.SerializeObject(list);

                return jsonString;
            }
            catch (Exception ex)
            {
                MessageBox.Show(Properties.Resources.GetCategoriesFailMessage, Properties.Resources.EMRAgentInterfaceModuleErrorCaption,MessageBoxButtons.OK,MessageBoxIcon.Error);
                Environment.Exit(0);
                return "";
            }
        }

        // 실시간 대기자 현황
        public String getRealTimeAwaiterCount()
        {
            Console.ForegroundColor = proxyConsoleColor;
            Console.WriteLine("[PROXY] getRealTimeAwaiterCount 함수처리 실행");
            Console.ResetColor();
            try
            {
                Object list = proxy.GetRealTimeAwaiterCount();

                String jsonString = JsonConvert.SerializeObject(list);

                return jsonString;
            }
            catch (Exception ex)
            {
                MessageBox.Show(Properties.Resources.GetRealTimeAwaiterCountFailMessage, Properties.Resources.EMRAgentInterfaceModuleErrorCaption,MessageBoxButtons.OK,MessageBoxIcon.Error);
                Environment.Exit(0);
                return "";
            }
        }

        // 병원 예약 현황
        public String getReservationAwaiterCount(String changedOnly)
        {
            Console.ForegroundColor = proxyConsoleColor;
            Console.WriteLine("[PROXY] getReservationAwaiterCount 함수처리 실행");
            Console.ResetColor();
            try
            {
                changedOnly = String.IsNullOrEmpty(changedOnly) ? "false" : changedOnly;

                List<OfficeReservationState> list = proxy.GetReservationAwaiterCount(bool.Parse(changedOnly));

                //list.ForEach(x => x.reservationStateList.ForEach(r => Console.WriteLine(r.datetime + " / " + Util.ConvertToUnixTimestamp(DateTime.ParseExact(r.datetime, "yyyy-MM-dd HH:mm", CultureInfo.InvariantCulture, DateTimeStyles.None)).ToString())));

                //list.ForEach(x => x.reservationStateList.ForEach(r => r.datetime = Util.ConvertToUnixTimestamp(DateTime.ParseExact(r.datetime, "yyyy-MM-dd HH:mm", CultureInfo.InvariantCulture, DateTimeStyles.None)).ToString()));

                String jsonString = JsonConvert.SerializeObject(list);

                return jsonString;
            }
            catch (Exception ex)
            {
                MessageBox.Show(Properties.Resources.GetReservationAwatierCountFailMessage, Properties.Resources.EMRAgentInterfaceModuleErrorCaption,MessageBoxButtons.OK,MessageBoxIcon.Error);
                Environment.Exit(0);
                return "";
            }            
        }
        #endregion

        #region Windows Notice
        public event DelegateShowWindowNotice EventHandlerShowWindowNotice;

        public void showWindowNotice(int type, String strReservation)
        {
            if (EventHandlerShowWindowNotice != null)
            {
                EventHandlerShowWindowNotice(type, strReservation);
            }
        }

        public void sendClickedToolTipReservation(int type, String strReservation)
        {
            var script = "eventListenerFromAgentProxy('ClickWindowNotice', '" + strReservation.Insert(1, "\"alarmContentsType\":" + type + ",") + "')";

            this.webView.ExecuteScript(script);
        }
        #endregion
    }

    #region 이벤트 리스너
    /// Balloon 에 띄울 메시지 수신 이벤트 리스너
    public delegate void DelegateShowWindowNotice(int type, String strReservation);
    #endregion
}

