﻿//ToolTipFrm.Designer.cs
namespace PCReservationClient
{
    partial class ToolTipFrm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tooltipTitleFirstLabel = new System.Windows.Forms.Label();
            this.tooltipContentFirstLabel = new System.Windows.Forms.Label();
            this.tooltipLogo = new System.Windows.Forms.PictureBox();
            this.tooltipXbtn = new System.Windows.Forms.PictureBox();
            this.tooltipContentSecondLabel = new System.Windows.Forms.Label();
            this.tooltipTitleSecondLabel = new System.Windows.Forms.Label();
            this.tooltipTitleThridLabel = new System.Windows.Forms.Label();
            this.tooltipContentThirdLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.tooltipLogo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tooltipXbtn)).BeginInit();
            this.SuspendLayout();
            // 
            // tooltipTitleFirstLabel
            // 
            this.tooltipTitleFirstLabel.AutoSize = true;
            this.tooltipTitleFirstLabel.Font = new System.Drawing.Font("Noto Sans CJK KR Bold", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.tooltipTitleFirstLabel.Location = new System.Drawing.Point(75, 10);
            this.tooltipTitleFirstLabel.Name = "tooltipTitleFirstLabel";
            this.tooltipTitleFirstLabel.Size = new System.Drawing.Size(63, 28);
            this.tooltipTitleFirstLabel.TabIndex = 0;
            this.tooltipTitleFirstLabel.Text = "새로운";
            // 
            // tooltipContentFirstLabel
            // 
            this.tooltipContentFirstLabel.AutoSize = true;
            this.tooltipContentFirstLabel.BackColor = System.Drawing.Color.Transparent;
            this.tooltipContentFirstLabel.Font = new System.Drawing.Font("Noto Sans CJK KR Regular", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.tooltipContentFirstLabel.Location = new System.Drawing.Point(76, 33);
            this.tooltipContentFirstLabel.Name = "tooltipContentFirstLabel";
            this.tooltipContentFirstLabel.Size = new System.Drawing.Size(76, 25);
            this.tooltipContentFirstLabel.TabIndex = 1;
            this.tooltipContentFirstLabel.Text = "윤현철님";
            // 
            // tooltipLogo
            // 
            this.tooltipLogo.Image = global::PCReservationClient.Properties.Resources.ddocdoc_32;
            this.tooltipLogo.Location = new System.Drawing.Point(21, 18);
            this.tooltipLogo.Name = "tooltipLogo";
            this.tooltipLogo.Size = new System.Drawing.Size(32, 32);
            this.tooltipLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.tooltipLogo.TabIndex = 2;
            this.tooltipLogo.TabStop = false;
            // 
            // tooltipXbtn
            // 
            this.tooltipXbtn.Image = global::PCReservationClient.Properties.Resources.ic_close_nor;
            this.tooltipXbtn.Location = new System.Drawing.Point(368, 10);
            this.tooltipXbtn.Name = "tooltipXbtn";
            this.tooltipXbtn.Size = new System.Drawing.Size(24, 24);
            this.tooltipXbtn.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.tooltipXbtn.TabIndex = 3;
            this.tooltipXbtn.TabStop = false;
            // 
            // tooltipContentSecondLabel
            // 
            this.tooltipContentSecondLabel.AutoSize = true;
            this.tooltipContentSecondLabel.BackColor = System.Drawing.Color.Transparent;
            this.tooltipContentSecondLabel.Font = new System.Drawing.Font("Noto Sans CJK KR Regular", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.tooltipContentSecondLabel.Location = new System.Drawing.Point(166, 36);
            this.tooltipContentSecondLabel.Name = "tooltipContentSecondLabel";
            this.tooltipContentSecondLabel.Size = new System.Drawing.Size(114, 22);
            this.tooltipContentSecondLabel.TabIndex = 4;
            this.tooltipContentSecondLabel.Text = "10월 16일 13:30";
            // 
            // tooltipTitleSecondLabel
            // 
            this.tooltipTitleSecondLabel.AutoSize = true;
            this.tooltipTitleSecondLabel.Font = new System.Drawing.Font("Noto Sans CJK KR Bold", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.tooltipTitleSecondLabel.ForeColor = System.Drawing.Color.DarkRed;
            this.tooltipTitleSecondLabel.Location = new System.Drawing.Point(141, 10);
            this.tooltipTitleSecondLabel.Name = "tooltipTitleSecondLabel";
            this.tooltipTitleSecondLabel.Size = new System.Drawing.Size(80, 28);
            this.tooltipTitleSecondLabel.TabIndex = 5;
            this.tooltipTitleSecondLabel.Text = "예약신청";
            // 
            // tooltipTitleThridLabel
            // 
            this.tooltipTitleThridLabel.AutoSize = true;
            this.tooltipTitleThridLabel.Font = new System.Drawing.Font("Noto Sans CJK KR Bold", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.tooltipTitleThridLabel.Location = new System.Drawing.Point(212, 10);
            this.tooltipTitleThridLabel.Name = "tooltipTitleThridLabel";
            this.tooltipTitleThridLabel.Size = new System.Drawing.Size(108, 28);
            this.tooltipTitleThridLabel.TabIndex = 6;
            this.tooltipTitleThridLabel.Text = "이 있습니다.";
            // 
            // tooltipContentThirdLabel
            // 
            this.tooltipContentThirdLabel.AutoSize = true;
            this.tooltipContentThirdLabel.BackColor = System.Drawing.Color.Transparent;
            this.tooltipContentThirdLabel.CausesValidation = false;
            this.tooltipContentThirdLabel.Font = new System.Drawing.Font("Noto Sans CJK KR Regular", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.tooltipContentThirdLabel.Location = new System.Drawing.Point(287, 34);
            this.tooltipContentThirdLabel.Name = "tooltipContentThirdLabel";
            this.tooltipContentThirdLabel.Size = new System.Drawing.Size(89, 24);
            this.tooltipContentThirdLabel.TabIndex = 7;
            this.tooltipContentThirdLabel.Text = "윤현철 원장";
            // 
            // ToolTipFrm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ClientSize = new System.Drawing.Size(404, 72);
            this.ControlBox = false;
            this.Controls.Add(this.tooltipContentThirdLabel);
            this.Controls.Add(this.tooltipTitleThridLabel);
            this.Controls.Add(this.tooltipTitleSecondLabel);
            this.Controls.Add(this.tooltipContentSecondLabel);
            this.Controls.Add(this.tooltipXbtn);
            this.Controls.Add(this.tooltipLogo);
            this.Controls.Add(this.tooltipContentFirstLabel);
            this.Controls.Add(this.tooltipTitleFirstLabel);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ToolTipFrm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.TransparencyKey = System.Drawing.Color.FromArgb(((int)(((byte)(123)))), ((int)(((byte)(123)))), ((int)(((byte)(123)))));
            ((System.ComponentModel.ISupportInitialize)(this.tooltipLogo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tooltipXbtn)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label tooltipTitleFirstLabel;
        private System.Windows.Forms.Label tooltipContentFirstLabel;
        private System.Windows.Forms.PictureBox tooltipLogo;
        private System.Windows.Forms.PictureBox tooltipXbtn;
        private System.Windows.Forms.Label tooltipContentSecondLabel;
        private System.Windows.Forms.Label tooltipTitleSecondLabel;
        private System.Windows.Forms.Label tooltipTitleThridLabel;
        private System.Windows.Forms.Label tooltipContentThirdLabel;

    }
}