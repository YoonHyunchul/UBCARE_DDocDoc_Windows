﻿//ToolTipFrm.cs
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Text;
using System.Runtime.InteropServices;

namespace PCReservationClient
{
    public partial class ToolTipFrm : Form
    {
        public int type { get; set; }
        public String strReservation { get; set; }

        private System.Windows.Forms.Timer slideTimer;
        private System.Windows.Forms.Timer closeTimer;

        public bool isShow = false;

        [DllImport("Gdi32.dll", EntryPoint = "CreateRoundRectRgn")]
        private static extern IntPtr CreateRoundRectRgn
        (
            int nLeftRect, // x-coordinate of upper-left corner
            int nTopRect, // y-coordinate of upper-left corner
            int nRightRect, // x-coordinate of lower-right corner
            int nBottomRect, // y-coordinate of lower-right corner
            int nWidthEllipse, // height of ellipse
            int nHeightEllipse // width of ellipse
         );

        public ToolTipFrm()
        {
            InitializeComponent();
        }
        public ToolTipFrm(int visibleTime)
        {
            InitializeComponent();

            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            Region = System.Drawing.Region.FromHrgn(CreateRoundRectRgn(0, 0, this.Width, this.Height, 4, 4));

            slideTimer = new System.Windows.Forms.Timer();
            slideTimer.Interval = 1;
            slideTimer.Tick += slideTimer_Open_Tick;

            closeTimer = new System.Windows.Forms.Timer();
            closeTimer.Tick += closeTimer_Tick;
            closeTimer.Interval = visibleTime;

        }

        void closeTimer_Tick(object sender, EventArgs e)
        {
            if (!this.mouseOverFlag)  // 마우스가 들어와있으면 툴팁을 닫지않고 타이머만 종료한다.
                ToolTipClose();
            closeTimer.Enabled = false;
            
        }

        int top;
        public void ToolTipShow(int top)
        {
            this.top = top;
            Show();
            this.isShow = true;
            slideTimer.Start();
            OnClickable();

            closeTimer.Start();
            closeTimer.Enabled = true;
        }

        public void ToolTipShow()
        {
            closeTimer.Stop();
            closeTimer.Enabled = false;
            closeTimer.Start();
            closeTimer.Enabled = true;
        }

        public void ToolTipClose()
        {
            this.isShow = false;
            slideTimer.Start();
            OffClickable();
            closeTimer.Stop();
            closeTimer.Enabled = false;
        }



        double cosDegreeDelta = 0;
        private void slideTimer_Open_Tick(object sender, EventArgs e)
        {
            if (this.Location.X < Screen.PrimaryScreen.WorkingArea.Width - this.Width)
            {
                this.slideTimer.Stop();
                cosDegreeDelta = 0;

                slideTimer.Tick -= new System.EventHandler(slideTimer_Open_Tick);
                slideTimer.Tick += new System.EventHandler(slideTimer_Close_Tick);

            }
            else
            {
                cosDegreeDelta = cosDegreeDelta + 3.1;
                
                if (this.InvokeRequired)
                {
                    this.Invoke(new Action(() => this.Location = new Point(this.Location.X - animSpeed(degree2radian(cosDegreeDelta), 7.15), this.Location.Y)));
                }
                else
                {
                    this.Location = new Point(this.Location.X - animSpeed(degree2radian(cosDegreeDelta), 7.15), this.Location.Y);

                }
            }
        }

        private void slideTimer_Close_Tick(object sender, EventArgs e)
        {
            if (this.Location.X > Screen.PrimaryScreen.WorkingArea.Width + 10)
            {
                this.slideTimer.Stop();
                this.Location = new Point(TooltipManager.Instance.screenWidth, TooltipManager.Instance.screenHeight - ((top + 1) * (this.Height + 7)));
                slideTimer.Tick -= new System.EventHandler(slideTimer_Close_Tick);
                slideTimer.Tick += new System.EventHandler(slideTimer_Open_Tick);
                cosDegreeDelta = 0;
                this.Hide();
            }
            else
            {
                cosDegreeDelta = cosDegreeDelta + 3.1;

                if (this.InvokeRequired)
                {
                    this.Invoke(new Action(() => this.Location = new Point(this.Location.X + animSpeed(degree2radian(cosDegreeDelta), 15), this.Location.Y)));
                }
                else
                {
                    this.Location = new Point(this.Location.X + animSpeed(degree2radian(cosDegreeDelta), 15), this.Location.Y);
                }
            }
        }



        private int animSpeed(double x, double speed)
        {
            return Convert.ToInt32((Math.Cos(x) + 1) * speed);
        }

        private double degree2radian(double degree)
        {
            return (double)(degree * Math.PI / (double)180d);
        }


        private void OnClickable()
        {
            this.MouseEnter += new System.EventHandler(this.ToolTipFrm_MouseEnter);
            this.MouseLeave += new System.EventHandler(this.ToolTipFrm_MouseLeave);
            this.MouseHover += new System.EventHandler(this.ToolTipFrm_MouseHover);
            this.Click += new System.EventHandler(this.ToolTipFrm_Click);
            this.tooltipLogo.Click += new System.EventHandler(this.tooltipLogo_Click);
            this.tooltipTitleFirstLabel.Click += new System.EventHandler(this.tooltipTitleFirstLabel_Click);
            this.tooltipTitleSecondLabel.Click += new System.EventHandler(this.tooltipTitleSecondLabel_Click);
            this.tooltipTitleThridLabel.Click += new System.EventHandler(this.tooltipTitleThridLabel_Click);
            this.tooltipContentFirstLabel.Click += new System.EventHandler(this.tooltipContentFirstLabel_Click);
            this.tooltipContentSecondLabel.Click += new System.EventHandler(this.tooltipContentSecondLabel_Click);
            this.tooltipContentThirdLabel.Click += new System.EventHandler(this.tooltipContentThirdLabel_Click);
            this.tooltipXbtn.MouseDown += new System.Windows.Forms.MouseEventHandler(this.tooltipXbtn_MouseDown);
            this.tooltipXbtn.MouseEnter += new System.EventHandler(this.tooltipXbtn_MouseEnter);
            this.tooltipXbtn.MouseLeave += new System.EventHandler(this.tooltipXbtn_MouseLeave);
            this.tooltipXbtn.MouseUp += new System.Windows.Forms.MouseEventHandler(this.tooltipXbtn_MouseUp);
        }

        private void OffClickable()
        {
            this.MouseEnter -= new System.EventHandler(this.ToolTipFrm_MouseEnter);
            this.MouseLeave -= new System.EventHandler(this.ToolTipFrm_MouseLeave);
            this.MouseHover -= new System.EventHandler(this.ToolTipFrm_MouseHover);
            this.Click -= new System.EventHandler(this.ToolTipFrm_Click);
            this.tooltipLogo.Click -= new System.EventHandler(this.tooltipLogo_Click);
            this.tooltipTitleFirstLabel.Click -= new System.EventHandler(this.tooltipTitleFirstLabel_Click);
            this.tooltipTitleSecondLabel.Click -= new System.EventHandler(this.tooltipTitleSecondLabel_Click);
            this.tooltipTitleThridLabel.Click -= new System.EventHandler(this.tooltipTitleThridLabel_Click);
            this.tooltipContentFirstLabel.Click -= new System.EventHandler(this.tooltipContentFirstLabel_Click);
            this.tooltipContentSecondLabel.Click -= new System.EventHandler(this.tooltipContentSecondLabel_Click);
            this.tooltipContentThirdLabel.Click -= new System.EventHandler(this.tooltipContentThirdLabel_Click);
            this.tooltipXbtn.MouseDown -= new System.Windows.Forms.MouseEventHandler(this.tooltipXbtn_MouseDown);
            this.tooltipXbtn.MouseEnter -= new System.EventHandler(this.tooltipXbtn_MouseEnter);
            this.tooltipXbtn.MouseLeave -= new System.EventHandler(this.tooltipXbtn_MouseLeave);
            this.tooltipXbtn.MouseUp -= new System.Windows.Forms.MouseEventHandler(this.tooltipXbtn_MouseUp);
        }

        // http://yamecoder.tistory.com/141
        public delegate void onToolTipClickDelegate(int type, String strReservation);
        public onToolTipClickDelegate myCallback;

        public void onToolTipClick(onToolTipClickDelegate callback)
        {
            myCallback = callback;
        }

        private void ToolTipClickEvent()
        {
            Console.WriteLine("클릭했습니다.");
            Console.WriteLine("type: " + this.type);
            Console.WriteLine("strReservation: " + this.strReservation);
            myCallback(this.type, this.strReservation);
        }

        
        private void ToolTipCloseSignal()
        {
            ToolTipClose();
            TooltipManager.Instance.ToolTipPosReset(this.top);
        }


        public void setToolTipText(int type, String reserveName, Nullable<DateTime> datetime, String docName)
        {
            switch(type)
            {
                case 1: // type : 1  예약

                    this.tooltipTitleFirstLabel.Text = "새로운";
                    this.tooltipTitleSecondLabel.Text = "예약신청";
                    this.tooltipTitleSecondLabel.ForeColor = Color.FromArgb(0, 123, 0, 0);
                    this.tooltipTitleThridLabel.Text = "이 있습니다.";
                    this.tooltipTitleThridLabel.Location = new Point(222, 10);

                    this.tooltipContentFirstLabel.Text = reserveName + "님";

                    if (datetime == null)
                    {
                        this.tooltipContentSecondLabel.Text = "";
                    }
                    else
                    {
                        DateTime dt = datetime.Value;
                        this.tooltipContentSecondLabel.Text = dt.ToString("yyyy년 MM월 dd일 HH:mm");
                    }

                    if (String.IsNullOrEmpty(docName))
                    {
                        this.tooltipContentThirdLabel.Text = "";
                    }
                    else
                    {
                        this.tooltipContentThirdLabel.Text = docName + " 선생님";
                    }

                    break;
                case 2: // type : 2  예약취소
                    this.tooltipTitleFirstLabel.Text = "예약이";
                    this.tooltipTitleSecondLabel.Text = "취소";
                    this.tooltipTitleSecondLabel.ForeColor = Color.FromArgb(0, 0, 0, 123);
                    this.tooltipTitleThridLabel.Text = "되었습니다.";
                    this.tooltipTitleThridLabel.Location = new Point(180, 10);

                    this.tooltipContentFirstLabel.Text = reserveName + "님";

                    if (datetime == null)
                    {
                        this.tooltipContentSecondLabel.Text = "";
                    }
                    else
                    {
                        DateTime dt = datetime.Value;
                        this.tooltipContentSecondLabel.Text = dt.ToString("yyyy년 MM월 dd일 HH:mm");
                    }

                    if (String.IsNullOrEmpty(docName))
                    {
                        this.tooltipContentThirdLabel.Text = "";
                    }
                    else
                    {
                        this.tooltipContentThirdLabel.Text = docName + " 선생님";
                    }

                    break;
                    
            }
        }

        public void setToolTipText(int reserveCount, int cancelCount)
        {
            this.tooltipTitleFirstLabel.Text = "";
            this.tooltipTitleSecondLabel.Text = (reserveCount + cancelCount) + "건의 알림";
            this.tooltipTitleSecondLabel.Location = new Point(75, 10);
            this.tooltipTitleSecondLabel.ForeColor = Color.FromArgb(0, 123, 0, 0);
            this.tooltipTitleThridLabel.Text = "이 있습니다.";
            this.tooltipTitleThridLabel.Location = new Point(185, 10);
            

            this.tooltipContentFirstLabel.Text = "예약 : " + reserveCount + "건 / 취소 : " + cancelCount + "건";
            this.tooltipContentSecondLabel.Text ="";
            this.tooltipContentThirdLabel.Text ="";
        }



        #region 툴팁에 포커스는 생기지 않지만 최상위화면 유지


        protected override bool ShowWithoutActivation
        {
            get { return true; }
        }

        private const int WS_EX_TOPMOST = 0x00000008;
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams createParams = base.CreateParams;
                createParams.ExStyle |= WS_EX_TOPMOST;
                return createParams;
            }
        }

        #endregion

        #region 톨팁 마우스 클릭 관련 이벤트

        private void ToolTipFrm_MouseHover(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Hand;
        }
        
        private void tooltipXbtn_MouseEnter(object sender, EventArgs e)
        {
            this.tooltipXbtn.Image = PCReservationClient.Properties.Resources.ic_close_foc;
        }

        private void tooltipXbtn_MouseLeave(object sender, EventArgs e)
        {
            this.tooltipXbtn.Image = PCReservationClient.Properties.Resources.ic_close_nor;
        }

        private void tooltipXbtn_MouseDown(object sender, MouseEventArgs e)
        {
            this.tooltipXbtn.Image = PCReservationClient.Properties.Resources.ic_close_pre;
        }

        private void tooltipXbtn_MouseUp(object sender, MouseEventArgs e)
        {
            this.tooltipXbtn.Image = PCReservationClient.Properties.Resources.ic_close_nor;
            ToolTipCloseSignal();
        }

        private void tooltipXbtn_Click(object sender, EventArgs e)
        {
            ToolTipCloseSignal();
        }

        private void ToolTipFrm_Click(object sender, EventArgs e)
        {
            ToolTipClickEvent();
            ToolTipCloseSignal();
        }

        private void tooltipLogo_Click(object sender, EventArgs e)
        {
            ToolTipClickEvent();
            ToolTipCloseSignal();
        }

        private void tooltipTitleFirstLabel_Click(object sender, EventArgs e)
        {
            ToolTipClickEvent();
            ToolTipCloseSignal();
        }

        private void tooltipTitleSecondLabel_Click(object sender, EventArgs e)
        {
            ToolTipClickEvent();
            ToolTipCloseSignal();
        }

        private void tooltipTitleThridLabel_Click(object sender, EventArgs e)
        {
            ToolTipClickEvent();
            ToolTipCloseSignal();
        }

        private void tooltipContentFirstLabel_Click(object sender, EventArgs e)
        {
            ToolTipClickEvent();
            ToolTipCloseSignal();
        }

        private void tooltipContentSecondLabel_Click(object sender, EventArgs e)
        {
            ToolTipClickEvent();
            ToolTipCloseSignal();
        }

        private void tooltipContentThirdLabel_Click(object sender, EventArgs e)
        {
            ToolTipClickEvent();
            ToolTipCloseSignal();
        }

        #endregion

        bool mouseOverFlag = false;
        private void ToolTipFrm_MouseEnter(object sender, EventArgs e)
        {
            mouseOverFlag = true;
        }

        private void ToolTipFrm_MouseLeave(object sender, EventArgs e)
        {
            mouseOverFlag = false;
            if(!this.closeTimer.Enabled)  // 타이머가 종료되고 마우스가 나가면 닫는다.
            {
                
                ToolTipClose();
            }
        }

        protected override void OnMouseLeave(EventArgs e)
        {
            if (this.ClientRectangle.Contains(this.PointToClient(Control.MousePosition)))
                return;
            else
                base.OnMouseLeave(e);
        }

    }
}
