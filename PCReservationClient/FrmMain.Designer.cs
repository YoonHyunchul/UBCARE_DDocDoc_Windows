﻿namespace PCReservationClient
{
    partial class FrmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMain));
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.LabelForReload = new System.Windows.Forms.ToolStripStatusLabel();
            this.LabelForDebugger = new System.Windows.Forms.ToolStripStatusLabel();
            this.notifyIconForRMS = new System.Windows.Forms.NotifyIcon(this.components);
            this.contextMenuStripForRMS = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.ToolStripMenuItemForClose = new System.Windows.Forms.ToolStripMenuItem();
            this.OptionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.emrWebView = new EMRUserControl.EMRWebView();
            this.statusStrip.SuspendLayout();
            this.contextMenuStripForRMS.SuspendLayout();
            this.SuspendLayout();
            // 
            // statusStrip
            // 
            this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.LabelForReload,
            this.LabelForDebugger});
            this.statusStrip.Location = new System.Drawing.Point(0, 939);
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.Size = new System.Drawing.Size(1584, 22);
            this.statusStrip.TabIndex = 3;
            this.statusStrip.Text = "statusStrip1";
            // 
            // LabelForReload
            // 
            this.LabelForReload.Name = "LabelForReload";
            this.LabelForReload.Size = new System.Drawing.Size(59, 17);
            this.LabelForReload.Text = "예약 관리";
            this.LabelForReload.Click += new System.EventHandler(this.LabelForReload_Click);
            // 
            // LabelForDebugger
            // 
            this.LabelForDebugger.Name = "LabelForDebugger";
            this.LabelForDebugger.Size = new System.Drawing.Size(43, 17);
            this.LabelForDebugger.Text = "시스템";
            this.LabelForDebugger.Click += new System.EventHandler(this.LabelForDebugger_Click);
            // 
            // notifyIconForRMS
            // 
            this.notifyIconForRMS.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIconForRMS.Icon")));
            this.notifyIconForRMS.Text = "예약관리시스템";
            this.notifyIconForRMS.Visible = true;
            this.notifyIconForRMS.BalloonTipClicked += new System.EventHandler(this.notifyIconForRMS_BalloonTipClicked);
            this.notifyIconForRMS.Click += new System.EventHandler(this.notifyIconForRMS_Click);
            // 
            // contextMenuStripForRMS
            // 
            this.contextMenuStripForRMS.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripMenuItemForClose,
            this.OptionToolStripMenuItem});
            this.contextMenuStripForRMS.Name = "contextMenuStripForRMS";
            this.contextMenuStripForRMS.Size = new System.Drawing.Size(99, 48);
            // 
            // ToolStripMenuItemForClose
            // 
            this.ToolStripMenuItemForClose.Name = "ToolStripMenuItemForClose";
            this.ToolStripMenuItemForClose.Size = new System.Drawing.Size(98, 22);
            this.ToolStripMenuItemForClose.Text = "종료";
            this.ToolStripMenuItemForClose.Click += new System.EventHandler(this.ToolStripMenuItemForClose_Click);
            // 
            // OptionToolStripMenuItem
            // 
            this.OptionToolStripMenuItem.Name = "OptionToolStripMenuItem";
            this.OptionToolStripMenuItem.Size = new System.Drawing.Size(98, 22);
            this.OptionToolStripMenuItem.Text = "설정";
            this.OptionToolStripMenuItem.Click += new System.EventHandler(this.OptionToolStripMenuItem_Click);
            // 
            // emrWebView
            // 
            this.emrWebView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.emrWebView.Location = new System.Drawing.Point(0, 0);
            this.emrWebView.Name = "emrWebView";
            this.emrWebView.Size = new System.Drawing.Size(1584, 939);
            this.emrWebView.TabIndex = 4;
            // 
            // FrmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1584, 961);
            this.Controls.Add(this.emrWebView);
            this.Controls.Add(this.statusStrip);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmMain";
            this.Text = "예약 관리 시스템";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmMain_FormClosing);
            this.statusStrip.ResumeLayout(false);
            this.statusStrip.PerformLayout();
            this.contextMenuStripForRMS.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip statusStrip;
        private EMRUserControl.EMRWebView emrWebView;
        private System.Windows.Forms.ToolStripStatusLabel LabelForReload;
        private System.Windows.Forms.ToolStripStatusLabel LabelForDebugger;
        private System.Windows.Forms.NotifyIcon notifyIconForRMS;
        private System.Windows.Forms.ContextMenuStrip contextMenuStripForRMS;
        private System.Windows.Forms.ToolStripMenuItem ToolStripMenuItemForClose;
        private System.Windows.Forms.ToolStripMenuItem OptionToolStripMenuItem;
    }
}

