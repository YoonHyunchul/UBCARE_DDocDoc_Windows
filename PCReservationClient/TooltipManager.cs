﻿//ToolTipManager.cs
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;
using Newtonsoft.Json;

namespace PCReservationClient
{
    class TooltipManager
    {
        private static readonly TooltipManager instance;
        private ToolTipFrm[] ttf;
        private int tooltipMaxLength = 3;

        private System.Windows.Forms.Timer resetTimer;
        private int visibleTime = 4 * 1000;

        private int top = -1;

        private int reserveCount = 0;
        private int cancelCount = 0;

        public int screenWidth;
        public int screenHeight;

        static TooltipManager()
        {
            instance = new TooltipManager();
        }

        public static TooltipManager Instance
        {
            get
            {
                return instance;
            }
        }

        private TooltipManager()
        {
            ttf = new ToolTipFrm[tooltipMaxLength + 1];  // 하나더 만드는 이유는 알림 갯수 초과시 초과 알림 표시용 틀팁박스
            for (int i = 0; i <= tooltipMaxLength; i++)
            {
                ttf[i] = new ToolTipFrm(visibleTime);
            }

            this.screenWidth = Screen.PrimaryScreen.WorkingArea.Width;
            this.screenHeight = Screen.PrimaryScreen.WorkingArea.Height;

            resetTimer = new Timer();
            resetTimer.Interval = visibleTime;
            resetTimer.Tick += resetTimer_Tick;
        }

        void resetTimer_Tick(object sender, EventArgs e)
        {
            top = -1;
            reserveCount = 0;
            cancelCount = 0;
            resetTimer.Stop();
        }



        public void ToolTipPosReset(int top)
        {
            if ( this.top == top)
            {
                this.top--;

            }

            //for (int i = 0; i <= tooltipMaxLength; i++)
            //    if (ttf[i].isShow)
            //        ttf[i].ToolTipClose();
            //this.top = -1;
            //this.reserveCount = 0;
            //this.cancelCount = 0;
        }

        public void makeTooltip(int type, String strReservation, ToolTipFrm.onToolTipClickDelegate callback)
        {
            Dictionary<String, String> dicReservation = JsonConvert.DeserializeObject<Dictionary<String, String>>(strReservation);

            //int type = 1;
            String reserveName = dicReservation["userName"];
            Nullable<DateTime> datetime = null;
            String docName = dicReservation["staff"];

            switch(type)
            {
                case 1: // type : 1  예약
                    reserveCount++;

                    // 예약 희망 일시
                    if (String.IsNullOrEmpty(dicReservation["tempReservationDatetime"]))
                    {
                        datetime = null;
                    }
                    else
                    {
                        datetime = Convert.ToDateTime(dicReservation["tempReservationDatetime"]);
                    }

                    break;
                case 2: // type : 2  예약취소
                    cancelCount++;
                    
                    // 예약 일시
                    if (String.IsNullOrEmpty(dicReservation["confirmReservationDatetime"]))
                    {
                        datetime = null;
                    }
                    else
                    {
                        datetime = Convert.ToDateTime(dicReservation["confirmReservationDatetime"]);
                    }

                    break;
            }


            //!/!/!/!/!/! 여기부터 
            if (top == tooltipMaxLength - 1) // 마지막 툴팁이라면
            {
                if (ttf[tooltipMaxLength].isShow)  // 이미 열려있다면 텍스트반 바꾸고 애니메이션 실행하지 않음
                {
                    ttf[tooltipMaxLength].setToolTipText(reserveCount, cancelCount);
                    ttf[tooltipMaxLength].ToolTipShow();
                }
                else
                {
                    ttf[tooltipMaxLength].setToolTipText(reserveCount, cancelCount);
                    ttf[tooltipMaxLength].ToolTipShow(top);
                    ttf[tooltipMaxLength].Location = new Point(screenWidth, screenHeight - ((tooltipMaxLength + 1) * (ttf[tooltipMaxLength].Height + 7)));
                }
            }
            else
            {
                ++top;
                ttf[top].setToolTipText(type, reserveName, datetime, docName);
                if (!ttf[top].isShow)  
                {
                    ttf[top].ToolTipShow(top);
                    ttf[top].Location = new Point(screenWidth, screenHeight - ((top + 1) * (ttf[top].Height + 7)));
                }
                ttf[top].type = type;
                ttf[top].strReservation = strReservation;
                ttf[top].onToolTipClick(callback);
            }
            //!/!/!/!/!/! 여기까지 리펙토링 계획 - yhc
            resetTimer.Stop();
            resetTimer.Start();
        }
    }
}
