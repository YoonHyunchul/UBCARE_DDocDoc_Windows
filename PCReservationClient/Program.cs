﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Windows.Forms;

namespace PCReservationClient
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThreadAttribute]        
        static void Main()
        {
            string mtxName = Properties.Settings.Default.GUID;
            Mutex mtx = new Mutex(true, mtxName);

            TimeSpan tsWait = new TimeSpan(0, 0, 0, 0, 500);
            bool success = mtx.WaitOne(tsWait);

            if (!success)
            {
                MessageBox.Show(Properties.Resources.ProcessAlreadyRunMessage,Properties.Resources.ProcessAlreadyRunMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new FrmMain());
        }
    }
}
