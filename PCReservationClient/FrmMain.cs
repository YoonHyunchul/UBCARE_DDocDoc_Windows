﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.Timers;
using EMRClientLib.Interface;
using System.Runtime.InteropServices;
using System.IO;
using Spring.Context;
using Spring.Context.Support;
using System.Media;
using System.Net;
using YSRAgentMaterial;
using UBCare.YSRAgentMaterial.Core.Util;

namespace PCReservationClient
{
    /**
     * @brief Windows Client for BIT
     */
    public partial class FrmMain : Form
    {
        [DllImport("user32.dll")]
        static extern bool FlashWindow(IntPtr hwnd, bool bInvert);

        [DllImport("kernel32.dll", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool AllocConsole();


        private String startUrl = "";
        private String healthUrl = "";
        private FormWindowState lastFormWindowState = FormWindowState.Normal;
        private Icon ddocdoc_ico = null;
        private Icon ddocdoc_blink_ico = null;
        
        private System.Timers.Timer notifyBlinkTimer = new System.Timers.Timer();

        /**
         * @brief 생성자
         */
        public FrmMain()
        {
            
            InitializeComponent();
            
            if (Properties.Settings.Default.Release)
            {
                Console.WriteLine("[Build Mode] Release");
                this.Text = Properties.Settings.Default.Title;
                startUrl = Properties.Settings.Default.RealUrl;
                healthUrl = Properties.Settings.Default.RealUrlHealth;
            }
            else
            {
                AllocConsole();
                Console.WriteLine("[Build Mode] Test Debugging");
                this.Text = Properties.Settings.Default.TestTitle;
                startUrl = Properties.Settings.Default.TestUrl;
                healthUrl = Properties.Settings.Default.TestUrlHealth;
            }
            checkBasicalConnected();

            // PCReservationClient.exe.config 내의 문자열 수정 필요
            // 참고
            //String startUrl = "http://test.crm.ddocdoc.kr/";
            // 김대리
            //String startUrl = "http://192.168.123.7:9011/";
            // 김이사님
            //String startUrl = "http://192.168.123.5:9011/";
            

            try
            {
                //IApplicationContext ctx = ContextRegistry.GetContext();
                AbstractAgentDerive agent = new AbstractAgentDerive();
                MapUtil.InitializeProfiles();
                agent.agentHospital = new MatAgentHospital();  
                agent.agentReservation = new MatAgentReservation();// (AbstractAgentReservation)ctx.GetObject("EMRAgentReservation");
                agent.agentReceive = new MatAgentReceive(); // (AbstractAgentReceive)ctx.GetObject("EMRAgentReceive");
                
                this.emrWebView.init(agent, startUrl);
            }
            catch(Spring.Objects.Factory.NoSuchObjectDefinitionException e)
            {
                Console.WriteLine("Spring.Objects.Factory.NoSuchObjectDefinitionException");
                Console.WriteLine(e);
                MessageBox.Show(Properties.Resources.NoSuchObjectDefinitionExceptionMessage ,Properties.Resources.NoSuchObjectDefinitionExceptionMessageCaption,MessageBoxButtons.OK,MessageBoxIcon.Error);
                //Environment.Exit(0);
                return;
            }
            catch(System.Configuration.ConfigurationException e)
            {
                Console.WriteLine("System.Configuration.ConfigurationException");
                Console.WriteLine(e);
                MessageBox.Show(Properties.Resources.ConfigurationExceptionMessage, Properties.Resources.ConfigurationExceptionMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Error);
                //Environment.Exit(0);
                return;
            }
            catch(System.Reflection.ReflectionTypeLoadException e)
            {
                Console.WriteLine("망했네");
                Console.WriteLine("System.Reflection.ReflectionTypeLoadException");
                Console.WriteLine(e);
            }


            // Tray Icon
            // http://son10001.blogspot.kr/2015/02/c.html
            this.notifyIconForRMS.Visible = true;
            this.notifyIconForRMS.ContextMenuStrip = this.contextMenuStripForRMS;
            this.notifyBlinkTimer.Interval = 100;
            this.notifyBlinkTimer.Elapsed += new ElapsedEventHandler(NotifyBlinkTimer_elapsed);


            this.ddocdoc_ico = PCReservationClient.Properties.Resources.ddocdoc_16;
            this.ddocdoc_blink_ico = PCReservationClient.Properties.Resources.ddocdoc_16_on;

            this.Activated += FrmMain_Activated;
            this.emrWebView.webViewProxy.EventHandlerShowWindowNotice += new EMRUserControl.DelegateShowWindowNotice(showWindowNotice);

  
        }

        private void checkBasicalConnected()
        {

            // MS에서제공하는 인터넷 연결 확인 여부 사이트
            if (!isHostConnected(url: Properties.Settings.Default.MSNetTest, expect: Properties.Settings.Default.MSNetTestResult))
            {
                MessageBox.Show(Properties.Resources.NetworkConnectionFailMessage, Properties.Resources.NetworkConnectionFailMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Stop);
                Environment.Exit(0);
            }
            
            // 똑닥 서버에 연결되어있는지 확인 여부 사이트
            if (!isHostConnected(url: healthUrl, expect: Properties.Settings.Default.HealthResult))
            {
                MessageBox.Show(Properties.Resources.DDocDocServerNotAliceMessage,Properties.Resources.DDocDocServerNotAliceMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Stop);
                Environment.Exit(0);
            }

            // 프로그램 중간에 인터넷 연결이 끊어지면 발생하는 이벤트
            System.Net.NetworkInformation.NetworkChange.NetworkAvailabilityChanged += (sender, e) =>
            {
                if (!e.IsAvailable)
                {
                    MessageBox.Show(Properties.Resources.NetworkConnectionFailMessage2, Properties.Resources.NetworkConnectionFailMessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            };


        }

        private bool isHostConnected(String url, String expect)
        {
            try
            {
                WebClient wc = new WebClient();
                string real = wc.DownloadString(url);
                if (real != expect)
                    return false;
            }
            catch(Exception e)
            {
                return false;
            }
            return true;
        }
        

        private void FrmMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.lastFormWindowState = this.WindowState;
            
            this.WindowState = FormWindowState.Minimized;
           
            e.Cancel = true;

            this.ShowInTaskbar = false;

            //Application.ExitThread();
            //Environment.Exit(0);
        }

        private void ToolStripMenuItemForClose_Click(object sender, EventArgs e)
        {
            this.notifyIconForRMS.Visible = false;

            Application.ExitThread();
            Environment.Exit(0);
        }

        private void toolStripStatusLabel4_Click(object sender, EventArgs e)
        {
            //this.emrWebView.Load("https://bbros-reservation.herokuapp.com/");

            string curDir = Directory.GetCurrentDirectory();
            this.emrWebView.Load(String.Format("file:///{0}/Resources/UnitTest.html", curDir));
        }

        private void LabelForReload_Click(object sender, EventArgs e)
        {
            this.emrWebView.ReLoad();
        }


        private void notifyIconForRMS_BalloonTipClicked(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Minimized)
                this.WindowState = lastFormWindowState;
            
            this.Activate();
            //this.Focus();
            //최대화
            //this.BringToFront();

            this.notifyBlinkTimer.Stop();
        }

        #region behinded buttons on footer

        
        
        // 웹 디버그창 열기 Easter Eggs
        private int clickCouter = 0;
        private void LabelForDebugger_Click(object sender, EventArgs e)
        {
            if(clickCouter % 3 == 2)
                this.emrWebView.ShowDebbger();

            clickCouter++;
            clickCouter %= 3;
        }

        private void toolStripStatusLabel1_Click(object sender, EventArgs e)
        {
            this.emrWebView.Load(startUrl);
        }

        public void notifyIconForRMS_Click(object sender, EventArgs e)
        {
            MouseEventArgs me = (MouseEventArgs)e;

            if (me.Button == MouseButtons.Left)
            {
                if (this.WindowState == FormWindowState.Minimized)
                    this.WindowState = lastFormWindowState;

                this.Activate();
                this.notifyBlinkTimer.Stop();
                this.ShowInTaskbar = true;
                //this.Focus();
                //최대화
                //this.BringToFront();

            }
        }

        #endregion

        #region Windows Notice

        public void showWindowNotice(int type, String strReservation)
        {
            // 툴팁박스띄우기
            if ( this.InvokeRequired)
            {
                this.BeginInvoke(new Action (()=> TooltipManager.Instance.makeTooltip(type, strReservation, callbackOnToolTipClick)));
            }
            else
            {
                TooltipManager.Instance.makeTooltip(type, strReservation, callbackOnToolTipClick);
            }
            

            SoundPlayer simpleAlert = new SoundPlayer(Properties.Resources.alert3);
            
            simpleAlert.Play();

            // 메인화면 활성화 안되어 있을때 
            if (!this.Focused)  
            {
                if ( this.InvokeRequired)
                {
                    this.BeginInvoke(new Action(() => FlashWindow(this.Handle, true)));
                    this.BeginInvoke(new Action(() => this.notifyBlinkTimer.Start()));
                }
                else
                {
                    // 타스크 아이콘 깜빡깜빡
                    FlashWindow(this.Handle, true);
                    // 트레이아이콘 깜빡깜빡
                    this.notifyBlinkTimer.Start();
                }
            }
        }

        void FrmMain_Activated(object sender, EventArgs e)
        {
            if ( this.InvokeRequired)
            {
                this.BeginInvoke(new Action(() => this.notifyBlinkTimer.Stop()));
                this.BeginInvoke(new Action(() => this.notifyIconForRMS.Icon = this.ddocdoc_ico));
            }
            else
            {
                this.notifyBlinkTimer.Stop();
                this.notifyIconForRMS.Icon = this.ddocdoc_ico;
            }
        }

        private void callbackOnToolTipClick(int type, String strReservation)
        {
            if (this.InvokeRequired)
            {
                this.BeginInvoke(new Action(()=> this.Activate()));
                if (this.WindowState == FormWindowState.Minimized )
                {
                    this.BeginInvoke(new Action(() => this.WindowState = this.lastFormWindowState));
                }

            }
            else
            {
                this.Activate();
                if (this.WindowState == FormWindowState.Minimized)
                    this.WindowState = this.lastFormWindowState;
            }
            this.emrWebView.webViewProxy.sendClickedToolTipReservation(type, strReservation);
            
        
        }

        private void NotifyBlinkTimer_elapsed(Object sender, ElapsedEventArgs e)
        {

            if (this.notifyIconForRMS.Icon == this.ddocdoc_ico)
            {
                if ( this.InvokeRequired)
                    this.BeginInvoke(new Action(() => this.notifyIconForRMS.Icon = this.ddocdoc_blink_ico));
                else
                    this.notifyIconForRMS.Icon = this.ddocdoc_blink_ico;
            }
            else
            {
                if (this.InvokeRequired)
                    this.BeginInvoke(new Action(() => this.notifyIconForRMS.Icon = this.ddocdoc_ico));
                else
                    this.notifyIconForRMS.Icon = this.ddocdoc_ico;
            }
        }

        #endregion


        #region Window Minimize Issue solover
        private const int WM_SYSCOMMAND = 0x0112;
        private const int SC_MINIMIZE = 0xf020;
        protected override void WndProc(ref Message m)
        {
            if (m.Msg == WM_SYSCOMMAND)
            {
                if (m.WParam.ToInt32() == SC_MINIMIZE)
                {
                    this.lastFormWindowState = this.WindowState;
                }
            }
            base.WndProc(ref m);
        }
        #endregion

        private void label1_Click(object sender, EventArgs e)
        {
            int type = 1;
            //showWindowNotice(type, "");
        }

        private void OptionToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }
        
    }
}

