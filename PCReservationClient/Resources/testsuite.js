﻿QUnit.module("String getHospitalInfo()", function () {
    QUnit.module("리턴: 병원 정보", function () {
        QUnit.test("getHospitalInfo()", function (assert) {
            // Given
            var expected = '{"hospitalId":"12345678","hospitalPassword":"12345678","isChartNoAutoGenerate":true,"useComment":true}';

            // Wnen
            var actual = AgentProxy.getHospitalInfo();

            // Then
            assert.equal(actual, expected, "actual: " + actual);
        });
    });
});

QUnit.module("String getStaffs()", function () {
    QUnit.module("리턴: 의료진 정보", function () {
        QUnit.test("getStaffs()", function (assert) {
            // Given
            var expected = '[{"uniqueKey":"11111","staffs":[{"staffId":"MA1","staff":"김보철","category":"치과보철과","officeId":"MA","office":"치과보철과"}]},{"uniqueKey":"22222","staffs":[{"staffId":"MA2","staff":"한보철","category":"치과보철과","officeId":"MA","office":"치과보철과"}]},{"uniqueKey":"12345","staffs":[{"staffId":"OS","staff":"비트","category":"치과교정과","officeId":"OS","office":"치과교정과"}]},{"uniqueKey":"33333","staffs":[{"staffId":"PI1","staff":"김치아","category":"구강내과","officeId":"PI","office":"구강내과"}]},{"uniqueKey":"44444","staffs":[{"staffId":"PI2","staff":"이구강","category":"구강내과","officeId":"PI","office":"구강내과"}]},{"uniqueKey":"55555","staffs":[{"staffId":"PI3","staff":"이치료","category":"구강내과","officeId":"PI","office":"구강내과"}]}]';

            // Wnen
            var actual = AgentProxy.getStaffs();

            // Then
            //console.log(JSON.stringify(JSON.parse(actual), "", 2));
            assert.equal(actual, expected, "actual: " + actual);
        });
    });
});

QUnit.module("String getCharts(String name, String birthdate, String gender)", function () {
    QUnit.module("리턴: 빈 리스트", function () {
        var expetedEmptyList = '[]';

        QUnit.test("getCharts('', '', '')", function (assert) {
            // Given
            // Wnen
            var actual = AgentProxy.getCharts('', '', '');

            // Then
            assert.equal(actual, expetedEmptyList, "actual: " + actual);
        });

        QUnit.test("getCharts('', '', 'M')", function (assert) {
            // Given
            // Wnen
            var actual = AgentProxy.getCharts('', '', 'M');

            // Then
            assert.equal(actual, expetedEmptyList, "actual: " + actual);
        });

        QUnit.test("getCharts('김종민', '1971-02-26', 'F')", function (assert) {
            // Given
            // Wnen
            var actual = AgentProxy.getCharts('김종민', '1971-02-26', 'F');

            // Then
            assert.equal(actual, expetedEmptyList, "actual: " + actual);
        });

        QUnit.test("getCharts('John Doe', '', '')", function (assert) {
            // Given
            // Wnen
            var actual = AgentProxy.getCharts('John Doe', '', '');

            // Then
            assert.equal(actual, expetedEmptyList, "actual: " + actual);
        });
    });

    QUnit.module("리턴: 차트 정보가 하나인 리스트", function () {
        var expectedListContainsOne = '[{"chartNo":"31","emrUserId":"31","name":"김종민","phone":"01066884731","birthDate":"19710226","gender":"M","address":""}]';

        QUnit.test("getCharts('김종민', '', '')", function (assert) {
            // Given
            // Wnen
            var actual = AgentProxy.getCharts('김종민', '', '');

            // Then
            assert.equal(actual, expectedListContainsOne, "actual: " + actual);
        });

        QUnit.test("getCharts('', '1971-02-26', '')", function (assert) {
            // Given
            // Wnen
            var actual = AgentProxy.getCharts('', '1971-02-26', '');

            // Then
            assert.equal(actual, expectedListContainsOne, "actual: " + actual);
        });

        QUnit.test("getCharts('김종민', '1971-02-26', '')", function (assert) {
            // Given
            // Wnen
            var actual = AgentProxy.getCharts('김종민', '1971-02-26', '');

            // Then
            assert.equal(actual, expectedListContainsOne, "actual: " + actual);
        });

        QUnit.test("getCharts('김종민', '1971-02-26', 'M')", function (assert) {
            // Given
            // Wnen
            var actual = AgentProxy.getCharts('김종민', '1971-02-26', 'M');

            // Then
            assert.equal(actual, expectedListContainsOne, "actual: " + actual);
        });
    });

    QUnit.module("리턴: 차트 정보가 두개인 리스트", function () {
        var expectedListContainsTwo = '[{"chartNo":"6","emrUserId":"6","name":"안신환","phone":"010-1234-1111","birthDate":"19890707","gender":"M","address":""},{"chartNo":"8","emrUserId":"8","name":"나재진","phone":"010-1111-1111","birthDate":"19890707","gender":"M","address":""}]';

        QUnit.test("getCharts('', '1989-07-07', '')", function (assert) {
            // Given
            // Wnen
            var actual = AgentProxy.getCharts('', '1989-07-07', '');

            // Then
            assert.equal(actual, expectedListContainsTwo, "actual: " + actual);
        });
    });
});

QUnit.module("String getReservation(String emrReservId)", function () {
    QUnit.module("리턴: 빈 문자열", function () {
        var expetedEmptyString = '';

        QUnit.test("getReservation('')", function (assert) {
            // Given
            // Wnen
            var actual = AgentProxy.getReservation('');

            // Then
            assert.equal(actual, expetedEmptyString, "actual: " + actual);
        });

        QUnit.test("getReservation('00000')", function (assert) {
            // Given
            // Wnen
            var actual = AgentProxy.getReservation('00000');

            // Then
            assert.equal(actual, expetedEmptyString, "actual: " + actual);
        });
    });

    QUnit.module("리턴: 하나의 예약", function () {
        QUnit.test("getReservation('25435')", function (assert) {
            // Given
            var expected = '{"emrReservId":"25435","reservationId":"","userName":"박보검","userPhone":"","birthDate":"2007-09-09","gender":"M","userId":null,"symptomText":null,"confirmReservationDatetime":"2016-09-20 16:00","category":null,"officeId":"MA","office":"치과보철과","staffId":"MA1","staff":"김보철","chartNo":"22","memo":"","comment":"연속메모랑게"}';

            // Wnen
            var actual = AgentProxy.getReservation('25435');

            // Then
            assert.equal(actual, expected, "actual: " + actual);
        });
    });
});

QUnit.module("String getReservations(String startDate, String endDate)", function () {
    QUnit.module("리턴: 빈 리스트", function () {
        var expetedEmptyList = '[]';

        QUnit.test("getReservations('9999-01-01', '9999-12-31')", function (assert) {
            // Given
            // Wnen
            var actual = AgentProxy.getReservations('9999-01-01', '9999-12-31');

            // Then
            assert.equal(actual, expetedEmptyList, "actual: " + actual);
        });
    });

    QUnit.module("리턴: 9월 20일 예약 리스트", function () {
        QUnit.test("getReservations('2016-09-20', '2016-09-21')", function (assert) {
            // Given
            // Wnen
            var actual = AgentProxy.getReservations('2016-09-20', '2016-09-21');
            var json = JSON.parse(actual);

            // Then
            assert.equal(json.length, 5, "actual: " + actual);
        });
    });

    QUnit.module("리턴: 오늘의 예약 리스트", function () {
        var today = new Date().format("yyyy-MM-dd");
        var tommorow = new Date().addDays(1).format("yyyy-MM-dd");

        QUnit.test("getReservations('" + today + "', '" + tommorow + "')", function (assert) {
            // Given
            // Wnen
            var actual = AgentProxy.getReservations(today, tommorow);
            var json = JSON.parse(actual);

            // Then
            assert.ok(json.length > -1, "actual: " + actual);
        });
    });

    QUnit.module("리턴: 오늘을 포함한 이후의 모든 예약 리스트", function () {
        var today = new Date().format("yyyy-MM-dd");
        var tommorow = new Date().addDays(1).format("yyyy-MM-dd");

        QUnit.test("getReservations('', '')", function (assert) {
            // Given
            // Wnen
            var actual = AgentProxy.getReservations('', '');
            var json = JSON.parse(actual);

            // Then
            assert.ok(json.length > -1, "actual: " + actual);
        });
    });
});

//TODO:
QUnit.module("String createReservation(String strReservation)", function () {

});

//TODO:
QUnit.module("bool updateReservation(String strReservation)", function () {

});

//TODO:
QUnit.module("String deleteReservation(String strReservation)", function () {

});

QUnit.module("String getReceive(String receiveId)", function () {
    QUnit.module("리턴: 빈 문자열", function () {
        var expetedEmptyString = '';

        QUnit.test("getReceive('')", function (assert) {
            // Given
            // Wnen
            var actual = AgentProxy.getReceive('');

            // Then
            assert.equal(actual, expetedEmptyString, "actual: " + actual);
        });

        QUnit.test("getReceive('00000')", function (assert) {
            // Given
            // Wnen
            var actual = AgentProxy.getReceive('00000');

            // Then
            assert.equal(actual, expetedEmptyString, "actual: " + actual);
        });
    });

    QUnit.module("리턴: 하나의 접수", function () {
        QUnit.test("getReceive('25435')", function (assert) {
            // Given
            var expected = '{"receiveId":"25435","chartNo":"22","userName":"박보검","userPhone":"","birthDate":"20070909","gender":"M","category":"치과보철과","officeId":"MA","office":"치과보철과","staffId":"MA1","staff":"김보철","comment":"연속메모랑게","receiveState":0,"receiveDatetime":"201609201600","stateUpdatedDatetime":"201609200959"}';

            // Wnen
            var actual = AgentProxy.getReceive('25435');

            // Then
            assert.equal(actual, expected, "actual: " + actual);
        });
    });
});

QUnit.module("String getReceivesFromReservation(String startDate, String endDate, String strStaffIds)", function () {
    var expectedEmptyList = '[]';
    
    QUnit.test("getReceivesFromReservation('','','')", function (assert) {
        var actual = AgentProxy.getReceivesFromReservation('', '', '');

        assert.equal(actual, expectedEmptyList, "actual: " + actual);
    });

    QUnit.test("getReceivesFromReservation('0000-01-01','9999-12-31','')", function (assert) {
        var actual = AgentProxy.getReceivesFromReservation('0000-01-01', '9999-12-31', '');
        assert.equal(actual, expectedEmptyList, "actual: " + actual);
    });
});

QUnit.module("String getReceiptsFromReservation(String startDate, String endDate, String strDtaffIds)", function () {
    var expectedEmptyList = '[]';

    QUnit.test("getReceiptsFromReservation('','','')", function (assert) {
        var actual = AgentProxy.getReceiptsFromReservation('', '', '');
        assert.equal(actual, expectedEmptyList, "actual: " + actual);
    });

    QUnit.test("getReceiptsFromReservation('0000-01-01','9999-12-31','')", function (assert) {
        var actual = AgentProxy.getReceiptsFromReservation('0000-01-01', '9999-12-31', '');
        assert.equal(actual, expectedEmptyList, "actual: " + actual);
    });
});

QUnit.module("String getReceives(String startDate, String endDate, String strStaffIds)", function () {
    QUnit.module("리턴: 빈 리스트", function () {
        var expetedEmptyList = '[]';

        QUnit.test("getReceives('', '', '')", function (assert) {
            // Given
            // Wnen
            var actual = AgentProxy.getReceives('', '', '');

            // Then
            assert.equal(actual, expetedEmptyList, "actual: " + actual);
        });

        QUnit.test("getReceives('9999-01-01', '9999-12-31', '')", function (assert) {
            // Given
            // Wnen
            var actual = AgentProxy.getReceives('9999-01-01', '9999-12-31', '');

            // Then
            assert.equal(actual, expetedEmptyList, "actual: " + actual);
        });
    });

    QUnit.module("리턴: 접수 리스트", function () {
        QUnit.test("getReceives('2016-10-01', '2016-10-07', '')", function (assert) {
            // Given
            var expected = '[{"receiveId":"25501","chartNo":"6","userName":"안신환","userPhone":"010-1234-1111","birthDate":"19890707","gender":"M","category":"구강내과","officeId":"PI","office":"구강내과","staffId":"PI3","staff":"이치료","comment":"","receiveState":0,"receiveDatetime":"201609121130","stateUpdatedDatetime":"201610051136"},{"receiveId":"25502","chartNo":"6","userName":"안신환","userPhone":"010-1234-1111","birthDate":"19890707","gender":"M","category":"구강내과","officeId":"PI","office":"구강내과","staffId":"PI3","staff":"이치료","comment":"","receiveState":0,"receiveDatetime":"201609121130","stateUpdatedDatetime":"201610051418"}]';

            // Wnen
            var actual = AgentProxy.getReceives('2016-10-01', '2016-10-07', '');

            // Then
            assert.equal(actual, expected, "actual: " + actual);
        });

        QUnit.test("getReceives('2016-10-01', '2016-10-07', 'PI1,MI2')", function (assert) {
            // Given
            var expected = '[{"receiveId":"25501","chartNo":"6","userName":"안신환","userPhone":"010-1234-1111","birthDate":"19890707","gender":"M","category":"구강내과","officeId":"PI","office":"구강내과","staffId":"PI3","staff":"이치료","comment":"","receiveState":0,"receiveDatetime":"201609121130","stateUpdatedDatetime":"201610051136"},{"receiveId":"25502","chartNo":"6","userName":"안신환","userPhone":"010-1234-1111","birthDate":"19890707","gender":"M","category":"구강내과","officeId":"PI","office":"구강내과","staffId":"PI3","staff":"이치료","comment":"","receiveState":0,"receiveDatetime":"201609121130","stateUpdatedDatetime":"201610051418"}]';

            // Wnen
            var actual = AgentProxy.getReceives('2016-10-01', '2016-10-07', 'PI3,MI2');

            // Then
            assert.equal(actual, expected, "actual: " + actual);
        });
    });
});

QUnit.module("String getCategories()", function () {
    QUnit.module("리턴: 진료 과목 리스트", function () {
        QUnit.test("getCategories()", function (assert) {
            // Given
            var expected = '["치과보철과","치과교정과","구강내과"]';

            // Wnen
            var actual = AgentProxy.getCategories();

            // Then
            //console.log(JSON.stringify(JSON.parse(actual), "", 2));
            assert.equal(actual, expected, "actual: " + actual);
        });
    });
});

QUnit.module("String getRealTimeAwaiterCount()", function () {
    QUnit.module("리턴: 직원별 실시간 대기자 수 리스트", function () {
        QUnit.test("getRealTimeAwaiterCount()", function (assert) {
            // Given
            // Wnen
            var actual = AgentProxy.getRealTimeAwaiterCount();
            var json = JSON.parse(actual);

            // Then
            assert.ok(json.officeDetail.length > 0, "actual: " + actual);
        });
    });
});

QUnit.module("String getReservationAwaiterCount(String changedOnly)", function () {
    QUnit.module("리턴: 직원별 예약 대기자 리스트", function () {
        QUnit.test("getReservationAwaiterCount('')", function (assert) {
            // Given
            // Wnen
            var actual = AgentProxy.getReservationAwaiterCount('');
            var json = JSON.parse(actual);


            // Then
            assert.ok(Array.isArray(json), "actual: " + actual);
        });
    });

    QUnit.module("리턴: 직원별 예약 대기자 리스트", function () {
        QUnit.test("getReservationAwaiterCount('true')", function (assert) {
            // Given
            // Wnen
            var actual = AgentProxy.getReservationAwaiterCount('true');
            var json = JSON.parse(actual);

            // Then
            assert.ok(Array.isArray(json), "actual: " + actual);
        });
    });

    QUnit.module("리턴: 직원별 예약 대기자 리스트", function () {
        QUnit.test("getReservationAwaiterCount('false')", function (assert) {
            // Given
            // Wnen
            var actual = AgentProxy.getReservationAwaiterCount('false');
            var json = JSON.parse(actual);

            // Then
            assert.ok(Array.isArray(json), "actual: " + actual);
        });
    });
});

/* 
void showWindowNotice(int type, String strReservation)
 
void sendClickedToolTipReservation(int type, String strReservation)
*/