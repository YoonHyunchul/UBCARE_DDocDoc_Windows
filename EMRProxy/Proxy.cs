﻿using EMRClientLib.Domain;
using EMRClientLib.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Timers;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Windows.Forms;
using System.Threading;

namespace EMRProxy
{
    /**
     * @brief ERM 과 PC클라이언트를 중재하는 Proxy
     */
    public class Proxy : IProxy
    {
        /// IDdocdocAgent 구현체(EMR 업체별 구현 필요)
        public AbstractAgentDerive agent { get; private set; }

        /// 마지막으로 전송한 예약 현황
        public List<Reservation> lastOfficeReservationStateItemList { get; set; }
        
        /// Ddocdoc API Server 로그인을 위한 병원 ID
        public String HospitalId
        {
            get;
            private set;
        }

        /// Ddocdoc API Server 로그인을 위한 병원 비밀번호
        public String HospitalPassword
        {
            get;
            private set;
        }

        /**
         * @brief 생성자
         */
        public Proxy(AbstractAgentDerive agent)
        {
            this.agent = agent;
            this.HospitalId = this.agent.agentHospital.GetHospitalId().Trim();
            this.HospitalPassword = this.agent.agentHospital.GetHospitalPassword().Trim();
        }

        #region EventHandler
        /// EMR 에서 진료 대기자 수 변동 시
        public void AddListenerOnChangeRealtimeAwaiterCounterUsingEMR(DelegateChangeRealtimeAwaiterCounterFromEMR listener)
        {
            this.agent.agentHospital.EventHandlerForChangeRealtimeAwaiterCounterFromEMR += listener; 
        }

        /// EMR 에서 예약을 생성한 경우
        public void AddListenerOnCreateReservationUsingEMR(DelegateCreateReservationFromEMR listener)
        {
            this.agent.agentReservation.EventHandlerForCreateReservationFromEMR += listener;
        }
        
        /// EMR 에서 예약을 변경한 경우
        public void AddListenerOnUpdateReservationUsingEMR(DelegateUpdateReservationFromEMR listener)
        {
            this.agent.agentReservation.EventHandlerForUpdateReservationFromEMR += listener;
        }

        /// EMR 에서 예약을 삭제한 경우
        public void AddListenerOnDeleteReservationUsingEMRR(DelegateDeleteReservationFromEMR listener)
        {
            this.agent.agentReservation.EventHandlerForDeleteReservationFromEMR += listener;
        }

        /// EMR 에서 예약을 접수한 경우
        public void AddListenerOnReceiveReservationUsingEMR(DelegateReceiveReservationFromEMR listener)
        {
            this.agent.agentReservation.EventHandlerForReceiveReservationFromEMR += listener;
        }

        /// EMR 에서 예약을 수납한 경우
        public void AddListenerOnReceiptReservationUsingEMR(DelegateReceiptReservationFromEMR listener)
        {
            this.agent.agentReservation.EventHandlerForReceiptReservationFromEMR += listener;
        }
        #endregion

        #region WebKit 요청 처리
        #region 4 PC클라이언트
        /**
         * @brief 차트 번호를 자동 생성 여부
         * @return bool (자동 생성: true / 수동 생성: false)
         */
        public bool IsChartNoAutoGenerate()
        {
            return agent.agentHospital.IsChartNoAutoGenerate();
        }

        /**
         * @brief 연속 메모(환자 이력) 사용 여부
         * @return bool (사용: true / 미사용: false)
         */
        public bool UseComment()
        {
            return agent.agentHospital.UseComment();
        }

        /**
         * @brief 진료시간 요청
         * @return 진료시간 리스트
         * @details 휴무일은 전송하지 않는다.
         */
        public List<Timetable> GetTimetable()
        {
            List<Timetable> list = agent.agentHospital.GetTimetable();

            return list;
        }

        /**
         * @brief 휴무 정보 조회
         * @return 휴무 정보 리스트
         * @details 오늘 이후의 휴무 일정 전체를 반환
         */
        public Dictionary<String, List<DayOffInfo>> GetDayOffSchedule()
        {
            Dictionary<String, List<DayOffInfo>> list = agent.agentHospital.GetDayOffSchedule();

            return list;
        }

        /**
         * @brief 진료실별 의사 목록 조회
         * @return 진료실별 의사 목록 목록
         */
        public Object GetOfficeStaffs()
        {
            List<CategoryOfficeStaff> list = agent.agentHospital.GetCategoryOfficeStaffs();

            // [{"office":"MA","staffs":["김보철","한보철"]},{"office":"OS","staffs":["비트"]},{"office":"PI","staffs":["김치아","이구강","이치료"]}]
            /*
            object officeStaffList = list
                .GroupBy(x => x.officeId)
                .Select(group => new
                {
                    office = group.Key,
                    staffs = group.Select(y => y.staff)
                });
            */

            // [{"officeId":"MA","office":"치과보철과","staffs":[{"staffId":"MA1","staff":"김보철"},{"staffId":"MA2","staff":"한보철"}]},{"officeId":"OS","office":"치과교정과","staffs":[{"staffId":"OS","staff":"비트"}]},{"officeId":"PI","office":"구강내과","staffs":[{"staffId":"PI1","staff":"김치아"},{"staffId":"PI2","staff":"이구강"},{"staffId":"PI3","staff":"이치료"}]}]
            object officeStaffList = list
                .GroupBy(x => new { x.officeId, x.office })
                .Select(group => new
                {
                    officeId = group.Key.officeId,
                    office = group.Key.office,
                    staffs = group.Select(y => new { staffId = y.staffId, staff = y.staff })
                });

            return officeStaffList;
        }

        /**
         * @brief 청구진료과별 의사 목록 조회
         * @return 청구진료과별 의사 목록 목록
         */
        public Object GetStaffs()
        {
            List<CategoryOfficeStaff> list = agent.agentHospital.GetCategoryOfficeStaffs();

            #region Example
            /*
                [
                  {
                    "uniqueKey": "11111",
                    "staffs": [
                      {
                        "staffId": "MA1",
                        "staff": "김보철",
                        "category": "치과보철과",
                        "officeId": "MA",
                        "office": "치과보철과"
                      },
                      {
                        "staffId": "PI3",
                        "staff": "이치료",
                        "category": "구강내과",
                        "officeId": "PI",
                        "office": "구강내과"
                      }
                    ]
                  },
                  {
                    "uniqueKey": "22222",
                    "staffs": [
                      {
                        "staffId": "MA2",
                        "staff": "한보철",
                        "category": "치과보철과",
                        "officeId": "MA",
                        "office": "치과보철과"
                      }
                    ]
                  },
                  {
                    "uniqueKey": "12345",
                    "staffs": [
                      {
                        "staffId": "OS",
                        "staff": "비트",
                        "category": "치과교정과",
                        "officeId": "OS",
                        "office": "치과교정과"
                      }
                    ]
                  },
                  {
                    "uniqueKey": "33333",
                    "staffs": [
                      {
                        "staffId": "PI1",
                        "staff": "김치아",
                        "category": "구강내과",
                        "officeId": "PI",
                        "office": "구강내과"
                      }
                    ]
                  },
                  {
                    "uniqueKey": "44444",
                    "staffs": [
                      {
                        "staffId": "PI2",
                        "staff": "이구강",
                        "category": "구강내과",
                        "officeId": "PI",
                        "office": "구강내과"
                      }
                    ]
                  }
                ]
             */
            #endregion
            object officeStaffList = list
                .GroupBy(x => x.uniqueKey)
                .Select(group => new
                {
                    uniqueKey = group.Key,
                    staffs = group.Select(x => new { x.staffId, x.staff, x.category, x.officeId, x.office })
                });

            return officeStaffList;
        }

        /**
        * @brief 차트 정보 조회
        * @param name: 환자 성명
        * @param birthDate: 생년월일(yyyy-MM-dd)
        * @param gender: 성별(M/F)
        * @return 차트 정보 리스트
        * @details 성별 코드 참조 - http://marga.tistory.com/488
        */
        public List<Chart> GetCharts(String name, String birthDate, String gender)
        {
            List<Chart> list = agent.agentHospital.GetCharts(name, birthDate, gender);

            return list;
        }


        /**
         * @brief 마지막 생성된 차트번호 조회
         * @return 마지막 차트번호
         */
        public String GetLastestChartNO()
        {
            String lastChartNO = agent.agentHospital.GetLastestChartNO();

            return lastChartNO;
        }


        /**
         * @brief 일자와 진료실&직원(주치의) 정보로 예약 현황 검색
         * @param date: 검색 일자(yyyy-MM-dd)
         * @param officeArray: 진료실&직원(주치의) String 배열
         * @return 일시별 진료실&직원(주치의)별 예약 현황
         */
        public List<Reservation> GetReservations(String date, String[] officeArray)
        {
            List<CategoryOfficeStaff> offices = new List<CategoryOfficeStaff>();

            List<Reservation> list = agent.agentReservation.GetReservations(date, offices);

            if (officeArray == null)
            {
                return list;
            }
            else
            {
                return list.Where(item => officeArray.Contains<String>(item.office)).ToList();
            }
        }

        public List<Reservation> GetReservations(String startDate, String endDate)
        {
            return this.GetReservations(startDate, endDate, new String[] { });
        }

        public List<Reservation> GetReservations(String startDate, String endDate, String[] officeArray)
        {
            List<CategoryOfficeStaff> offices = new List<CategoryOfficeStaff>();

            List<Reservation> list = agent.agentReservation.GetReservations(startDate, endDate, offices);

            if (officeArray == null)
            {
                return list;
            }
            else
            {
                return list.Where(item => officeArray.Contains<String>(item.office)).ToList();
            }
        }


        public Reservation GetReservation(String emrReservId)
        {
            Reservation reservation = agent.agentReservation.GetReservation(emrReservId);

            return reservation;
        }

        /**
         * @brief 예약 생성
         * @param reservation: Reservation
         * @return emrReservId
         */
        public String CreateReservation(Reservation reservation)
        {
            String emrReservId = agent.agentReservation.CreateReservation(reservation);

            if (String.IsNullOrEmpty(emrReservId))
            {
                return null;
            }
            else
            {
                return emrReservId.Trim();
            }
        }

        /**
         * @brief 예약 변경
         * @param reservation: Reservation
         * @return 성공 여부
         */
        public bool UpdateReservation(Reservation reservation)
        {
            return agent.agentReservation.UpdateReservation(reservation);
        }

        /**
         * @brief 예약 삭제
         * @param reservation: Reservation
         * @return 성공 여부
         */
        public bool DeleteReservation(Reservation reservation)
        {            
            return agent.agentReservation.DeleteReservation(reservation);
        }

        /**
         * @brief 접수 상세 정보 조회
         * @param receiveId: 접수 번호
         * @return 접수 상세 정보
         */
        public virtual Receive GetReceive(String receiveId)
        {
            return agent.agentReceive.GetReceive(receiveId);
        }

        /**
         * @brief 검색 시작 일자와 검색 종료 일자, 직원(주치의)들의 정보로 접수 현황 검색
         * @param startDate: 검색 시작 일자(yyyy-MM-dd)
         * @param endDate: 검색 종료 일자(yyyy-MM-dd)
         * @param staffIds: 직원(주치의) 아이디 리스트 List<String>
         * @details: startDate <= OcmUpdDtm < endDate
         *      "2016-09-01 00:00" <= OcmUpdDtm < "2016-10-01 00:00"
         * @return 접수 현황 리스트
         */
        public virtual List<Receive> GetReceives(String startDate, String endDate, String[] staffIds)
        {
            return agent.agentReceive.GetReceives(startDate, endDate, staffIds);
        }



        /**
         * @brief 검색 시작 일자와 검색 종료 일자, 직원(주치의)들의 정보로 예약 후 접수한 접수 현황검색
         * @param startDate: 검색시작일자 (yyyy-MM-dd)
         * @param endDate: 검색 종료 일자(yyy-MM-dd)
         * @param staffIds: 직원(주치의) 아이디 String 배열
         * @return 예약 후 접수 현황 리스트
         */
        public List<Receive> GetReceivesFromReservation(string startDate, string endDate, string[] staffIds)
        {
            return agent.agentReceive.GetReceivesFromReservation(startDate, endDate, staffIds);
        }

        /**
       * @brief 검색 시작 일자와 검색 종료 일자, 직원(주치의)들의 정보로 예약 후 접수, 수납완료 된 접수현황 검색
       * @param startDate 검색시작일자(yyyy-MM-dd)
       * @param endDate 검색종료일자(yyyy-MM-dd)
       * @param staffIds: 직원(주치의) 아이디 String배열
       * @return 예약 후 접수 및 수납 완료 현황 리스트
       */
        public List<Receive> GetReceiptsFromReservation(string startDate, string endDate, string[] staffIds)
        {
            return agent.agentReceive.GetReceiptsFromReservation(startDate, endDate, staffIds);
        }


        #endregion

        #region 4 예약서버
        /**
         * @brief 확인한 예약 현황을 JSON 형식으로 송출하기 위한 구조 생성
         * @param isDiff: true - 이전 전송한 것의 차이분만 전송 / false - 전체 내용 전송
         * @return JSON 전송을 위한 구조 결과
         */
        public List<OfficeReservationState> GetReservationAwaiterCount(bool isDiff)
        {
            List<CategoryOfficeStaff> categoryOfficeStaffs = agent.agentHospital.GetCategoryOfficeStaffs();

            var staffCategories = categoryOfficeStaffs.GroupBy(x => x.staff, x => x.category).ToDictionary(g => g.Key, g => g.ToList());

            //List<Reservation> currentOfficeReservationStateItemList = agent.GetReservations(null, null);
            List<Reservation> currentOfficeReservationStateItemList = agent.agentReservation.GetReservations(null, new List<CategoryOfficeStaff>());
            
            // 변경분만 전송
            if (isDiff == true)
            {
                currentOfficeReservationStateItemList = MakeDiffReservationStateItemList(currentOfficeReservationStateItemList);
            }

            #region 확인한 예약 현황을 JSON 형식으로 송출하기 위한 구조 생성
            List<OfficeReservationState> currentOfficeReservationStateList = new List<OfficeReservationState>();

            if (currentOfficeReservationStateItemList == null)
            {
            }
            else
            {
                var groupbyOfficeStaff = currentOfficeReservationStateItemList.GroupBy(x => new { staffId = x.staffId, staff = x.staff }, x => new { confirmReservationDatetime = x.confirmReservationDatetime, patients = new Patient(x.chartNo, x.emrReservId) });

                foreach (var item in groupbyOfficeStaff)
                {
                    OfficeReservationState ors = new OfficeReservationState(item.Key.staffId, item.Key.staff, new List<ReservationState>());

                    ors.categories = staffCategories[ors.staff];

                    var groupByDatetime = item.GroupBy(x => x.confirmReservationDatetime, x => x.patients).ToList();

                    for (int idx = 0; idx < groupByDatetime.Count(); idx++)
                    {
                        ors.reservationStateList.Add(new ReservationState(groupByDatetime[idx].Key, groupByDatetime[idx].Count()));
                    }

                    currentOfficeReservationStateList.Add(ors);
                }
            }
            #endregion

            return currentOfficeReservationStateList;
        }

        /**
         * @brief 예약 현황에서 마지막 버전과 차이 부분만 찾아서 반환
         * @param currentOfficeReservationStateItemList: 예약 현황
         * @return 차이 부분
         */
        private List<Reservation> MakeDiffReservationStateItemList(List<Reservation> currentOfficeReservationStateItemList)
        {
            // 마지막으로 확인한 예약 현황에 대한 별칭
            var last = lastOfficeReservationStateItemList;
            // 현재 확인한 예약 현황에 대한 별칭
            var current = currentOfficeReservationStateItemList;

            if (last == null)
            {
                lastOfficeReservationStateItemList = current;
            }
            else
            {
                // 현재 확인한 예약 현황의 깊은 복사본 생성
                var currentCopy = new Reservation[current.Count];
                current.CopyTo(currentCopy);

                // Unit Test 방어 코드
                if (last == null)
                { }
                else
                {
                    // 현재 확인한 예약 현황에서 마지막 확인한 예약 현황 제거
                    for (int i = current.Count - 1; i >= 0; i--)
                    {
                        for (int idx = last.Count - 1; idx >= 0; idx--)
                        {
                            if (current[i].ToString().Equals(last[idx].ToString()))
                            {
                                current.RemoveAt(i);
                                break;
                            }
                        }
                    }
                }

                // 현재 확인한 예약 현황을 마지막 확인한 예약 현황으로 복사
                lastOfficeReservationStateItemList = currentCopy.ToList<Reservation>();
            }

            return current;
        }

        public List<String> GetCategories()
        {
            List<CategoryOfficeStaff> categoryOfficeStaffs = agent.agentHospital.GetCategoryOfficeStaffs();

            List<String> categories = categoryOfficeStaffs.Select(x => x.category).Distinct().ToList<String>();

            return categories;
        }

        public Object GetRealTimeAwaiterCount()
        {
            List<AwaiterCounter> list = agent.agentHospital.GetRealTimeAwaiterCount();

            // 진료과별 실시간 대기자수 + [진료과 & 직원(주치의)별 실시간 대기자수]
            var officeAwaiterCounter = list
                .GroupBy(x => x.office, x => x.count)
                .Select(group => new { office = group.Key, count = group.Sum(), staffDetail = new List<AwaiterCounter>() })
                .ToList();

            // 병원 전체 실시간 대기자수
            officeAwaiterCounter
                .ForEach(x => list
                    .ForEach(y => { if (x.office.Equals(y.office)) x.staffDetail.Add(y); }));

            var totalAwaiterCount = officeAwaiterCounter.Sum(group => group.count);

            var returnObject = new { totalCount = totalAwaiterCount, officeDetail = officeAwaiterCounter };

            return returnObject;
        }
        #endregion
        #endregion




    }
}