﻿using System;
using System.Collections.Generic;
using EMRClientLib.Domain;
using EMRClientLib.Interface;

namespace EMRProxy
{
    public interface IProxy
    {
        /// IDdocdocAgent 구현체(EMR 업체별 구현 필요)
        AbstractAgentDerive agent { get; }

        /// 마지막으로 전송한 예약 현황
        List<Reservation> lastOfficeReservationStateItemList { get; set; }
        /// Ddocdoc API Server 로그인을 위한 병원 ID
        string HospitalId { get; }
        /// Ddocdoc API Server 로그인을 위한 병원 비밀번호
        string HospitalPassword { get; }
        
        #region EventHandler
        /// EMR 에서 진료 대기자 수 변동 시
        void AddListenerOnChangeRealtimeAwaiterCounterUsingEMR(DelegateChangeRealtimeAwaiterCounterFromEMR listener);
        /// EMR 에서 예약을 생성한 경우
        void AddListenerOnCreateReservationUsingEMR(DelegateCreateReservationFromEMR listener);
        /// EMR 에서 예약을 변경한 경우
        void AddListenerOnDeleteReservationUsingEMRR(DelegateDeleteReservationFromEMR listener);
        /// EMR 에서 예약을 삭제한 경우
        void AddListenerOnReceiptReservationUsingEMR(DelegateReceiptReservationFromEMR listener);
        /// EMR 에서 예약을 접수한 경우
        void AddListenerOnReceiveReservationUsingEMR(DelegateReceiveReservationFromEMR listener);
        /// EMR 에서 예약을 수납한 경우
        void AddListenerOnUpdateReservationUsingEMR(DelegateUpdateReservationFromEMR listener);
        #endregion

        #region 4 PC클라이언트
        /**
         * @brief 차트 번호를 자동 생성 여부
         * @return bool (자동 생성: true / 수동 생성: false)
         */
        bool IsChartNoAutoGenerate();

        /**
         * @brief 연속 메모(환자 이력) 사용 여부
         * @return bool (사용: true / 미사용: false)
         */
        bool UseComment();

        /**
         * @brief 진료시간 요청
         * @return 진료시간 리스트
         * @details 휴무일은 전송하지 않는다.
         */
        List<Timetable> GetTimetable();

        /**
         * @brief 휴무 정보 조회
         * @return 휴무 정보 리스트
         * @details 오늘 이후의 휴무 일정 전체를 반환
         */
        Dictionary<string, List<DayOffInfo>> GetDayOffSchedule();

        /**
         * @brief 진료실별 의사 목록 조회
         * @return 진료실별 의사 목록 목록
         */
        object GetOfficeStaffs();

        /**
         * @brief 청구진료과별 의사 목록 조회
         * @return 청구진료과별 의사 목록 목록
         */
        object GetStaffs();

        /**
        * @brief 차트 정보 조회
        * @param name: 환자 성명
        * @param birthDate: 생년월일(yyyy-MM-dd)
        * @param gender: 성별(M/F)
        * @return 차트 정보 리스트
        * @details 성별 코드 참조 - http://marga.tistory.com/488
        */
        List<Chart> GetCharts(string name, string birthDate, string gender);


        /**
         * @brief 마지막 생성된 차트번호 조회
         * @return 마지막 차트번호
         */
        String GetLastestChartNO();


        /**
         * @brief 일자와 진료실&직원(주치의) 정보로 예약 현황 검색
         * @param date: 검색 일자(yyyy-MM-dd)
         * @param officeArray: 진료실&직원(주치의) String 배열
         * @return 일시별 진료실&직원(주치의)별 예약 현황
         */
        List<Reservation> GetReservations(string date, string[] officeArray);
        List<Reservation> GetReservations(string startDate, string endDate, string[] officeArray);

        /**
         * @brief 예약 생성
         * @param reservation: Reservation
         * @return emrReservId
         */
        String CreateReservation(Reservation reservation);

        /**
         * @brief 예약 변경
         * @param reservation: Reservation
         * @return 성공 여부
         */
        bool UpdateReservation(Reservation reservation);

        /**
         * @brief 예약 삭제
         * @param reservation: Reservation
         * @return 성공 여부
         */
        bool DeleteReservation(Reservation reservation);

        /**
         * @brief 접수 상세 정보 조회
         * @param receiveId: 접수 번호
         * @return 접수 상세 정보
         */
        Receive GetReceive(String receiveId);

        /**
         * @brief 검색 시작 일자와 검색 종료 일자, 직원(주치의)들의 정보로 접수 현황 검색
         * @param startDate: 검색 시작 일자(yyyy-MM-dd)
         * @param endDate: 검색 종료 일자(yyyy-MM-dd)
         * @param staffIds: 직원(주치의) 아이디 리스트 List<String>
         * @return 접수 현황 리스트
         */
        List<Receive> GetReceives(String startDate, String endDate, String[] staffIds);


        /**
         * @brief 검색 시작 일자와 검색 종료 일자, 직원(주치의)들의 정보로 예약 후 접수한 접수 현황검색
         * @param startDate: 검색시작일자 (yyyy-MM-dd)
         * @param endDate: 검색 종료 일자(yyy-MM-dd)
         * @param staffIds: 직원(주치의) 아이디 String 배열
         * @return 예약 후 접수 현황 리스트
         */
        List<Receive> GetReceivesFromReservation(String startDate, String endDate, String[] staffIds);


        /**
         * @brief 검색 시작 일자와 검색 종료 일자, 직원(주치의)들의 정보로 예약 후 접수, 수납완료 된 접수현황 검색
         * @param startDate 검색시작일자(yyyy-MM-dd)
         * @param endDate 검색종료일자(yyyy-MM-dd)
         * @param staffIds: 직원(주치의) 아이디 String배열
         * @return 예약 후 접수 및 수납 완료 현황 리스트
         */
        List<Receive> GetReceiptsFromReservation(String startDate, String endDate, String[] staffIds);

        #endregion

        #region 4 예약서버
        /**
         * @brief 진료실&의사별 예약 대기자수 현황 조회
         * @param isDiff: true: 이전 전송에 대한 차이만 전송 / false: 전체 전송
         * @return 진료실&의사별 예약 대기자수 현황
         */
        List<OfficeReservationState> GetReservationAwaiterCount(bool isDiff);

        /**
         * @brief 진료청구과 목록 조회
         * @return JSON 전송을 위한 구조 결과
         */        
        List<string> GetCategories();
		
        /**
         * @brief 진료실별, 의사별 실시간 대기자수
         * @return 진료실별, 의사별 실시간 대기자수 목록
         */     
        object GetRealTimeAwaiterCount();
		
        /**
         * @brief 예약 상세 정보 조회
         * @param emrReservId: EMR 예약 아이디
         * @return 예약 상세 정보
         */     
        Reservation GetReservation(string emrReservId);
        #endregion
    }
}
