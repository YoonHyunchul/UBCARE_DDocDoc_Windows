﻿using EMRClientLib.Interface;
using EMRClientLib.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Threading;

namespace Mock
{
    public class MockAgentHospital : AbstractAgentHospital
    {
        private DateTime _ToDay = DateTime.Today;
        List<Chart> _ChartInfoList = new List<Chart>();
        private List<Reservation> _ReservationList = new List<Reservation>();

        public MockAgentHospital()
        {
            // Start: 차트 정보
            _ChartInfoList.Add(new Chart("12341", "emrUsrId01", "홍길동", "010-1234-1231", "1999-09-01", "M", "서울","연속"));
            _ChartInfoList.Add(new Chart("12342", "emrUsrId02", "일지매", "010-1234-1232", "1999-09-02", "M", "부산", "연속"));
            _ChartInfoList.Add(new Chart("12343", "emrUsrId03", "설현", "010-1234-1233", "1999-09-03", "F", "대구", "연속"));
            _ChartInfoList.Add(new Chart("12344", "emrUsrId04", "강감찬", "010-1234-1234", "1999-09-04", "M", "대전", "연속"));
            _ChartInfoList.Add(new Chart("12345", "emrUsrId05", "배수지", "010-1234-1235", "2001-09-05", "F", "제주", "연속"));
        }

        private void setReservationList()
        {
            _ReservationList.Clear();

            // Start: 예약
            // "치과보철과", "김보철"
            _ReservationList.Add(new Reservation("emrReserveId001", null, "김수현", "010-1234-1201", "1999-09-01", "M", "su001", "", "2016-09-21 13:00", "치과보철과", "chibo", "치과보철과", "Kim", "김보철", "chartNo001", "예약메모", "연속메모"));
            _ReservationList.Add(new Reservation("emrReserveId002", null, "설현", "010-1234-1202", "1999-09-02", "F", "se001", "", "2016-09-21 13:10", "치과보철과", "chibo", "치과보철과", "Kim", "김보철", "chartNo002", "예약메모", "연속메모"));

            _ReservationList.Add(new Reservation("emrReserveId003", null, "이지아", "010-1234-1203", "1999-09-03", "F", "ji001", "", "2016-09-21 14:10", "치과보철과", "chibo", "치과보철과", "Kim", "김보철", "chartNo003", "예약메모", "연속메모"));
            _ReservationList.Add(new Reservation("emrReserveId004", null, "배수지", "010-1234-1204", "1999-09-04", "F", "su002", "", "2016-09-21 15:20", "치과보철과", "chibo", "치과보철과", "Kim", "김보철", "chartNo004", "예약메모", "연속메모"));

            // "치과보철과", "한보철"
            _ReservationList.Add(new Reservation("emrReserveId005", null, "강마루", "010-1234-1205", "1999-09-05", "M", "km001", "", "2016-09-21 12:00", "치과보철과", "chibo", "치과보철과", "Han", "한보철", "chartNo005", "예약메모", "연속메모"));
            _ReservationList.Add(new Reservation("emrReserveId006", null, "홍길동", "010-1234-1206", "1999-09-06", "M", "hg001", "", "2016-09-21 12:20", "치과보철과", "chibo", "치과보철과", "Han", "한보철", "chartNo006", "예약메모", "연속메모"));

            _ReservationList.Add(new Reservation("emrReserveId007", null, "일지매", "010-1234-1207", "1999-09-07", "M", "ij001", "", "2016-09-22 13:10", "치과보철과", "chibo", "치과보철과", "Han", "한보철", "chartNo007", "예약메모", "연속메모"));

            _ReservationList.Add(new Reservation("emrReserveId008", null, "이몽룡", "010-1234-1208", "1999-09-08", "M", "lm001", "", "2016-09-23 11:00", "치과보철과", "chibo", "치과보철과", "Han", "한보철", "chartNo008", "예약메모", "연속메모"));
            _ReservationList.Add(new Reservation("emrReserveId009", null, "변학도", "010-1234-1209", "1999-09-09", "M", "bh001", "", "2016-09-23 12:00", "치과보철과", "chibo", "치과보철과", "Han", "한보철", "chartNo009", "예약메모", "연속메모"));
            _ReservationList.Add(new Reservation("emrReserveId010", null, "성춘향", "010-1234-1210", "1999-09-10", "F", "sj001", "", "2016-09-23 17:00", "치과보철과", "chibo", "치과보철과", "Han", "한보철", "chartNo010", "예약메모", "연속메모"));

            // "치과교정과", "이구강"                          
            _ReservationList.Add(new Reservation("emrReserveId011", null, "로미오", "010-1234-1211", "1999-09-11", "M", "ro001", "", "2016-09-22 13:10", "치과교정과", "chikyo", "치과교정과", "Lee", "이구강", "chartNo011", "예약메모", "연속메모"));
            _ReservationList.Add(new Reservation("emrReserveId012", null, "줄리엣", "010-1234-1212", "1999-09-12", "F", "ju001", "", "2016-09-22 13:20", "치과교정과", "chikyo", "치과교정과", "Lee", "이구강", "chartNo012", "예약메모", "연속메모"));

            _ReservationList.Add(new Reservation("emrReserveId007", null, "레오", "010-1234-1213", "1999-09-13", "M", "lo001", "", "2016-09-22 13:30", "치과교정과", "chikyo", "치과교정과", "Lee", "이구강", "chartNo007", "예약메모", "연속메모"));

            _ReservationList.Add(new Reservation("emrReserveId013", null, "키티", "010-1234-1214", "1999-09-14", "F", "kt001", "", "2016-09-23 10:10", "치과교정과", "chikyo", "치과교정과", "Lee", "이구강", "chartNo013", "예약메모", "연속메모"));
            _ReservationList.Add(new Reservation("emrReserveId014", null, "니모", "010-1234-1215", "1999-09-15", "M", "nm001", "", "2016-09-23 11:00", "치과교정과", "chikyo", "치과교정과", "Lee", "이구강", "chartNo014", "예약메모", "연속메모"));
            _ReservationList.Add(new Reservation("emrReserveId015", null, "도리", "010-1234-1216", "1999-09-16", "F", "dr001", "", "2016-09-23 14:20", "치과교정과", "chikyo", "치과교정과", "Lee", "이구강", "chartNo015", "예약메모", "연속메모"));

            _ReservationList.Add(new Reservation("emrReserveId015", null, "곡성", "010-1234-1217", "1999-09-17", "M", "gs001", "", "2016-10-03 14:20", "치과교정과", "chikyo", "치과교정과", "Han", "한보철", "chartNo016", "예약메모", "연속메모"));

        }

        #region SQLServer Change Event Handler Test
        // https://msdn.microsoft.com/en-us/library/3ht3391b(v=vs.80).aspx
        protected int changeCount = 0;

        protected const string tableName = "RsvInf";
        protected const string statusMessage =
            "{0} changes have occurred.";
        protected Boolean exitRequested = false;
        protected Boolean waitInProgress = false;

        // The following objects are reused
        // for the lifetime of the application.
        protected SqlConnection connection = null;
        protected SqlCommand command = null;

        // The Service Name is required to correctly 
        // register for notification.
        // The Service Name must be already defined with
        // Service Broker for the database you are querying.
        protected const string ServiceName = "Service=ContactChangeNotifications";

        // The database name is needed for both the connection
        // string and the SqlNotificationRequest.Options property.
        protected const string DatabaseName = "drbitpack";


        // Specify how long the notification request
        // should wait before timing out.
        // This value waits for 30 seconds. 
        protected int NotificationTimeout = 30;

        private string GetConnectionString()
        {
            // To avoid storing the connection string in your code,
            // you can retrive it from a configuration file.

            // In general, client applications don't need to incur the
            // overhead of connection pooling.
            return String.Format("Data Source=(local);Integrated Security=true;Initial Catalog={0};Pooling=False;Asynchronous Processing=true;", DatabaseName);
        }

        private string GetSQL()
        {
            return "SELECT top 10 * FROM RsvInf ORDER BY RsvDtm DESC;";
        }

        private string GetListenerSQL()
        {
            // Note that ContactChangeMessages is the name
            // of the Service Broker queue that must
            // be already defined.
            return "WAITFOR (RECEIVE * FROM ContactChangeMessages);";
        }

        private void Listen()
        {
            using (SqlCommand command =
                new SqlCommand(GetListenerSQL(), connection))
            {
                // Make sure we don't time out before the
                // notification request times out.
                if (connection.State != ConnectionState.Open)
                {
                    connection.Open();
                }

                command.CommandTimeout = NotificationTimeout + 120;

                AsyncCallback callBack = new AsyncCallback(
                    this.OnReaderComplete);
                IAsyncResult asynResult = command.BeginExecuteReader(
                    callBack, command);
                if (asynResult.IsCompleted == true)
                {
                    waitInProgress = true;
                }
            }
        }

        private void OnReaderComplete(IAsyncResult asynResult)
        {            
            // At this point, this code will run on the UI thread.
            try
            {
                waitInProgress = false;
                SqlDataReader reader = ((SqlCommand)asynResult.AsyncState)
                    .EndExecuteReader(asynResult);
                while (reader.Read())
                // Empty queue of messages.
                // Application logic could partse
                // the queue data to determine why things.
                //{
                //    for (int i = 0; i <= reader.FieldCount - 1; i++)
                //        Console.WriteLine(reader[i].ToString());
                //}

                reader.Close();
                changeCount += 1;

                // The user can decide to request
                // a new notification by
                // checking the CheckBox on the form.
                // However, if the user has requested to
                // exit, we need to do that instead.
                if (exitRequested == true)
                {
                    
                }
                else
                {
                    GetData(true);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private void GetData(bool Register)
        {
            // Make sure the command object does not already have
            // a notification object associated with it.
            command.Notification = null;

            if (Register)
            {
                // Create and bind the SqlNotificationRequest object
                // to the command object.
                SqlNotificationRequest request =
                    new SqlNotificationRequest();
                request.UserData = new Guid().ToString();
                request.Options = String.Format(
                    "Service={0};local database={1}",
                    ServiceName, DatabaseName);

                // If a time-out occurs, a notification
                // will indicating that is the 
                // reason for the notification.
                request.Timeout = NotificationTimeout;
                command.Notification = request;
            }

            if (Register)
            {
                // Start the background listener.
                this.Listen();
            }
        }

        #endregion
        
        #region 로그인
        /**
         * @brief API Server 로 로그인을 하기 위한 병원 ID 조회
         * @return 병원 ID
         */
        public override String GetHospitalId()
        {
            return "12345678";
        }

        /**
         * @brief API Server 로 로그인을 하기 위한 병원 비밀번호 조회
         * @return 병원 비밀번호
         */
        public override String GetHospitalPassword()
        {
            return "12345678";
        }

        /**
         * @brief 차트 번호를 자동 생성하는지 확인
         * @return bool (자동 생성: true / 수동 생성: false)
         */
        public override bool IsChartNoAutoGenerate()
        {
            return true;
        }

        /**
         * @brief 연속 메모(환자 이력) 사용 여부
         * @return bool (사용: true / 미사용: false)
         */
        public override bool UseComment()
        {
            return false;
        }
        #endregion

        #region 청구진료과, 진찰실, 직원(주치의) 조회
        /**
         * @brief 청구진료과, 진료실, 직원(주치의) 조회
         * @return 청구진료과, 진료실, 직원(주치의) 목록
         */
        public override List<CategoryOfficeStaff> GetCategoryOfficeStaffs()
        {
            List<CategoryOfficeStaff> list = new List<CategoryOfficeStaff>();

            list.Add(new CategoryOfficeStaff("치과보철과", "chibo", "치과보철과", "Kim", "김보철", "98765"));
            list.Add(new CategoryOfficeStaff("치과보철과", "chibo", "치과보철과", "Han", "한보철", ""));
            list.Add(new CategoryOfficeStaff("치과교정과", "chikyo", "치과교정과", "Han", "한보철", ""));
            list.Add(new CategoryOfficeStaff("치과교정과", "chikyo", "치과교정과", "Lee", "이구강", ""));

            return list;
        }
        #endregion

        #region 진료시간
        /**
         * @brief 진료시간 요청
         * @return 진료시간 리스트
         */
        #region Example
        /*
[  
   {  
      "title":"월요일",
      "startTime":"09:00",
      "endTime":"18:00",
      "breakTime":[  
         {  
            "title":"점심",
            "startTime":"12:00",
            "endTime":"13:00"
         },
         {  
            "title":"저녁",
            "startTime":"18:00",
            "endTime":"19:00"
         }
      ]
   },
   {  
      "title":"화요일",
      "startTime":"09:00",
      "endTime":"20:00",
      "breakTime":[  
         {  
            "title":"점심",
            "startTime":"12:00",
            "endTime":"13:00"
         },
         {  
            "title":"저녁",
            "startTime":"18:00",
            "endTime":"19:00"
         }
      ]
   },
   {  
      "title":"목요일",
      "startTime":"09:00",
      "endTime":"20:00",
      "breakTime":[  
         {  
            "title":"점심",
            "startTime":"12:00",
            "endTime":"13:00"
         },
         {  
            "title":"저녁",
            "startTime":"18:00",
            "endTime":"19:00"
         }
      ]
   },
   {  
      "title":"금요일",
      "startTime":"09:00",
      "endTime":"20:00",
      "breakTime":[  
         {  
            "title":"점심",
            "startTime":"12:00",
            "endTime":"13:00"
         },
         {  
            "title":"저녁",
            "startTime":"18:00",
            "endTime":"19:00"
         }
      ]
   }
]
         */
        #endregion
        public override List<Timetable> GetTimetable()
        {
            List<Timetable> timeTableList = new List<Timetable>();
            List<BreakTime> breakTimeList = null;

            breakTimeList = new List<BreakTime>();
            breakTimeList.Add(new BreakTime("점심", "12:00", "13:00"));
            breakTimeList.Add(new BreakTime("저녁", "18:00", "19:00"));
            timeTableList.Add(new Timetable("월요일", "09:00", "18:00", breakTimeList));

            breakTimeList = new List<BreakTime>();
            breakTimeList.Add(new BreakTime("점심", "12:00", "13:00"));
            breakTimeList.Add(new BreakTime("저녁", "18:00", "19:00"));
            timeTableList.Add(new Timetable("화요일", "09:00", "20:00", breakTimeList));

            breakTimeList = new List<BreakTime>();
            breakTimeList.Add(new BreakTime("점심", "12:00", "13:00"));
            breakTimeList.Add(new BreakTime("저녁", "18:00", "19:00"));
            timeTableList.Add(new Timetable("목요일", "09:00", "20:00", breakTimeList));

            breakTimeList = new List<BreakTime>();
            breakTimeList.Add(new BreakTime("점심", "12:00", "13:00"));
            breakTimeList.Add(new BreakTime("저녁", "18:00", "19:00"));
            timeTableList.Add(new Timetable("금요일", "09:00", "20:00", breakTimeList));

            return timeTableList;
        }

        /**
         * @brief 휴무 정보 조회
         * @return 휴무 정보 리스트
         * @details 오늘 이후의 휴무 일정 전체를 반환
         */
        public override Dictionary<String, List<DayOffInfo>> GetDayOffSchedule()
        {
            Dictionary<String, List<DayOffInfo>> staffDayOffInfos = new Dictionary<String, List<DayOffInfo>>();

            List<DayOffInfo> dayOffInfos = new List<DayOffInfo>();

            dayOffInfos.Add(new DayOffInfo("여름휴가", "2016-08-08"));
            dayOffInfos.Add(new DayOffInfo("여름휴가", "2016-08-09"));
            dayOffInfos.Add(new DayOffInfo("여름휴가", "2016-08-10"));
            dayOffInfos.Add(new DayOffInfo("광복절", "2016-08-15"));
            dayOffInfos.Add(new DayOffInfo("추석연휴", "2016-09-14"));
            dayOffInfos.Add(new DayOffInfo("추석연휴", "2016-09-15"));
            dayOffInfos.Add(new DayOffInfo("추석연휴", "2016-09-16"));

            staffDayOffInfos.Add("김보철", dayOffInfos);

            dayOffInfos.Clear();

            dayOffInfos.Add(new DayOffInfo("여름휴가", "2016-08-08"));
            dayOffInfos.Add(new DayOffInfo("여름휴가", "2016-08-09"));
            dayOffInfos.Add(new DayOffInfo("여름휴가", "2016-08-10"));
            dayOffInfos.Add(new DayOffInfo("광복절", "2016-08-15"));
            dayOffInfos.Add(new DayOffInfo("추석연휴", "2016-09-14"));
            dayOffInfos.Add(new DayOffInfo("추석연휴", "2016-09-15"));
            dayOffInfos.Add(new DayOffInfo("추석연휴", "2016-09-16"));

            staffDayOffInfos.Add("한보철", dayOffInfos);

            dayOffInfos.Clear();

            dayOffInfos.Add(new DayOffInfo("여름휴가", "2016-08-08"));
            dayOffInfos.Add(new DayOffInfo("여름휴가", "2016-08-09"));
            dayOffInfos.Add(new DayOffInfo("여름휴가", "2016-08-10"));
            dayOffInfos.Add(new DayOffInfo("광복절", "2016-08-15"));
            dayOffInfos.Add(new DayOffInfo("추석연휴", "2016-09-14"));
            dayOffInfos.Add(new DayOffInfo("추석연휴", "2016-09-15"));
            dayOffInfos.Add(new DayOffInfo("추석연휴", "2016-09-16"));

            staffDayOffInfos.Add("이구강", dayOffInfos);

            return staffDayOffInfos;
        }

        #endregion

        #region 진료 대기자
        /**
         * @brief 실시간 대기자수 조회
         * @return 진료실별, 의사별 실시간 대기자수 목록
         */
        public override List<AwaiterCounter> GetRealTimeAwaiterCount()
        {
            List<AwaiterCounter> awaiterCounterList = new List<AwaiterCounter>();

            awaiterCounterList.Add(new AwaiterCounter("chibo", "치과보철과", "Kim", "김보철", 2));
            awaiterCounterList.Add(new AwaiterCounter("chibo", "치과보철과", "Han", "한보철", 1));
            awaiterCounterList.Add(new AwaiterCounter("chikyo", "치과교정과", "Lee", "이구강", 5));

            return awaiterCounterList;
        }

        #endregion

        #region 차트 정보
        /**
         * @brief 차트 정보 조회
         * @param name: 환자 성명
         * @param birthDate: 생년월일(yyyy-MM-dd)
         * @param genter: 성별(M/F)
         * @return 차트 정보 리스트
         */
        public override List<Chart> GetCharts(String name, String birthDate, String gender)
        {
            List<Chart> chartInfoList = _ChartInfoList;

            return chartInfoList;
        }
        #endregion
    }
}