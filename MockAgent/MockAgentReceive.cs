﻿using EMRClientLib.Interface;
using EMRClientLib.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Threading;

namespace Mock
{
    public class MockAgentReservation : AbstractAgentReservation
    {
        private DateTime _Today = DateTime.Today;
        List<Chart> _ChartInfoList = new List<Chart>();
        private List<Reservation> _ReservationList = new List<Reservation>();
        
        public MockAgentReservation()
        {
            _ChartInfoList.Add(new Chart("12341", "emrUsrId01", "홍길동", "010-1234-1231", "1999-09-01", "M", "서울","연속메모"));
            _ChartInfoList.Add(new Chart("12342", "emrUsrId02", "일지매", "010-1234-1232", "1999-09-02", "M", "부산", "연속메모"));
            _ChartInfoList.Add(new Chart("12343", "emrUsrId03", "설현", "010-1234-1233", "1999-09-03", "F", "대구","연속메모"));
            _ChartInfoList.Add(new Chart("12344", "emrUsrId04", "강감찬", "010-1234-1234", "1999-09-04", "M", "대전", "연속메모"));
            _ChartInfoList.Add(new Chart("12345", "emrUsrId05", "배수지", "010-1234-1235", "2001-09-05", "F", "제주", "연속메모"));
        }

        private void setReservationList()
        {
            _ReservationList.Clear();

            // Start: 예약
            // "치과보철과" ,"김보철"
            _ReservationList.Add(new Reservation("emrReserveId001", null, "김수현", "010-1234-1201", "1999-09-01", "M", "su001", "", "2016-09-21 13:00", "치과보철과", "chibo", "치과보철과", "Kim", "김보철", "chartNo001", "예약메모", "연속메모"));
            _ReservationList.Add(new Reservation("emrReserveId002", null, "설현", "010-1234-1202", "1999-09-02", "F", "se001", "", "2016-09-21 13:10", "치과보철과", "chibo", "치과보철과", "Kim", "김보철", "chartNo002", "예약메모", "연속메모"));

            _ReservationList.Add(new Reservation("emrReserveId003", null, "이지아", "010-1234-1203", "1999-09-03", "F", "ji001", "", "2016-09-21 14:10", "치과보철과", "chibo", "치과보철과", "Kim", "김보철", "chartNo003", "예약메모", "연속메모"));
            _ReservationList.Add(new Reservation("emrReserveId004", null, "배수지", "010-1234-1204", "1999-09-04", "F", "su002", "", "2016-09-21 15:20", "치과보철과", "chibo", "치과보철과", "Kim", "김보철", "chartNo004", "예약메모", "연속메모"));

            // "치과보철과", "한보철"
            _ReservationList.Add(new Reservation("emrReserveId005", null, "강마루", "010-1234-1205", "1999-09-05", "M", "km001", "", "2016-09-21 12:00", "치과보철과", "chibo", "치과보철과", "Han", "한보철", "chartNo005", "예약메모", "연속메모"));
            _ReservationList.Add(new Reservation("emrReserveId006", null, "홍길동", "010-1234-1206", "1999-09-06", "M", "hg001", "", "2016-09-21 12:20", "치과보철과", "chibo", "치과보철과", "Han", "한보철", "chartNo006", "예약메모", "연속메모"));

            _ReservationList.Add(new Reservation("emrReserveId007", null, "일지매", "010-1234-1207", "1999-09-07", "M", "ij001", "", "2016-09-22 13:10", "치과보철과", "chibo", "치과보철과", "Han", "한보철", "chartNo007", "예약메모", "연속메모"));

            _ReservationList.Add(new Reservation("emrReserveId008", null, "이몽룡", "010-1234-1208", "1999-09-08", "M", "lm001", "", "2016-09-23 11:00", "치과보철과", "chibo", "치과보철과", "Han", "한보철", "chartNo008", "예약메모", "연속메모"));
            _ReservationList.Add(new Reservation("emrReserveId009", null, "변학도", "010-1234-1209", "1999-09-09", "M", "bh001", "", "2016-09-23 12:00", "치과보철과", "chibo", "치과보철과", "Han", "한보철", "chartNo009", "예약메모", "연속메모"));
            _ReservationList.Add(new Reservation("emrReserveId010", null, "성춘향", "010-1234-1210", "1999-09-10", "F", "sj001", "", "2016-09-23 17:00", "치과보철과", "chibo", "치과보철과", "Han", "한보철", "chartNo010", "예약메모", "연속메모"));

            // "치과교정과", "이구강"                          
            _ReservationList.Add(new Reservation("emrReserveId011", null, "로미오", "010-1234-1211", "1999-09-11", "M", "ro001", "", "2016-09-22 13:10", "치과교정과", "chikyo", "치과교정과", "Lee", "이구강", "chartNo011", "예약메모", "연속메모"));
            _ReservationList.Add(new Reservation("emrReserveId012", null, "줄리엣", "010-1234-1212", "1999-09-12", "F", "ju001", "", "2016-09-22 13:20", "치과교정과", "chikyo", "치과교정과", "Lee", "이구강", "chartNo012", "예약메모", "연속메모"));

            _ReservationList.Add(new Reservation("emrReserveId007", null, "레오", "010-1234-1213", "1999-09-13", "M", "lo001", "", "2016-09-22 13:30", "치과교정과", "chikyo", "치과교정과", "Lee", "이구강", "chartNo007", "예약메모", "연속메모"));

            _ReservationList.Add(new Reservation("emrReserveId013", null, "키티", "010-1234-1214", "1999-09-14", "F", "kt001", "", "2016-09-23 10:10", "치과교정과", "chikyo", "치과교정과", "Lee", "이구강", "chartNo013", "예약메모", "연속메모"));
            _ReservationList.Add(new Reservation("emrReserveId014", null, "니모", "010-1234-1215", "1999-09-15", "M", "nm001", "", "2016-09-23 11:00", "치과교정과", "chikyo", "치과교정과", "Lee", "이구강", "chartNo014", "예약메모", "연속메모"));
            _ReservationList.Add(new Reservation("emrReserveId015", null, "도리", "010-1234-1216", "1999-09-16", "F", "dr001", "", "2016-09-23 14:20", "치과교정과", "chikyo", "치과교정과", "Lee", "이구강", "chartNo015", "예약메모", "연속메모"));

            _ReservationList.Add(new Reservation("emrReserveId015", null, "곡성", "010-1234-1217", "1999-09-17", "M", "gs001", "", "2016-10-03 14:20", "치과교정과", "chikyo", "치과교정과", "Han", "한보철", "chartNo016", "예약메모", "연속메모"));
        }

        #region SQLServer Change Event Handler Test

        protected int changeCount = 0;

        protected const string tableName = "RsvInf";
        protected const string statusMessage = "{0} changes have occurred";
        protected Boolean exitRequested = false;
        protected Boolean waitInProgress = false;

        protected SqlConnection connection = null;
        protected SqlCommand command = null;

        protected const string ServiceName = "Service=ContactChangeNotification";

        protected const string DatabaseName = "drbitpack";

        protected int NotificationTimeout = 30;

        private string GetConnectionString()
        {
            return String.Format("Data Source=(local);Integrated Security=true;Initial Catalog={0};Pooling=False;Asynchronous Processing=true;", DatabaseName);
        }

        private string GetSQL()
        {
            return "SELECT top 10 * FROM RsvInf ORDER BY RsvDtm DESC;";
        }

        private string GetListenerSQL()
        {
            // Note that ContactChangeMessages is the name
            // of the Service Broker queue that must
            // be already defined.
            return "WAITFOR (RECEIVE * FROM ContactChangeMessages);";
        }

        private void Listen()
        {
            using (SqlCommand command =
                new SqlCommand(GetListenerSQL(), connection))
            {
                // Make sure we don't time out before the
                // notification request times out.
                if (connection.State != ConnectionState.Open)
                {
                    connection.Open();
                }

                command.CommandTimeout = NotificationTimeout + 120;

                AsyncCallback callBack = new AsyncCallback(
                    this.OnReaderComplete);
                IAsyncResult asynResult = command.BeginExecuteReader(
                    callBack, command);
                if (asynResult.IsCompleted == true)
                {
                    waitInProgress = true;
                }
            }
        }

        private void OnReaderComplete(IAsyncResult asynResult)
        {
            // At this point, this code will run on the UI thread.
            try
            {
                waitInProgress = false;
                SqlDataReader reader = ((SqlCommand)asynResult.AsyncState)
                    .EndExecuteReader(asynResult);
                while (reader.Read())
                    // Empty queue of messages.
                    // Application logic could partse
                    // the queue data to determine why things.
                    //{
                    //    for (int i = 0; i <= reader.FieldCount - 1; i++)
                    //        Console.WriteLine(reader[i].ToString());
                    //}

                    reader.Close();
                changeCount += 1;

                // The user can decide to request
                // a new notification by
                // checking the CheckBox on the form.
                // However, if the user has requested to
                // exit, we need to do that instead.
                if (exitRequested == true)
                {

                }
                else
                {
                    GetData(true);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private void GetData(bool Register)
        {
            // Make sure the command object does not already have
            // a notification object associated with it.
            command.Notification = null;

            if (Register)
            {
                // Create and bind the SqlNotificationRequest object
                // to the command object.
                SqlNotificationRequest request =
                    new SqlNotificationRequest();
                request.UserData = new Guid().ToString();
                request.Options = String.Format(
                    "Service={0};local database={1}",
                    ServiceName, DatabaseName);

                // If a time-out occurs, a notification
                // will indicating that is the 
                // reason for the notification.
                request.Timeout = NotificationTimeout;
                command.Notification = request;
            }

            if (Register)
            {
                // Start the background listener.
                this.Listen();
            }
        }
        #endregion  


        #region 예약
        /**
         * @brief 차트 번호를 이용해서 환자Id 를 찾고 이를 이용해서 해당 환자의 모든 예약을 검색
         * @param chartNo: 차트번호
         * @return 예약 내역 리스트 (예약일시 내림차순 정렬)
         * @details SELECT * FROM 예약 WHERE 환자Id IN (SELECT DISTINCT 환자ID FROM 차트 WHERE 차트번호 = @chartNo) ORDER BY 예약일시 DESC
         */
        public override List<Reservation> GetReservations(string emrUserId)
        {
            setReservationList();
            List<Reservation> reservationList = _ReservationList;

            return reservationList;
        }

        /**
         * @brief 일자와 진료실&직원(주치의) 정보로 예약 현황 검색
         * @param date: 검색 일자
         * @param offices: [{office, staff}]
         * @return 예약 현황 리스트: [{String office, String staff, String datetime, String chartNo, String emrReservId, String userName}]
         */
        public override List<Reservation> GetReservations(string date, List<CategoryOfficeStaff> offices)
        {
            //Given
            setReservationList();
            List<Reservation> officeReservationStateItemList = _ReservationList;

            //When
            if(!String.IsNullOrEmpty(date))
            {
                officeReservationStateItemList = officeReservationStateItemList.Where(x => x.confirmReservationDatetime.Contains(date)).ToList<Reservation>();
            }
            if (offices != null)
            {
                List<String> office_staff = new List<String>();

                foreach (var officeInfo in offices)
                {
                    office_staff.Add(officeInfo.office + ":" + officeInfo.staff);
                }

                officeReservationStateItemList = officeReservationStateItemList.Where(x => office_staff.Contains(x.office.Trim() + ":" + x.staff.Trim())).ToList<Reservation>();
            }
            //Then  
            return officeReservationStateItemList;

        }

        ///**
        // * @brief 예약 상세 정보 조회
        // * @param emrReservId: EMR 예약 아이디
        // * @return 예약 상세 정보
        // */
        public override Reservation GetReservation(string emrReservId)
        {
            //Given
            setReservationList();
            Reservation reservation = _ReservationList.Where(x => emrReservId.Equals(x.emrReservId)).SingleOrDefault();

            return reservation;
        }

        #endregion

        /**
         * @brief PC클라이언트에서 예약 생성
         * @param reservation: Reservation
         * @return EMR 예약 아이디
         */

        public override String CreateReservation(Reservation reservation)
        {
            //String emrReservId = "emrReservId" + DateTime.Now.ToString("yyyyMMddHHmmssffffzzz");
            String emrReservId = "5791f4e04687f0240bf75a8a";

            return emrReservId;
        }



        /**
         * @brief PC클라이언트에서 예약 변경
         * @param reservation: Reservation
         * @return 성공 여부
         */
        public override bool UpdateReservation(Reservation reservation)
        {
            bool result = false;

            if ("5791f4e04687f0240bf75a8a".Equals(reservation.emrReservId))
            {
                result = true;
            }

            return result;
        }
        
        /**
         * @brief PC클라이언트에서 예약 취소
         * @param reservation: Reservation
         * @return 성공 여부
         */
        public override bool DeleteReservation(Reservation reservation)
        {
            bool result = false;

            if ("5791f4e04687f0240bf75a8a".Equals(reservation.emrReservId))
            {
                result = true;
            }

            return result;
        }




    }
}
