﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EMRClientLib.Domain
{
    /**
     * @brief 환자 정보: 차트번호, EMR 예약 아이디
     */
    public class Patient
    {
        /// 차트번호
        [JsonProperty(PropertyName = "chartNo")]
        public String chartNo { get; set; }

        /// EMR 예약 아이디
        [JsonProperty(PropertyName = "emrReservId")]
        public String emrReservId { get; set; }

        /**
         * @brief 생성자
         * @param chartNo: 차트번호
         * @param emrReservId: EMR 예약 아이디
         */
        public Patient(String chartNo, String emrReservId)
        {
            this.chartNo = chartNo;
            this.emrReservId = emrReservId;
        }
    }
}
