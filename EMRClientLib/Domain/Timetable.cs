﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EMRClientLib.Domain
{
    /**
     * @brief 요일별 진료 시간
     */
    public class Timetable
    {
        /// 요일
        [JsonProperty(PropertyName = "title")]
        public String title { get; set; }

        /// 진료 시작 시각(HH:mm)
        [JsonProperty(PropertyName = "startTime")]
        public String startTime { get; set; }

        /// 진료 마감 시각(HH:mm)
        [JsonProperty(PropertyName = "endTime")]
        public String endTime { get; set; }

        /// 진료 쉬는 시간 리스트
        [JsonProperty(PropertyName = "breakTimes")]
        public List<BreakTime> breakTimes { get; set; }

        /**
         * @brief 생성자
         * @param title: 요일
         * @param startTime: 진료 시작 시각(HH:mm)
         * @param endTime: 진료 마감 시각(HH:mm)
         * @param breakTimeList: 진료 쉬는 시간 리스트
         */
        public Timetable(String title, String startTime, String endTime, List<BreakTime> breakTimeList)
        {
            this.title = title;
            this.startTime = startTime;
            this.endTime = endTime;
            this.breakTimes = breakTimeList;
        }
    }
}