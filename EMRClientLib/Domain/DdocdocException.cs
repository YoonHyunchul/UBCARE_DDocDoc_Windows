﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EMRClientLib.Domain
{
    /**
     * @brief EMR Cleint 전용 Exception
     */
    [Serializable]
    public class DdocdocException : Exception
    {
        /**
         * @brief 생성자
         */
        public DdocdocException() : base() { }

        /**
         * @brief 생성자
         * @param message: message
         */
        public DdocdocException(String message) : base(message) { }

        /**
         * @brief 생성자
         * @param message: message
         * @param innerException: innerException
         */
        public DdocdocException(String message, Exception innerException) : base(message, innerException) { }
    }
}
