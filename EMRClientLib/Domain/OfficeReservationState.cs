﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EMRClientLib.Domain
{
    /**
     * @brief 진료실 & 직원(주치의)별 예약 현황
     */
    public class OfficeReservationState
    {
        /// 청구진료과 목록
        [JsonProperty(PropertyName = "categories")]
        public List<String> categories { get; set; }

        /// 직원(주치의) ID
        [JsonProperty(PropertyName = "staffId")]
        public String staffId { get; set; }

        /// 직원(주치의)
        [JsonProperty(PropertyName = "staff")]
        public String staff { get; set; }

        /// 예약 정보 리스트
        [JsonProperty(PropertyName = "reservationState")]
        public List<ReservationState> reservationStateList { get; set; }

        /**
         * @brief 생성자
         * @param staffId: 직원(주치의) ID
         * @param staff: 직원(주치의)
         * @param reservationStateList: 예약 정보 리스트
         */
        public OfficeReservationState(String staffId, String staff, List<ReservationState> reservationStateList)
        {
            this.staffId = staffId;
            this.staff = staff;
            this.reservationStateList = reservationStateList;
        }
    }
}
