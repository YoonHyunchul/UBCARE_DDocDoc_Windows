﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EMRClientLib.Domain
{
    /**
     * @brief 예약 일시 예약 환자 정보들: 예약 일시, 환자 정보들
     */
    public class ReservationState
    {
        /// 예약 일시(yyyy-MM-dd HH:mm)
        [JsonProperty(PropertyName = "datetime")]
        public String datetime { get; set; }

        /// count
        [JsonProperty(PropertyName = "count")]
        public int count { get; set; }

        /**
         * @brief 생성자
         * @param datetime: 예약일시(yyyy-MM-dd HH:mm)
         * @param count: 예약일시(10분 단위)별 예약자 수
         */
        public ReservationState(String datetime, int count)
        {
            this.datetime = datetime;
            this.count = count;
        }
    }
}
