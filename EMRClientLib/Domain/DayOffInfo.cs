﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EMRClientLib.Domain
{
    /**
     * @brief 휴무 정보
     */
    public class DayOffInfo
    {
        /// 휴무명
        [JsonProperty(PropertyName = "title")]
        public String title { get; set; }

        /// 휴무일(yyyy-MM-dd)
        [JsonProperty(PropertyName = "date")]
        public String date { get; set; }

        /**
         * @brief 생성자
         * @param title: 휴무명
         * @param date: 휴무일(yyyy-MM-dd)
         */
        public DayOffInfo(String title, String date)
        {
            this.title = title;
            this.date = date;
        }
    }
}
