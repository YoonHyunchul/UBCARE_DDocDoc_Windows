﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EMRClientLib.Domain
{
    /**
     * @brief 청구진료과, 진료실, 직원(주치의) 정보
     */
    public class CategoryOfficeStaff
    {
        /// 청구진료과
        [JsonProperty(PropertyName = "category")]
        public String category { get; set; }
        
        /// 진료실 ID
        [JsonProperty(PropertyName = "officeId")]
        public String officeId { get; set; }

        /// 진료실
        [JsonProperty(PropertyName = "office")]
        public String office { get; set; }

        /// 직원(주치의) ID
        [JsonProperty(PropertyName = "staffId")]
        public String staffId { get; set; }

        /// 직원(주치의)
        [JsonProperty(PropertyName = "staff")]
        public String staff { get; set; }

        //TODO
        /// uniqueKey - 의사인 경우 의사면허번호
        [JsonProperty(PropertyName = "uniqueKey")]
        public String uniqueKey { get; set; }

        /**
         * @brief 생성자
         */
        public CategoryOfficeStaff()
        { }

        /**
         * @brief 생성자
         * @param category: 청구진료과
         * @param officeId: 진료실 ID
         * @param office: 진료실
         * @param staffId: 직원(주치의) ID
         * @param staff: 직원(주치의)
         * @param uniqueKey: 의사인 경우 의사면허번호
         */
        public CategoryOfficeStaff(String category, String officeId, String office, String staffId, String staff, String uniqueKey)
        {
            this.category = category;
            this.officeId = officeId;
            this.office = office;
            this.staffId = staffId;
            this.staff = staff;
            this.uniqueKey = uniqueKey;
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
            {
                return false;
            }

            CategoryOfficeStaff p = obj as CategoryOfficeStaff;
            if ((System.Object)p == null)
            {
                return false;
            }

            if (!this.category.Equals(p.category))
                return false;

            if (!this.officeId.Equals(p.officeId))
                return false;

            if (!this.office.Equals(p.office))
                return false;

            if (!this.staffId.Equals(p.staffId))
                return false;

            if (!this.staff.Equals(p.staff))
                return false;

            if (this.uniqueKey == null && p.uniqueKey != null)
                return false;

            if (!this.uniqueKey.Equals(p.staff))
                return false;

            return true;
        }

        public override int GetHashCode()
        {
            return (category + officeId + office + staffId + staff + uniqueKey).GetHashCode();
        }
    }
}