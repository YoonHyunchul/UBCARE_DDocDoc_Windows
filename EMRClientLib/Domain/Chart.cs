﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EMRClientLib.Domain
{
    /**
     * @brief 차트 정보
     */
    public class Chart
    {
        /// 차트번호
        [JsonProperty(PropertyName = "chartNo")]
        public String chartNo { get; set; }

        /// EMR 환자 ID
        [JsonProperty(PropertyName = "emrUserId")]
        public String emrUserId { get; set; }

        /// 환자 성명
        [JsonProperty(PropertyName = "name")]
        public String name { get; set; }

        /// 연락처
        [JsonProperty(PropertyName = "phone")]
        public String phone { get; set; }

        /// 생년월일(yyyy-MM-dd)
        [JsonProperty(PropertyName = "birthDate")]
        public String birthDate { get; set; }

        /// 성별(M/F)
        [JsonProperty(PropertyName = "gender")]
        public String gender { get; set; }

        /// 환자 주소
        [JsonProperty(PropertyName = "address")]
        public String address { get; set; }

        /// 연속메모
        [JsonProperty(PropertyName = "comment")]
        public String comment { get; set; }

        /**
         * @brief 생성자
         * @param chartNo: 차트번호
         * @param emrUserId: EMR 환자 ID
         * @param name: 환자 성명
         * @param phone: 연락처
         * @param birthDate: 생년월일(yyyy-MM-dd)
         * @param gender: 성별(M/F)
         * @param address: 환자 주소
         */
        public 
        Chart
        (
            String chartNo, 
            String emrUserId, 
            String name, 
            String phone, 
            String birthDate, 
            String gender, 
            String address, 
            String comment
        )
        {
            this.chartNo = chartNo;
            this.emrUserId = emrUserId;
            this.name = name;
            this.phone = phone;
            this.birthDate = birthDate;
            this.gender = gender;
            this.address = address;
            this.comment = comment;
        }

        public Chart()
        {}
    }
}
