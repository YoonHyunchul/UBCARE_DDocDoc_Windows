﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EMRClientLib.Domain
{
    /**
     * @brief 접수
     */
    public class Receive
    {
        /// 접수번호
        [JsonProperty(PropertyName = "receiveId")]
        public String receiveId { get; set; }

        /// 차트번호
        [JsonProperty(PropertyName = "chartNo")]
        public String chartNo { get; set; }

        /// 사용자 실명
        [JsonProperty(PropertyName = "userName")]
        public String userName { get; set; }

        /// 사용자 전화번호
        [JsonProperty(PropertyName = "userPhone")]
        public String userPhone { get; set; }

        /// 생년월일(yyyy-MM-dd)
        [JsonProperty(PropertyName = "birthDate")]
        public String birthDate { get; set; }

        /// 성별(M/F)
        [JsonProperty(PropertyName = "gender")]
        public String gender { get; set; }

        /// 청구진료과
        [JsonProperty(PropertyName = "category")]
        public String category { get; set; }

        /// 진료실 ID
        [JsonProperty(PropertyName = "officeId")]
        public String officeId { get; set; }

        /// 진료실
        [JsonProperty(PropertyName = "office")]
        public String office { get; set; }

        /// 직원(주치의) ID
        [JsonProperty(PropertyName = "staffId")]
        public String staffId { get; set; }

        /// 직원(주치의)
        [JsonProperty(PropertyName = "staff")]
        public String staff { get; set; }
        
        /// 연속메모
        [JsonProperty(PropertyName = "comment")]
        public String comment { get; set; }

        /// 당일메모
        [JsonProperty(PropertyName = "memo")]
        public String memo { get; set; }


        /// 상태: EnumReceiveState.RECEIVE(접수) / EnumReceiveState.CHECK_UP(진찰) / EnumReceiveState.RECEIPT_WAIT(수납대기) / EnumReceiveState.RECEIPT_DONE(수납완료)
        [JsonProperty(PropertyName = "receiveState")]
        public String receiveStateString { 
            get 
            {
                return receiveState.ToString();
            }

            set
            {
                receiveState = (EnumReceiveState)Enum.Parse(typeof(EnumReceiveState), value);
            }
        }

        [JsonIgnore]
        public EnumReceiveState receiveState { 
            get; set;
        }

        /// 접수 일시(yyyy-MM-dd HH:mm) 
        [JsonProperty(PropertyName = "receiveDatetime")]
        public String receiveDatetime { get; set; }

        /// 상태 변경 일시(yyyy-MM-dd HH:mm)
        [JsonProperty(PropertyName = "stateUpdatedDatetime")]
        public String stateUpdateDatetime { get; set; }

        /**
         * @brief 생성자
         */
        public Receive()
        { }

        /**
         * @brief 생성자
         * @param receiveId: 접수번호
         * @param chartNo: 차트번호
         * @param userName: 사용자 실명
         * @param userPhone: 사용자 전화번호
         * @param birthDate: 생년월일(yyyy-MM-dd)
         * @param gender: 성별(M/F)
         * @param category: 청구진료과
         * @param officeId: 진료실 ID
         * @param office: 진료실
         * @param staffId: 직원(주치의) ID
         * @param staff: 직원(주치의)
         * @param comment: 연속메모
         * @param memo : 당일메모
         * @param receiveState: 상태
         * @param receiveDatetime: 접수 일시(yyyy-MM-dd HH:mm)
         * @param stateUpdateDatetime: 상태 변경 일시(yyyy-MM-dd HH:mm)
         */
        public Receive(String receiveId, string chartNo, string userName, string userPhone, string birthDate, 
            string gender, string category, string officeId, string office, string staffId, 
            string staff, string comment, string memo, EnumReceiveState receiveState, string receiveDatetime, string stateUpdateDatetime)
        {
            this.receiveId = receiveId;
            this.chartNo = chartNo;
            this.userName = userName;
            this.userPhone = userPhone;
            this.birthDate = birthDate;
            this.gender = gender;
            this.category = category;
            this.officeId = officeId;
            this.office = office;
            this.staffId = staffId;
            this.staff = staff;
            this.comment = comment;
            this.memo = memo;
            this.receiveState = receiveState;
            this.receiveDatetime = receiveDatetime;
            this.stateUpdateDatetime = stateUpdateDatetime;
        }

        /**
         * @brief 접수 상태
         */
        public enum EnumReceiveState
        {
            RECEIVE, //접수
            CHECK_UP, //진찰
            RECEIPT_WAIT, //수납대기
            RECEIPT_DONE // 수납완료
        }
    }
}
