﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EMRClientLib.Domain
{
    /**
     * @brief 예약 정보
     */
    public class Reservation
    {
        /// EMR 예약 아이디
        [JsonProperty(PropertyName = "emrReservId")]
        public String emrReservId { get; set; }

        /// 똑닥 예약 아이디
        [JsonProperty(PropertyName = "reservationId")]
        public String reservationId { get; set; }

        /// 사용자 실명
        [JsonProperty(PropertyName = "userName")]
        public String userName { get; set; }

        /// 사용자 전화번호
        [JsonProperty(PropertyName = "userPhone")]
        public String userPhone { get; set; }

        /// 생년월일(yyyy-MM-dd)
        [JsonProperty(PropertyName = "birthDate")]
        public String birthDate { get; set; }

        /// 성별(M/F)
        [JsonProperty(PropertyName = "gender")]
        public String gender { get; set; }

        /// 똑닥 사용자 아이디
        [JsonProperty(PropertyName = "userId")]
        public String userId { get; set; }

        /// 증상 (사용자 입력)
        [JsonProperty(PropertyName = "symptomText")]
        public String symptomText { get; set; }

        /// 예약 일시(yyyy-MM-dd HH:mm)
        [JsonProperty(PropertyName = "confirmReservationDatetime")]
        public String confirmReservationDatetime { get; set; }

        /// 청구진료과
        [JsonProperty(PropertyName = "category")]
        public String category { get; set; }

        /// 진료실 ID
        [JsonProperty(PropertyName = "officeId")]
        public String officeId { get; set; }

        /// 진료실
        [JsonProperty(PropertyName = "office")]
        public String office { get; set; }

        /// 직원(주치의) ID
        [JsonProperty(PropertyName = "staffId")]
        public String staffId { get; set; }

        /// 직원(주치의)
        [JsonProperty(PropertyName = "staff")]
        public String staff { get; set; }

        /// 차트번호
        [JsonProperty(PropertyName = "chartNo")]
        public String chartNo { get; set; }

        /// 예약메모
        [JsonProperty(PropertyName = "memo")]
        public String memo { get; set; }

        /// 연속메모
        [JsonProperty(PropertyName = "comment")]
        public String comment { get; set; }

        [JsonIgnore]
        public ReservationState reserveState { get; set; }

        /// 예약 상태
        [JsonProperty(PropertyName = "reserveState")]
        public String reserveStateString
        {
            get
            {
                return reserveState.ToString();
            }
            set
            {
                reserveState = (ReservationState)Enum.Parse(typeof(ReservationState), value);
            }
        }
        /**
         * @brief 생성자
         */
        public Reservation()
        { }

        /**
         * @brief 생성자
         * @param emrReservId: EMR 예약 아이디
         * @param reservationId: 똑닥 예약 아이디
         * @param userName: 사용자 실명
         * @param userPhone: 사용자 전화번호
         * @param birthDate: 생년월일(yyyy-MM-dd)
         * @param gender: 성별(M/F)
         * @param userId: 똑닥 사용자 아이디
         * @param symptomText: 증상 (사용자 입력)
         * @param confirmReservationDatetime: 예약 일시(yyyy-MM-dd HH:mm)
         * @param category: 청구진료과
         * @param officeId: 진료실 ID
         * @param office: 진료실
         * @param staffId: 직원(주치의) ID
         * @param staff: 직원(주치의)
         * @param chartNo: 차트번호
         * @param memo: 예약메모
         * @param comment: 연속메모
         * @param reserveStatus : 예약상태 (OS 예약, OS 예약취소)
         */
        public Reservation( String emrReservId, String reservationId,   String userName,    String userPhone,   String birthDate,   String gender,      
                            String userId,      String symptomText,     String confirmReservationDatetime,      String category,    String officeId,    String office,
                            String staffId,     String staff,           String chartNo,     String memo,        String comment,     String reserveState)
        {
            this.emrReservId = emrReservId;
            this.reservationId = reservationId;
            this.userName = userName;
            this.userPhone = userPhone;
            this.birthDate = birthDate;
            this.gender = gender;
            this.userId = userId;
            this.symptomText = symptomText;
            this.confirmReservationDatetime = confirmReservationDatetime;
            this.category = category;
            this.officeId = officeId;
            this.office = office;
            this.staffId = staffId;
            this.staff = staff;
            this.chartNo = chartNo;
            this.memo = memo;
            this.comment = comment;
            this.reserveStateString = reserveState;
        }

        public enum ReservationState
        {
            RESERVE=10,        // 예약대기
            CONFIRM,        // 예약확정
            DENY,           // 예약거부
            RESCISSION,     // 예약철회
            RECEIVE,        // 내원
            COMPLETE        // 완료
        }

        public override String ToString()
        {
            StringBuilder sb = new StringBuilder("emrReservId : ");
            sb.Append(this.emrReservId);

            sb.Append("\nreservationId : ");
            sb.Append(this.reservationId);
            sb.Append("\nuserName : ");
            sb.Append(this.userName);
            sb.Append("\nuserPhone : ");
            sb.Append(this.userPhone);
            sb.Append("\nbirthDate : ");
            sb.Append(this.birthDate);
            sb.Append("\ngender : ");
            sb.Append(this.gender);
            sb.Append("\nuserId : ");
            sb.Append(this.userId);
            sb.Append("\nsymptomText : ");
            sb.Append(this.symptomText);
            sb.Append("\nconfirmReservationDatetime : ");
            sb.Append(this.confirmReservationDatetime);
            sb.Append("\ncategory : ");
            sb.Append(this.category);
            sb.Append("\nofficeId : ");
            sb.Append(this.officeId);
            sb.Append("\noffice : ");
            sb.Append(this.office);
            sb.Append("\nstaffId : ");
            sb.Append(this.staffId);
            sb.Append("\nstaff : ");
            sb.Append(this.staff);
            sb.Append("\nchartNo : ");
            sb.Append(this.chartNo);
            sb.Append("\nmemo : ");
            sb.Append(this.memo);
            sb.Append("\ncomment : ");
            sb.Append(this.comment);
            sb.Append("\nreserveState : ");
            sb.Append(this.reserveState);

            return sb.ToString();
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
            {
                return false;
            }

            Reservation p = obj as Reservation;
            if ((System.Object)p == null)
            {
                return false;
            }

            if (!this.emrReservId.Equals(p.emrReservId))
                return false;

            if(String.IsNullOrEmpty(this.reservationId))
            {
                if (!String.IsNullOrEmpty(p.reservationId))
                    return false;
            }
            else if (!this.reservationId.Equals(p.reservationId))
                return false;

            if (!this.userName.Equals(p.userName))
                return false;

            if (!this.userPhone.Equals(p.userPhone))
                return false;

            if (!this.birthDate.Equals(p.birthDate))
                return false;
            


            if (!this.gender.Equals(p.gender))
                return false;

            if (String.IsNullOrEmpty(this.userId))
            {
                if (!String.IsNullOrEmpty(p.userId))
                    return false;
            }
            else if (!this.userId.Equals(p.userId))
                return false;

            if (String.IsNullOrEmpty(this.symptomText))
            {
                if (!String.IsNullOrEmpty(p.symptomText))
                    return false;
            }
            else if (!this.symptomText.Equals(p.symptomText))
                return false;

            if (!this.confirmReservationDatetime.Equals(p.confirmReservationDatetime))
                return false;

            if (String.IsNullOrEmpty(this.category))
            {
                if (!String.IsNullOrEmpty(p.category))
                    return false;
            }
            else if (!this.category.Equals(p.category))
                return false;



            if (!this.officeId.Equals(p.officeId))
                return false;

            if (!this.office.Equals(p.office))
                return false;

            if (!this.staffId.Equals(p.staffId))
                return false;

            if (!this.staff.Equals(p.staff))
                return false;

            if (!this.chartNo.Equals(p.chartNo))
                return false;



            if (String.IsNullOrEmpty(this.memo))
            {
                if (!String.IsNullOrEmpty(p.memo))
                    return false;
            }
            else if (!this.memo.Equals(p.memo))
                return false;

            if (String.IsNullOrEmpty(this.comment))
            {
                if (!String.IsNullOrEmpty(p.comment))
                    return false;
            }
            else if (!this.comment.Equals(p.comment))
                return false;

            return true;
        }

        public override int GetHashCode()
        {
            return this.ToString().GetHashCode();
        }
    }
}
