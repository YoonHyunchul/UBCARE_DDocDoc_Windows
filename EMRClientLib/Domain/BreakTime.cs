﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EMRClientLib.Domain
{
    /**
     * @brief 진료 쉬는 시간
     */
    public class BreakTime
    {
        /// 진료 쉬는 시간 명칭
        [JsonProperty(PropertyName = "title")]
        public String title { get; set; }

        /// 진료 쉬는 시작 시각(HH:mm)
        [JsonProperty(PropertyName = "startTime")]
        public String startTime { get; set; }

        /// 진료 쉬는 마감 시각(HH:mm)
        [JsonProperty(PropertyName = "endTime")]
        public String endTime { get; set; }

        /**
         * @brief 생성자
         * @param title: 진료 쉬는 시간 명칭
         * @param startTime: 진료 쉬는 시작 시각(HH:mm)
         * @param endTime: 진료 쉬는 마감 시각(HH:mm)
         */
        public BreakTime(String title, String startTime, String endTime)
        {
            this.title = title;
            this.startTime = startTime;
            this.endTime = endTime;
        }
    }
}
