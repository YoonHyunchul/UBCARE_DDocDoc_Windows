﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EMRClientLib.Domain
{
    /**
     * @brief 실시간 대기자 정보: 차트번호, 성명, 연락처
     */
    public class AwaiterCounter
    {
        /// 진료실 ID
        [JsonProperty(PropertyName = "officeId")]
        public String officeId { get; set; }

        /// 진료실
        [JsonProperty(PropertyName = "office")]
        public String office { get; set; }

        /// 직원(주치의) ID
        [JsonProperty(PropertyName = "staffId")]
        public String staffId { get; set; }

        /// 직원(주치의)
        [JsonProperty(PropertyName = "staff")]
        public String staff { get; set; }

        /// 실시간 대기자수
        [JsonProperty(PropertyName = "count")]
        public int count { get; set; }

        /**
         * @brief 생성자
         * @param officeId: 진료실 ID
         * @param office: 진료실
         * @param staffId: 직원(주치의) ID
         * @param staff: 직원(주치의)
         * @param count: 대기자수
         */
        public AwaiterCounter(String officeId, String office, String staffId, String staff, int count)
        {
            this.officeId = officeId;
            this.office = office;
            this.staffId = staffId;
            this.staff = staff;
            this.count = count;
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
            {
                return false;
            }

            AwaiterCounter p = obj as AwaiterCounter;
            if ((System.Object)p == null)
            {
                return false;
            }

            if (!this.officeId.Equals(p.officeId))
                return false;

            if (!this.office.Equals(p.office))
                return false;

            if (!this.staffId.Equals(p.staffId))
                return false;

            if (!this.staff.Equals(p.staff))
                return false;

            if (this.count != p.count)
                return false;

            return true;
        }

        public override int GetHashCode()
        {
            return (officeId + office + staffId + staff + count).GetHashCode();
        }
    }
}