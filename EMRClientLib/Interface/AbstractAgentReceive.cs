﻿using EMRClientLib.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EMRClientLib.Interface
{
    public abstract class AbstractAgentReceive
    {

        #region 접수
        /**
         * @brief 접수 상세 정보 조회
         * @param receiveId: 접수 번호
         * @return 접수 상세 정보
         */
        //TODO
        public virtual Receive GetReceive(String receiveId)
        {
            throw new NotImplementedException();
        }

        /**
         * @brief 검색 시작 일자와 검색 종료 일자, 직원(주치의)들의 정보로 접수 현황 검색
         * @param startDate: 검색 시작 일자(yyyy-MM-dd)
         * @param endDate: 검색 종료 일자(yyyy-MM-dd)
         * @param staffIds: 직원(주치의) 아이디 String 배열
         * @return 접수 현황 리스트
         */
        //TODO
        public virtual List<Receive> GetReceives(String startDate, String endDate, String[] staffIds)
        {
            throw new NotImplementedException();
        }
        

        /**
         * @brief 검색 시작 일자와 검색 종료 일자, 직원(주치의)들의 정보로 예약 후 접수한 접수 현황검색
         * @param startDate: 검색시작일자 (yyyy-MM-dd)
         * @param endDate: 검색 종료 일자(yyy-MM-dd)
         * @param staffIds: 직원(주치의) 아이디 String 배열
         * @return 예약 후 접수 현황 리스트
         */
        public virtual List<Receive> GetReceivesFromReservation(String StartDate, String endDate, String[] staffIds)
        {
            throw new NotImplementedException();
        }


        /**
         * @brief 검색 시작 일자와 검색 종료 일자, 직원(주치의)들의 정보로 예약 후 접수, 수납완료 된 접수현황 검색
         * @param startDate 검색시작일자(yyyy-MM-dd)
         * @param endDate 검색종료일자(yyyy-MM-dd)
         * @param staffIds: 직원(주치의) 아이디 String배열
         * @return 예약 후 접수 및 수납 완료 현황 리스트
         */
        public virtual List<Receive> GetReceiptsFromReservation(String StartDate, String endDate, String[] staffIds)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
