﻿using EMRClientLib.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

// 2016.10.04 AbstractAgentHospital 접수 , 예약 파트 분리 여부 피저빌리티

namespace EMRClientLib.Interface
{
    #region 이벤트 리스너
    public delegate void DelegateChangeRealtimeAwaiterCounterFromEMR();
    #endregion

    /**
     * @brief 각 EMR Cleint 가 구현해야할 전략 패턴의 전략 인터페이스
     */
    public abstract class AbstractAgentHospital 
    {
        #region EventHandler
        /// EMR 에서 진료 대기자 수 변동 시
        public event DelegateChangeRealtimeAwaiterCounterFromEMR EventHandlerForChangeRealtimeAwaiterCounterFromEMR;

        /**
         * @brief EMR 에서 진료 대기자 수 변동 시
         * 접수, 접수취소 시 이벤트 발생 
         */
        public void FireChangeAwaiterCounterFromEMR()
        {
            // 테이블 변경 감지
            // https://msdn.microsoft.com/en-us/library/ms179315(v=sql.110).aspx
            if (EventHandlerForChangeRealtimeAwaiterCounterFromEMR != null)
                EventHandlerForChangeRealtimeAwaiterCounterFromEMR();
        }
        #endregion EventHandler
        
        #region 병원 정보
        /**
         * @brief API Server 로 로그인을 하기 위한 병원 ID 조회
         * @return 병원 ID
         */
        public virtual String GetHospitalId()
        {
            throw new NotImplementedException();
        }
        
        /**
         * @brief API Server 로 로그인을 하기 위한 병원 비밀번호 조회
         * @return 병원 비밀번호
         */
        public virtual String GetHospitalPassword()
        {
            throw new NotImplementedException();
        }
        
        /**
         * @brief 차트 번호 자동 생성 여부
         * @return bool (자동 생성: true / 수동 생성: false)
         */
        public virtual bool IsChartNoAutoGenerate()
        {
            throw new NotImplementedException();
        }
        
        /**
         * @brief 연속 메모(환자 이력) 사용 여부
         * @return bool (사용: true / 미사용: false)
         */
        public virtual bool UseComment()
        {
            throw new NotImplementedException();
        }
        #endregion

        #region 청구진료과, 진찰실, 직원(주치의) 조회
        /**
         * @brief 청구진료과, 진료실, 직원(주치의) 조회
         * @return 청구진료과, 진료실, 직원(주치의) 목록
         */
        #region Example
        /*
[  
   {  
	  "category":"치과보철과",
	  "office":"치과보철과",
	  "staff":"김보철"
   },
   {  
	  "category":"치과보철과",
	  "office":"치과보철과",
	  "staff":"한보철"
   },
   {  
	  "category":"치과교정과",
	  "office":"치과교정과",
	  "staff":"이구강"
   }
]
         */
        #endregion
        public virtual List<CategoryOfficeStaff> GetCategoryOfficeStaffs()
        {
            throw new NotImplementedException();
        }
        #endregion

        /**
         * @brief 진료시간 요청
         * @return 진료시간 리스트
         * @details 휴무일은 전송하지 않는다.
         */
        #region Example
        /*
[  
   {  
      "title":"월요일",
      "startTime":"09:00",
      "endTime":"18:00",
      "breakTime":[  
         {  
            "title":"점심",
            "startTime":"12:00",
            "endTime":"13:00"
         },
         {  
            "title":"저녁",
            "startTime":"18:00",
            "endTime":"19:00"
         }
      ]
   },
   {  
      "title":"화요일",
      "startTime":"09:00",
      "endTime":"20:00",
      "breakTime":[  
         {  
            "title":"점심",
            "startTime":"12:00",
            "endTime":"13:00"
         },
         {  
            "title":"저녁",
            "startTime":"18:00",
            "endTime":"19:00"
         }
      ]
   },
   {  
      "title":"목요일",
      "startTime":"09:00",
      "endTime":"20:00",
      "breakTime":[  
         {  
            "title":"점심",
            "startTime":"12:00",
            "endTime":"13:00"
         },
         {  
            "title":"저녁",
            "startTime":"18:00",
            "endTime":"19:00"
         }
      ]
   },
   {  
      "title":"금요일",
      "startTime":"09:00",
      "endTime":"20:00",
      "breakTime":[  
         {  
            "title":"점심",
            "startTime":"12:00",
            "endTime":"13:00"
         },
         {  
            "title":"저녁",
            "startTime":"18:00",
            "endTime":"19:00"
         }
      ]
   }
]
         */
        #endregion
        public virtual List<Timetable> GetTimetable()
        {
            throw new NotImplementedException();
        }

        /**
         * @brief 휴무 정보 조회
         * @return 휴무 정보 리스트
         * @details 오늘 이후의 직원(주치의)별 휴무 일정 전체를 반환
         */
        public virtual Dictionary<String, List<DayOffInfo>> GetDayOffSchedule()
        {
            throw new NotImplementedException();
        }
        
        #region 차트 정보
        /**
         * @brief 차트 정보 조회
         * @param name: 환자 성명
         * @param birthDate: 생년월일(yyyy-MM-dd)
         * @param gender: 성별(M/F)
         * @return 차트 정보 리스트
         * @details 
         * @details name, birthDate, gender 가 null 이면 where 에서 제외, 아니면 and 조건
         *          성별 코드 참조 - http://marga.tistory.com/488
         */
        #region Example
        /*
[  
   {  
      "chartNo":"12341",
      "name":"홍길동",
      "phone":"010-1234-1231"
   },
   {  
      "chartNo":"12342",
      "name":"일지매",
      "phone":"010-1234-1232"
   },
   {  
      "chartNo":"12343",
      "name":"설현",
      "phone":"010-1234-1233"
   },
   {  
      "chartNo":"12344",
      "name":"강감찬",
      "phone":"010-1234-1234"
   },
   {  
      "chartNo":"12345",
      "name":"배수지",
      "phone":"010-1234-1235"
   }
]
         */
        #endregion
        public virtual List<Chart> GetCharts(String name, String birthDate, String gender)
        {
            throw new NotImplementedException();
        }

        public virtual String GetLastestChartNO()
        {
            throw new NotImplementedException();
        }
        #endregion

        #region 실시간 대기자수
        /**
         * @brief 진료실별, 의사별 실시간 대기자수
         * @return 진료실별, 의사별 실시간 대기자수 목록
         */
        #region Example
        /*
[  
   {  
	  "office":"치과보철과",
	  "staff":"김보철",
	  "count":2
   },
   {  
	  "office":"치과보철과",
	  "staff":"한보철",
	  "count":1
   },
   {  
	  "office":"치과교정과",
	  "staff":"이구강",
	  "count":5
   }
]
         */
        #endregion
        public virtual List<AwaiterCounter> GetRealTimeAwaiterCount()
        {
            throw new NotImplementedException();
        }
        #endregion

    }
}