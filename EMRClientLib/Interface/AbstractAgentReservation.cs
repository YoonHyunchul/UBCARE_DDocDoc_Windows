﻿using EMRClientLib.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EMRClientLib.Interface
{
    #region
    /// EMR 에서 예약을 생성한 경우 이벤트 리스너
    public delegate void DelegateCreateReservationFromEMR(Reservation reservation);
    /// EMR 에서 예약을 변경한 경우 이벤트 리스너
    public delegate void DelegateUpdateReservationFromEMR(Reservation reservation);
    /// EMR 에서 예약을 삭제한 경우 이벤트 리스너
    public delegate void DelegateDeleteReservationFromEMR(Reservation reservation);
    /// EMR 에서 예약을 접수한 경우 이벤트 리스너  
    public delegate void DelegateReceiveReservationFromEMR(Reservation reservation);
    /// EMR 에서 예약을 수납한 경우 이벤트 리스너 
    public delegate void DelegateReceiptReservationFromEMR(Reservation reservation);
    
    #endregion

    public abstract class AbstractAgentReservation
    {

        

        #region EventHandler
        // EMR 에서 예약을 생성한 경우
        public event DelegateCreateReservationFromEMR EventHandlerForCreateReservationFromEMR;
        /// EMR 에서 예약을 변경한 경우
        public event DelegateUpdateReservationFromEMR EventHandlerForUpdateReservationFromEMR;
        /// EMR 에서 예약을 삭제한 경우
        public event DelegateDeleteReservationFromEMR EventHandlerForDeleteReservationFromEMR;
        
        /// EMR 에서 예약을 접수한 경우
        public event DelegateReceiveReservationFromEMR EventHandlerForReceiveReservationFromEMR;
        /// EMR 에서 예약을 수납한 경우
        public event DelegateReceiptReservationFromEMR EventHandlerForReceiptReservationFromEMR;


        /**
         * @brief EMR 에서 예약을 생성한 경우
         */
        public void FireCreateReservationFromEMR(Reservation reservation)
        {
            // 테이블 변경 감지
            // https://msdn.microsoft.com/en-us/library/ms179315(v=sql.110).aspx
            if (EventHandlerForCreateReservationFromEMR != null)
                EventHandlerForCreateReservationFromEMR(reservation);
        }

        /**
         * @brief EMR 에서 예약을 변경한 경우
         */
        public void FireUpdateReservationFromEMR(Reservation reservation)
        {
            // 테이블 변경 감지
            // https://msdn.microsoft.com/en-us/library/ms179315(v=sql.110).aspx          
            if (EventHandlerForUpdateReservationFromEMR != null)
                EventHandlerForUpdateReservationFromEMR(reservation);
        }

        /**
         * @brief EMR 에서 예약을 삭제한 경우
         */
        public void FireDeleteReservationFromEMR(Reservation reservation)
        {
            // 테이블 변경 감지
            // https://msdn.microsoft.com/en-us/library/ms179315(v=sql.110).aspx
            if (EventHandlerForDeleteReservationFromEMR != null)
                EventHandlerForDeleteReservationFromEMR(reservation);
        }

        /**
         * @brief EMR 에서 예약을 접수한 경우
         */
        public void FireReceiveReservationFromEMR(Reservation reservation)
        {
            if (EventHandlerForReceiveReservationFromEMR != null)
                EventHandlerForReceiveReservationFromEMR(reservation);
        }

        /**
         * @brief EMR 에서 예약을 수납한 경우
         */
        public void FireReceiptReservationFromEMR(Reservation reservation)
        {
            if (EventHandlerForReceiptReservationFromEMR != null)
                EventHandlerForReceiptReservationFromEMR(reservation);
        }

        #endregion

        #region 예약
        /**
         * @brief 예약 상세 정보 조회
         * @param emrReservId: EMR 예약 아이디
         * @return 예약 상세 정보
         */
        #region Example
        /*
{  
  "userName":"홍길동",
  "userPhone":"010-1234-5678",
  "birthDate":"1993-02-08",
  "gender":"M",
  "userId":"gildongHong",
  "symptomText":"아파요. 마이 아파요.. 정말 아파요",
  "confirmReservationDatetime":"2016-08-02 14:20"
}
         */
        #endregion
        public virtual Reservation GetReservation(String emrReservId)
        {
            throw new NotImplementedException();
        }
        
        /**
         * @brief 일자와 진료실&직원(주치의) 정보로 예약 현황 검색
         * @param date: 검색 일자(yyyy-MM-dd)
         * @param offices: 진료실&직원(주치의) 리스트 List<CategoryOfficeStaff>
         * @return 일시별 진료실&직원(주치의)별 예약 현황
         */
        #region Example
        /*
[  
   {  
      "office":"치과보철과",
      "staff":"김보철",
      "reservationState":[  
         {  
            "datetime":"2016-08-01 13:00",
            "patients":[  
               {  
                  "chartNo":"chartNo001",
                  "emrReservId":"emrReservId001"
               },
               {  
                  "chartNo":"chartNo002",
                  "emrReservId":"emrReservId002"
               }
            ]
         },
         {  
            "datetime":"2016-08-01 13:10",
            "patients":[  
               {  
                  "chartNo":"chartNo003",
                  "emrReservId":"emrReservId003"
               },
               {  
                  "chartNo":"chartNo004",
                  "emrReservId":"emrReservId004"
               }
            ]
         }
      ]
   },
   {  
      "office":"치과보철과",
      "staff":"한보철",
      "reservationState":[  
         {  
            "datetime":"2016-08-01 12:00",
            "patients":[  
               {  
                  "chartNo":"chartNo005",
                  "emrReservId":"emrReservId005"
               },
               {  
                  "chartNo":"chartNo006",
                  "emrReservId":"emrReservId006"
               }
            ]
         },
         {  
            "datetime":"2016-08-02 13:10",
            "patients":[  
               {  
                  "chartNo":"chartNo007",
                  "emrReservId":"emrReservId007"
               }
            ]
         },
         {  
            "datetime":"2016-08-03 10:00",
            "patients":[  
               {  
                  "chartNo":"chartNo008",
                  "emrReservId":"emrReservId008"
               },
               {  
                  "chartNo":"chartNo009",
                  "emrReservId":"emrReservId009"
               },
               {  
                  "chartNo":"chartNo010",
                  "emrReservId":"emrReservId010"
               }
            ]
         }
      ]
   },
   {  
      "office":"치과교정과",
      "staff":"이구강",
      "reservationState":[  
         {  
            "datetime":"2016-08-02 13:00",
            "patients":[  
               {  
                  "chartNo":"chartNo011",
                  "emrReservId":"emrReservId011"
               },
               {  
                  "chartNo":"chartNo012",
                  "emrReservId":"emrReservId012"
               }
            ]
         },
         {  
            "datetime":"2016-08-02 13:10",
            "patients":[  
               {  
                  "chartNo":"chartNo007",
                  "emrReservId":"emrReservId007"
               }
            ]
         },
         {  
            "datetime":"2016-08-03 10:00",
            "patients":[  
               {  
                  "chartNo":"chartNo013",
                  "emrReservId":"emrReservId013"
               },
               {  
                  "chartNo":"chartNo014",
                  "emrReservId":"emrReservId014"
               },
               {  
                  "chartNo":"chartNo015",
                  "emrReservId":"emrReservId015"
               }
            ]
         }
      ]
   }
]
         */
        #endregion
        public virtual List<Reservation> GetReservations(String date, List<CategoryOfficeStaff> offices)
        {
            throw new NotImplementedException();
        }

        // 윤현철 추가 GetResvations 스펙 변경으로 인해 선 오버로딩 작업 
        public virtual List<Reservation> GetReservations(String startDate, String endDate, List<CategoryOfficeStaff> offices)
        {
            throw new NotImplementedException();
        }

        /**
         * @brief 환자Id 를 이용해서 해당 환자의 모든 예약을 검색
         * @param emrUserId: EMR 환자 Id
         * @return 예약 내역 리스트 (예약일시 내림차순 정렬)
         */
        public virtual List<Reservation> GetReservations(String emrUserId)
        {
            throw new NotImplementedException();
        }

        /**
         * @brief 예약 생성
         * @param reservation: Reservation
         * @return EMR 예약 아이디
         */
        public virtual String CreateReservation(Reservation reservation)
        {
            throw new NotImplementedException();
        }

        /**
         * @brief 예약 변경
         * @param reservation: Reservation
         * @return 성공 여부
         */
        public virtual bool UpdateReservation(Reservation reservation)
        {
            throw new NotImplementedException();
        }

        /**
         * @brief 예약 삭제
         * @param reservation: Reservation
         * @return 성공 여부
         */
        public virtual bool DeleteReservation(Reservation reservation)
        {
            throw new NotImplementedException();
        }
        #endregion
        
    }
}
