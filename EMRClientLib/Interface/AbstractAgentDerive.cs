﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EMRClientLib.Interface
{
    public class AbstractAgentDerive
    {
        public AbstractAgentHospital agentHospital { get; set; }
        public AbstractAgentReceive agentReceive { get; set; }
        public AbstractAgentReservation agentReservation { get; set; }
    }
}
