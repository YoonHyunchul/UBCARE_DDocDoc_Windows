﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace EMRClientLib
{
    /**
     * @brief Utility
     */
    public class Util
    {
        /*
         * brief 대시(-) 가 없는 일자(일시)를 대시(-)가 있는 형태로 변경
         * param datetime: String 일자(yyyyMMdd) 또는 일시(yyyyMMddHHmm)
         * return String 일자(yyyy-MM-dd) 또는 일시(yyyy-MM-dd HH:mm)
         *
         *
        public static String NormalizeDateTimeString(String datetime) 
        {
            String result = datetime;

            if (!String.IsNullOrEmpty(datetime) && datetime.IndexOf('-') < 0)
            {
                //yyyy-MM-dd'T'HH:mm:ss.S'Z'
                switch(datetime.Length)
                {
                    case 8:
                        {
                            DateTime theDateTime = DateTime.ParseExact(datetime, "yyyyMMdd", CultureInfo.InvariantCulture, DateTimeStyles.None);

                            result = theDateTime.ToString("yyyy-MM-dd");

                            break;
                        }
                    case 12:
                        {
                            DateTime theDateTime = DateTime.ParseExact(datetime, "yyyyMMddHHmm", CultureInfo.InvariantCulture, DateTimeStyles.None);

                            result = theDateTime.ToString("yyyy-MM-dd HH:mm");

                            break;
                        }
                    default:
                        {
                            DateTime theDateTime = DateTime.ParseExact(datetime, "MM/dd/yyyy HH:mm:ss", CultureInfo.InvariantCulture, DateTimeStyles.None);

                            result = theDateTime.AddHours(9).ToString("yyyy-MM-dd HH:mm"); //"08/04/2016 04:10:00"

                            break;
                        }

                }
            }

            return result;
        }
         */

        /**
         * @brief UnixTimestamp 를 DateTime 으로 변경
         * @param timestamp: Unix Timestamp
         * @return DateTime
         */
        /*
        public static DateTime ConvertFromUnixTimestamp(double timestamp)
        {
            DateTime origin = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            return origin.AddHours(9).AddSeconds(timestamp);
        }
        */

        /**
         * @brief UnixTimestamp 를 DateTime 으로 변경
         * @param strTimestamp: Unix Timestamp 문자열
         * @return DateTime
         */
        /*
        public static DateTime ConvertFromUnixTimestamp(String strTimestamp)
        {
            double timestamp = Double.Parse(strTimestamp);
            timestamp = timestamp > 253402300799 ? timestamp / 1000 : timestamp;
            return ConvertFromUnixTimestamp(timestamp);
        }
        */

        /**
         * @brief DateTime 을 UnixTimestamp 로 변경
         * @param datetime: DateTime
         * @return Unix Timestamp
         */
        /*
        public static double ConvertToUnixTimestamp(DateTime datetime)
        {
            DateTime origin = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            TimeSpan diff = datetime.ToUniversalTime() - origin;
            return Math.Floor(diff.TotalMilliseconds);
        }
        */
    }
}
